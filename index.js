/**
 * @format
 */

import {AppRegistry} from 'react-native';
import PushNotification from 'react-native-push-notification';
import App from './src/App';
import {name as appName} from './app.json';
import 'react-native-gesture-handler';
PushNotification.createChannel(
  {
    channelId: 'foodlike', // (required)
    channelName: 'foodlike', // (required)
    playSound: true, // (optional) default: true
    soundName: 'default', // (optional) See `soundName` parameter of `localNotification` function
    vibrate: true,
  },
  created => console.log(`createChannel returned '${created}'`), // (optional) callback returns whether the channel was created, false means it already existed.
);

AppRegistry.registerComponent(appName, () => App);
