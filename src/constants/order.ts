interface ITimeSettings {
  [key: number]: {max: number; min: number};
}
export const timeSettings: ITimeSettings = {
  0: {
    min: 10,
    max: 21,
  },
  1: {
    min: 10,
    max: 21,
  },
  2: {
    min: 10,
    max: 21,
  },
  3: {
    min: 10,
    max: 21,
  },
  4: {
    min: 10,
    max: 22,
  },
  5: {
    min: 10,
    max: 22,
  },
  6: {
    min: 10,
    max: 21,
  },
};
