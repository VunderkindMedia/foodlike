import {ImageSourcePropType} from 'react-native';

export interface IAnotherImages {
  logo: ImageSourcePropType;
  no_photo: ImageSourcePropType;
  money: ImageSourcePropType;
  cart_empty: ImageSourcePropType;
  favorites_empty: ImageSourcePropType;
  checkbox_true: ImageSourcePropType;
  old_logo: ImageSourcePropType;
  new_logo: ImageSourcePropType;
  offline_logo: ImageSourcePropType;
  review_finish: ImageSourcePropType;
}
export const AnotherImages: IAnotherImages = {
  logo: require('../../../assets/images/foodlike/logo.png'),
  no_photo: require('../../../assets/images/foodlike/no-photo.png'),
  money: require('../../../assets/images/foodlike/money.png'),
  cart_empty: require('../../../assets/images/foodlike/cart_empty.png'),
  favorites_empty: require('../../../assets/images/foodlike/favorite_empty.png'),
  checkbox_true: require('../../../assets/images/foodlike/checkbox_true.png'),
  old_logo: require('../../../assets/images/foodlike/old_logo.png'),
  new_logo: require('../../../assets/images/foodlike/new_logo.png'),
  offline_logo: require('../../../assets/images/foodlike/offline_logo.png'),
  review_finish: require('../../../assets/images/foodlike/review_finish.png'),
};
