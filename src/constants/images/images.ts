import Config from 'react-native-config';
import {FoodLikeImages, IFoodLikeImages} from './foodlike/images';
import {AnotherImages, IAnotherImages} from './another/images';

interface IAllImages<T> {
  [Key: string]: T;
}
type TAllImages = IFoodLikeImages | IAnotherImages;

const AllImages: IAllImages<TAllImages> = {
  FoodLike: FoodLikeImages,
  Another: AnotherImages,
};

const PDImages = AllImages[Config.APP_NAME];
export default PDImages;
