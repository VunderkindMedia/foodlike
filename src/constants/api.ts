import Config from 'react-native-config';

export const BACK_API = Config.API_BACK_URL;
export const API_URL = Config.API_URL;
export const API_POSTER_SHOP_URL = Config.POSTER_SHOP_URL;
const API_TOKEN = Config.API_TOKEN;
const SMS_TOKEN = Config.SMS_TOKEN;
const SMS_ID = Config.SMS_ID;

export const API_SETTINGS = `${BACK_API}/settings/get`;
export const API_REVIEWS = (client_phone: string) =>
  `${BACK_API}/clients/get?client_phone=+${client_phone}`;
export const API_SEND_REVIEW = `${BACK_API}/clients/order_review`;
export const API_CANCEL_REVIEW = (client_phone: string, id: number) =>
  `${BACK_API}/clients/order_delete?client_phone=+${client_phone}&order_id=${id}`;
export const API_PAYMENT_TYPES = `${API_URL}settings.getPaymentMethods?token=${API_TOKEN}`;
export const API_CLIENT_SAVE_TOKEN = (
  token: string,
  os: string,
  phone: string,
) => `${BACK_API}/clients/add?client_phone=${phone}&token=${token}&os=${os}`;
export const API_CLIENT_GET = (id: number) =>
  `${API_URL}clients.getClient?token=${API_TOKEN}&client_id=${id}`;
export const API_CLIENT_FIND = (phone: string) =>
  `${API_URL}clients.getClients?token=${API_TOKEN}&phone=${phone}`;
export const API_CLIENT_CREATE = `${API_URL}clients.createClient?token=${API_TOKEN}`;
export const API_CLIENT_UPDATE = `${API_URL}clients.updateClient?token=${API_TOKEN}`;
export const API_SEND_SMS = (code: string, phone: string) =>
  `https://sms.ru/sms/send?api_id=${SMS_TOKEN}&to=${phone}&msg=Код: ${code}&json=1&from=${SMS_ID}`;
export const API_SEND_CALL = (phone: string) =>
  `https://sms.ru/code/call?phone=${phone}&api_id=4BCD67BD-38CF-3D89-F859-2F469CABCED8`;

export const API_PRODUCTS_GET = `${API_POSTER_SHOP_URL}/api/v2/product/list`;
export const API_PROMOTIONS_GET = `${API_URL}clients.getPromotions?token=${API_TOKEN}`;
export const API_CATEGORIES_GET = `${API_URL}menu.getCategories?token=${API_TOKEN}`;

export const API_ORDERS_CREATE = `${API_URL}incomingOrders.createIncomingOrder?token=${API_TOKEN}`;
export const API_ORDERS_GET = (client_phone: string) =>
  `${BACK_API}/orders/get?phone=${client_phone}`;
export const API_TRANSACTION_GET = (id: number) =>
  `${API_URL}dash.getTransaction?
    token=${API_TOKEN}&
    transaction_id=${id}&
    include_products=true&
    include_delivery=true`;
