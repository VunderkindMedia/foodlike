export const DEMO_PHONE = '+77777777777';
export const DEMO_CODE = '7777';

export const SIGNUP_TITLE = 'Укажите телефон';
export const SIGNUP_PHONE_TITLE = 'Номер телефона:';
export const SIGNUP_STEP2_TITLE =
  'Введите последние 4 цифры номера, с которого Вам поступит звонок';
export const SIGNUP_STEP2_TITLE_RECALL =
  'Введите последние 4 цифры номера, с которого Вам поступит новый звонок';
export const SIGNUP_STEP3_TITLE = 'Как вас зовут?';
export const SIGNUP_CODE_TITLE = 'Звонок поступит на номер ';
export const SIGNUP_NAME_TITLE = 'Имя';
export const SIGNUP_OFFER_START = 'Продолжая, вы соглашаетесь с';
export const SIGNUP_OFFER_END = 'Политикой конфиденциальности';
export const SIGNUP_STEP1_BUTTON_TITLE = 'Получить код';
export const SIGNUP_STEP2_BUTTON_TITLE = 'Продолжить';
export const SIGNUP_STEP3_BUTTON_TITLE = 'Продолжить';
export const SIGNUP_NAME_PLACEHOLDER = 'Иван';
export const WELCOME_BUTTON_TITLE = 'Продолжить';
export const WELCOME_TITLE = 'Доставим вкусно!';
export const WELCOME_SUBTITLE =
  'Оформите заказ за пару кликов, не выходя из дома.';
export const PHONE_MASK = '+7 (999) 999-99-99';
export const CODE_MASK = '9-9-9-9';
export const SIGNUP_RESEND_CODE = 'Позвонить повторно';
export const ERROR_BUTTON_TITLE = 'Повторить';
export const OFFLINE_BUTTON_TITLE = 'Попробовать снова';

export const CART_BUTTON_TITLE = 'Оформить заказ';
export const CART_EMPTY_SUBTITLE =
  'Добавьте понравившийся товар из меню, чтобы оформить заказ';
export const CART_EMPTY_TITLE = 'В корзине пусто';

export const ORDER_CREATE_BUTTON_TITLE = 'Оформить заказ';
export const ORDER_CREATE_ADDRESS_TITLE = 'Адрес доставки';
export const ORDER_CREATE_ADDRESS_BUTTON_TITLE = 'Выбрать адрес доставки';
export const ADDRESS_STREET_LABEL = 'Улица, дом';
export const ADDRESS_APART_LABEL = 'Кв./Офис';
export const ADDRESS_ENTRANCE_LABEL = 'Подъезд';
export const ADDRESS_FLOOR_LABEL = 'Этаж';
export const ADDRESS_ADD_NEW = 'Добавить новый адрес';
export const ADDRESS_LIST_BUTTON_TITLE = 'Сохранить';

export const EMPTY_GO_TO_MENU = 'Перейти в меню';

export const ORDER_COMMENT_TITLE = 'Комментарий к заказу';

export const ORDER_CREATE_DELIVERY_TIME_TITLE = 'Дата и время доставки';
export const ORDER_CREATE_USER_INFO_TITLE = 'Информация о получателе';
export const ORDER_CREATE_USER_INFO_CHECKBOX = 'Заказ для другого человека';
export const ORDER_CREATE_USER_NAME_TITLE = 'Имя';
export const ORDER_CREATE_USER_PHONE_TITLE = 'Телефон';
export const ORDER_CREATE_PAY_TYPE_TITLE = 'Способ оплаты';
export const ORDER_CREATE_NOT_RECALL = 'Не перезванивать мне';
export const ORDER_CREATE_NOT_RECALL_MSG =
  'Оператор позвонит вам в случае, если потребуется внести изменения в заказ или скорректировать время доставки';
export const ORDER_CREATE_USE_BONUSES = 'Списать бонусы';
export const ORDER_CREATE_TABLEWARE_COUNT = 'Количество приборов';

export const PRODUCT_ADD_CART_TITLE = 'Добавить';
export const PRODUCT_BONUSES_TITLE = 'Бонусы: ';
export const PRODUCT_DESCRIPTION_TITLE = 'Состав';
