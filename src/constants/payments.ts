import {PaymentTypes} from '../model/payment';

export const DELIVERY_PRICE = 100;

export const lockedPayTypes = [
  PaymentTypes.Certificate,
  PaymentTypes.Deposit,
  PaymentTypes.Custom,
];
