import {StyleSheet} from 'react-native';

export const globalStyles = StyleSheet.create({
  fieldErrorText: {
    marginTop: 4,
    color: 'red',
    marginLeft: 4,
    fontFamily: 'SFProDisplay-Regular',
  },
});
