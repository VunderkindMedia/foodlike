import {ReactText} from 'react';
interface defaultColors {
  [key: string]: string | ReactText[];
}
export interface IAnotherColors extends defaultColors {
  transparent: string;
  primary: string;
  green: string;
  'lime-green': string;
  purple: string;
  blue: string;
  red: string;
  orange: string;
  yellow: string;
  'navy-blue': string;
  'mint-blue': string;
  pink: string;
  borderDefault: string;
  textBlack: string;
  textDark: string;
  lightGrayText: string;
  lightestText: string;
  textWhite: string;
  normalGrayText: string;
  disabledBackground: string;
  buttonLightGray: string;
  errorRed: string;
  whiteBackground: string;
  lightBackground: string;
  mintGray: string;
  backgroundGradient: ReactText[];
  backgroundLightPrimary: string;
  brightOrange: string;
  defaultBackground: string;
  lightDefaultBackground: string;
}
export const AnotherColors: IAnotherColors = {
  transparent: 'transparent',
  primary: '#F43B5B',
  green: '#A6EAA9',
  'lime-green': '#A3E7E7',
  purple: '#EAB8EB',
  blue: '#BEDBEB',
  red: '#FBB6BA',
  orange: '#FBCFAF',
  yellow: '#ECDE93',
  'navy-blue': '#A6CCE7',
  'mint-blue': '#A3E7E7',
  pink: '#FDBBC1',
  borderDefault: '#e2e2e2',
  textBlack: '#000',
  textDark: '#8f8f8f',
  lightestText: '#d7dadb',
  lightGrayText: '#BFC0C0',
  normalGrayText: '#7c7c7c',
  textWhite: '#fff',
  disabledBackground: '#c7c7c7',
  buttonLightGray: '#D9DDDF',
  errorRed: 'red',
  whiteBackground: '#fcfcfc',
  lightBackground: '#F2F2F2',
  mintGray: '#555e58',
  backgroundGradient: ['#FF5270', '#FF6752', '#F43B5B'],
  backgroundLightPrimary: '#F5A8B5',
  brightOrange: 'orange',
  defaultBackground: '#e5e5e5',
  lightDefaultBackground: '#e8e8e8',
};
