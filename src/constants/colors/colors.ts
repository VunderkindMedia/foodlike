import Config from 'react-native-config';
import {FoodLikeColors, IFoodLikeColors} from './foodlike/colors';
import {AnotherColors, IAnotherColors} from './another/colors';

interface IAllColors<T> {
  [Key: string]: T;
}
type TAllColors = IFoodLikeColors | IAnotherColors;

const AllColors: IAllColors<TAllColors> = {
  FoodLike: FoodLikeColors,
  Another: AnotherColors,
};

const PDColors = AllColors[Config.APP_NAME];
export default PDColors;
