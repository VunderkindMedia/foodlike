import {randomNumber} from '../helpers/randomizers';

export const randomColorForCat = () => {
  const colors = [
    'green',
    'lime-green',
    'purple',
    'blue',
    'red',
    'orange',
    'yellow',
    'navy-blue',
    'mint-blue',
    'pink',
  ];
  return colors[randomNumber(9, 1)];
};
