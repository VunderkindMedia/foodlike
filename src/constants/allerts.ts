import {useCallback} from 'react';
import {Product} from '../model/product';
import {ALERT_TYPE, Dialog} from 'react-native-alert-notification';

export const twoButtonAlert = (
  title: string,
  msg: string,
  titleOk: string,
  okPressed: () => void,
) =>
  Dialog.show({
    type: ALERT_TYPE.WARNING,
    title: title,
    textBody: msg,
    button: titleOk,
    onPressButton: () => {
      okPressed();
      Dialog.hide();
    },
  });

export const CartRemoveAlert = () => {
  return useCallback((onClick: (item: Product) => void, item: Product) => {
    return twoButtonAlert(
      'Удалить?',
      'Вы действительно хотите удалить данный товар?',
      'Удалить',
      () => onClick(item),
    );
  }, []);
};
export const CartCleanAlert = () => {
  return useCallback((onClick: () => void) => {
    return twoButtonAlert(
      'Очистить?',
      'Вы действительно хотите удалить все товары из корзины?',
      'Очистить',
      () => onClick(),
    );
  }, []);
};
export const LogoutAlert = () => {
  return useCallback((onClick: () => void) => {
    return twoButtonAlert(
      'Выйти из профиля?',
      'Вы действительно выйти из профиля?',
      'Выйти',
      () => onClick(),
    );
  }, []);
};
