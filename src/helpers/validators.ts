import {DEMO_CODE, DEMO_PHONE} from '../constants/strings';

export const isDemo = (code: string, phone: string) => {
  return code.replace(/\D/g, '') === DEMO_CODE && phone === DEMO_PHONE;
};
