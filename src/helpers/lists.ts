export const paginate = <T extends {}>(
  array: T[],
  page_size: number,
  page_number: number,
) => array.slice(0, page_number * page_size);

export const getUniqueBy = <T extends {[key: string]: any}>(
  arr: T[],
  prop: string,
) => {
  const set = new Set<T>();
  return arr.filter(o => !set.has(o[prop]) && set.add(o[prop]));
};
