import moment from 'moment';

export const stringCleaner = (value: string) => {
  return value.replace(/\D/g, '');
};

export const phoneFormatter = (phone: string) => {
  return '+' + stringCleaner(phone);
};

export const discount = (
  discount: number,
  price: number,
  count: number = 1,
) => {
  return (((price * discount) / 100) * count).toFixed(2).replace('.', ',');
};

export const dateToStringFormatter = (date: Date) => {
  if (moment(date).weekday() === moment().weekday()) {
    return moment(date).format('Сегодня в HH:mm');
  }
  return moment(date).format('DD-MM-yyyy в HH:mm');
};
