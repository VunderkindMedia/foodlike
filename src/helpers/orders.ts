export const orderStatusToText = (status: number, text_status: string) => {
  if (status === 0) {
    return 'обрабатывается';
  } else if (status === 1) {
    if (text_status === 'end') {
      return 'завершен';
    } else {
      return 'принят';
    }
  } else if (status === 7) {
    return 'отменен';
  }
};
