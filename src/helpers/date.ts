import {timeSettings} from '../constants/order';
import moment from 'moment-timezone';
moment.tz.setDefault('Asia/Sakhalin');

export const getAvailableDeliveryTime = (date: Date) => {
  const tzDate = moment(date);
  const checkedDay = tzDate.day();
  const checkedHour = tzDate.hour();
  const maxHour = timeSettings[checkedDay].max;
  const minHour = timeSettings[checkedDay].min;

  if (minHour < checkedHour) {
    //Уже открыты
    if (maxHour > checkedHour) {
      //Еще не закрыты
      return new Date(tzDate.valueOf());
    } else {
      tzDate
        .set('h', maxHour)
        .set('m', 0)
        .set('s', Math.floor(Math.random() * 60) + 1);
      return new Date(tzDate.valueOf());
      //Уже закрыты
    }
  } else {
    tzDate
      .set('h', minHour + 1)
      .set('m', 0)
      .set('s', Math.floor(Math.random() * 10) + 1);
    return new Date(tzDate.valueOf());
    //Еще не открыты
  }
};

export const correctDateMinutesByValue = (minutes: number, date: Date) => {
  const dateMin = date.getMinutes();
  if (dateMin % 5) {
    date.setMinutes(dateMin + 5 - (dateMin % 5));
  }
  return date;
};
