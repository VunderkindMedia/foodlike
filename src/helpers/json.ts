export function getClone<T>(obj: any): T {
  return JSON.parse(JSON.stringify(obj)) as T;
}

export const privateReplacer = (key: string, value: any) => {
  if (key[0] === '_') {
    return undefined;
  } else {
    return value;
  }
};
