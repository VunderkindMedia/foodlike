import {useDispatch} from 'react-redux';
import {useCallback} from 'react';
import {
  addFavorite,
  getProductsAndCategories,
  removeFavorite,
} from '../redux/products/actions';
import {Product} from '../model/product';

const useLoadProductsAndCats = () => {
  const dispatch = useDispatch();
  return useCallback(() => {
    dispatch(getProductsAndCategories());
  }, [dispatch]);
};

const useAddFavorite = () => {
  const dispatch = useDispatch();
  return useCallback(
    (product: Product) => {
      dispatch(addFavorite(product));
    },
    [dispatch],
  );
};
const useRemoveFavorite = () => {
  const dispatch = useDispatch();
  return useCallback(
    (product: Product) => {
      dispatch(removeFavorite(product));
    },
    [dispatch],
  );
};

export {useLoadProductsAndCats, useAddFavorite, useRemoveFavorite};
