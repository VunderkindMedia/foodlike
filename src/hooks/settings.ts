import {useDispatch} from 'react-redux';
import {
  cancelReview,
  getReviewOrder,
  getSettings,
  sendReview,
  setIntro,
} from '../redux/settings/actions';
import {useCallback} from 'react';
import {IReviewInitial} from '../screens/review/review';

const useLoadSettings = () => {
  const dispatch = useDispatch();
  return useCallback(() => {
    dispatch(getSettings());
  }, [dispatch]);
};
const useLoadReviewOrder = () => {
  const dispatch = useDispatch();
  return useCallback(() => {
    dispatch(getReviewOrder());
  }, [dispatch]);
};

const useSetIntro = () => {
  const dispatch = useDispatch();
  return useCallback(
    (intro: boolean) => {
      dispatch(setIntro(intro));
    },
    [dispatch],
  );
};

const useSendReview = () => {
  const dispatch = useDispatch();
  return useCallback(
    (values: IReviewInitial) => {
      dispatch(sendReview(values));
    },
    [dispatch],
  );
};

const useCancelReview = () => {
  const dispatch = useDispatch();
  return useCallback(() => {
    dispatch(cancelReview());
  }, [dispatch]);
};

export {
  useLoadSettings,
  useSetIntro,
  useLoadReviewOrder,
  useSendReview,
  useCancelReview,
};
