import {useCallback} from 'react';
import {useDispatch} from 'react-redux';
import {
  addAddress,
  checkAddress,
  clearClient,
  createClient,
  findClient,
  removeAddress,
  saveClientToken,
  updateClient,
} from '../redux/account/actions';
import {Dispatch} from 'redux';
import {Client} from '../model/client';
import {StorageManager} from '../storage-manager';
import {Address} from '../model/address';

const useGetClientRemote = () => {
  const dispatch = useDispatch();
  return useCallback(
    (client: Client) => {
      const phone = client.phone;
      dispatch(findClient(phone));
    },
    [dispatch],
  );
};
const useLogin = () => {
  const dispatch = useDispatch();
  return useCallback(
    (phone: string) => {
      return dispatch(async (dispatch: Dispatch) => {
        await findClient(phone)(dispatch);
      });
    },
    [dispatch],
  );
};

const useLogout = () => {
  const dispatch = useDispatch();
  return useCallback(() => {
    dispatch(clearClient());
    StorageManager.setLocalClient(null);
  }, [dispatch]);
};

const useFindClient = () => {
  const dispatch = useDispatch();
  return useCallback(
    (phone: string) => {
      dispatch(findClient(phone));
    },
    [dispatch],
  );
};

const useCreateClient = () => {
  const dispatch = useDispatch();
  return useCallback(
    (client: Client) => {
      dispatch(createClient(client));
    },
    [dispatch],
  );
};

const useUpdateClient = () => {
  const dispatch = useDispatch();
  return useCallback(
    (client: Client, cb: () => void) => {
      dispatch(updateClient(client, cb));
    },
    [dispatch],
  );
};

const useAddAddress = () => {
  const dispatch = useDispatch();
  return useCallback(
    (address: Address) => {
      dispatch(addAddress(address));
    },
    [dispatch],
  );
};
const useRemoveAddress = () => {
  const dispatch = useDispatch();
  return useCallback(
    (address: Address) => {
      dispatch(removeAddress(address));
    },
    [dispatch],
  );
};
const useCheckAddress = () => {
  const dispatch = useDispatch();
  return useCallback(
    (address: Address) => {
      dispatch(checkAddress(address));
    },
    [dispatch],
  );
};
const useSaveToken = () => {
  const dispatch = useDispatch();
  return useCallback(
    (token: string, os: string) => {
      dispatch(saveClientToken(token, os));
    },
    [dispatch],
  );
};

export {
  useGetClientRemote,
  useLogin,
  useFindClient,
  useCreateClient,
  useLogout,
  useAddAddress,
  useRemoveAddress,
  useCheckAddress,
  useSaveToken,
  useUpdateClient,
};
