import {useCallback} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {setNavigation} from '../redux/navigation/actions';
import {NavigationContainerRef} from '@react-navigation/native';
import {IAppState} from '../redux/root-reducer';

const useSetNavigation = () => {
  const dispatch = useDispatch();
  return useCallback(
    (navigationRef: NavigationContainerRef<ReactNavigation.RootParamList>) => {
      dispatch(setNavigation(navigationRef));
    },
    [dispatch],
  );
};

const useNavigationRef = () =>
  useSelector((state: IAppState) => state.navigation.ref);

export {useSetNavigation, useNavigationRef};
