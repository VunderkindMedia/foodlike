import {useCallback, useEffect, useState} from 'react';
import {runLongNetworkOperation} from '../redux/request/actions';
import {useDispatch} from 'react-redux';
import {api} from '../api';
import useCountDown from 'react-countdown-hook';
import {getRandomCode} from '../helpers/randomizers';
import {useSmsLeftTime} from '../redux/sms/selectors';
import {setLeftTime} from '../redux/sms/actions';
import {CallResponse} from '../api/sms';

const useSendSMS = () => {
  const dispatch = useDispatch();
  return useCallback(
    (code: string, phone: string) => {
      runLongNetworkOperation(async () => {
        await api.sms.send(code, phone);
      })(dispatch, 'sms_query');
    },
    [dispatch],
  );
};

const useSendCall = () => {
  const dispatch = useDispatch();
  return useCallback(
    (phone: string, onResponse: (response: CallResponse) => void) => {
      runLongNetworkOperation(async () => {
        const response = await api.sms.call(phone);
        onResponse(response);
      })(dispatch, 'call_query');
    },
    [dispatch],
  );
};

const useSendTimer = (): [number, () => void] => {
  const dispatch = useDispatch();
  const initialTime = 30 * 1000; // 60 сек
  const interval = 1000; //
  const reduxLeftTime = useSmsLeftTime();
  const [leftTimer, {start}] = useCountDown(initialTime, interval);
  useEffect(() => {
    dispatch(setLeftTime(leftTimer));
  }, [dispatch, leftTimer, reduxLeftTime]);

  return [reduxLeftTime, start];
};

const useRandomCode = (): [string, () => void] => {
  const [data, setData] = useState('');
  const getRandomHandler = useCallback(() => {
    setData(getRandomCode());
  }, []);
  return [data, getRandomHandler];
};

export {useSendSMS, useSendTimer, useRandomCode, useSendCall};
