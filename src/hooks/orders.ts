import {useDispatch} from 'react-redux';
import {useCallback} from 'react';
import {create, get, getTransactions} from '../redux/orders/actions';
import {Product} from '../model/product';
import {IPDCreateOrderForm} from '../screens/cart/form/form-options';
import {Transaction} from '../model/transaction';

const useCreateOrder = () => {
  const dispatch = useDispatch();
  return useCallback(
    (
      values: IPDCreateOrderForm,
      products: Product[],
      onSuccess: (result: any) => void,
    ) => {
      dispatch(create(values, products, onSuccess));
    },
    [dispatch],
  );
};

const useGetOrders = () => {
  const dispatch = useDispatch();
  return useCallback(() => {
    dispatch(get());
  }, [dispatch]);
};
const useGetTransactions = () => {
  const dispatch = useDispatch();
  return useCallback(
    (id: number, onSuccess: (transaction: Transaction) => void) => {
      dispatch(getTransactions(id, onSuccess));
    },
    [dispatch],
  );
};

export {useCreateOrder, useGetOrders, useGetTransactions};
