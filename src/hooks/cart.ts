import {useDispatch} from 'react-redux';
import {useCallback} from 'react';
import {Product} from '../model/product';
import {add, clean, decrement, increment, remove} from '../redux/cart/actions';

const useCartIncrement = () => {
  const dispatch = useDispatch();
  return useCallback(
    (product: Product) => {
      dispatch(increment(product));
    },
    [dispatch],
  );
};
const useCartDecrement = () => {
  const dispatch = useDispatch();
  return useCallback(
    (product: Product) => {
      dispatch(decrement(product));
    },
    [dispatch],
  );
};
const useCartRemove = () => {
  const dispatch = useDispatch();
  return useCallback(
    (product: Product) => {
      dispatch(remove(product));
    },
    [dispatch],
  );
};
const useCartAdd = () => {
  const dispatch = useDispatch();
  return useCallback(
    (product: Product) => {
      dispatch(add(product));
    },
    [dispatch],
  );
};

const useCartClean = () => {
  const dispatch = useDispatch();
  return useCallback(() => {
    dispatch(clean());
  }, [dispatch]);
};

export {
  useCartIncrement,
  useCartDecrement,
  useCartRemove,
  useCartAdd,
  useCartClean,
};
