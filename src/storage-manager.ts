import {Store} from 'redux';
import AsyncStorage, {
  AsyncStorageStatic,
} from '@react-native-async-storage/async-storage';
import {
  setAddresses,
  setCheckedAddress,
  setStorageClient,
} from './redux/account/actions';
import {setCartList} from './redux/cart/actions';
import {setLoader} from './redux/storage/actions';
import {Address, IAddressJson} from './model/address';
import {Client} from './model/client';
import {Product} from './model/product';
import {setFavoritesProducts} from './redux/products/actions';
import {setIntro} from './redux/settings/actions';

export class StorageManager {
  private static storage: AsyncStorageStatic = AsyncStorage;
  static async init(store: Store) {
    const dispatch = store.dispatch;

    const allPromises = Promise.all([
      this.storage.getItem('storageClient').then(client => {
        console.log('Профиль загружен из локального хранилища');
        client && JSON.parse(client)
          ? dispatch(setStorageClient(JSON.parse(client)))
          : dispatch(setStorageClient(undefined));
      }),
      this.storage.getItem('favorites').then(favorites => {
        console.log('Избранные товары загружены из локального хранилища');
        favorites && dispatch(setFavoritesProducts(JSON.parse(favorites)));
      }),
      this.storage.getItem('cart').then(cart => {
        console.log('Корзина загружена из локального хранилища');
        cart && dispatch(setCartList(JSON.parse(cart)));
      }),
      this.storage.getItem('addresses').then(addresses => {
        console.log('Адреса загружены из локального хранилища');
        addresses &&
          dispatch(
            setAddresses(
              (JSON.parse(addresses) || []).map((address: IAddressJson) =>
                Address.parse(address),
              ),
            ),
          );
      }),
      this.storage.getItem('checked_address').then(id => {
        console.log('Выбранный адрес загружен из локального хранилища');
        id && dispatch(setCheckedAddress(id));
      }),
      this.storage.getItem('intro').then(intro => {
        console.log('Флаг интро загружен из локального хранилища');
        intro && dispatch(setIntro(JSON.parse(intro)));
      }),
    ]);
    allPromises.then(() => {
      dispatch(setLoader(false));
    });
  }
  static setLocalClient(client: Client | null) {
    this.storage.setItem('storageClient', JSON.stringify(client));
  }
  static setFavorites(products: Product[]) {
    this.storage.setItem('favorites', JSON.stringify(products));
  }
  static setCart(products: Product[]) {
    this.storage.setItem('cart', JSON.stringify(products));
  }
  static setAddresses(addresses: Address[]) {
    this.storage.setItem('addresses', JSON.stringify(addresses));
  }
  static setCheckedAddress(id: string) {
    this.storage.setItem('checked_address', id);
  }
  static async setIntro(intro: boolean) {
    await this.storage.setItem('intro', JSON.stringify(intro));
  }
}
