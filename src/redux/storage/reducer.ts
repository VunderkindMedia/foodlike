import {Reducer} from 'redux';
import {SET_STORAGE_LOADER, TStorageLoaderActionTypes} from './action-types';

export interface IStorageReducerState {
  loading: boolean;
}

const initialState: IStorageReducerState = {
  loading: true,
};

export const storage: Reducer<IStorageReducerState> = (
  state = initialState,
  action: TStorageLoaderActionTypes,
) => {
  switch (action.type) {
    case SET_STORAGE_LOADER: {
      return {...state, loading: action.payload};
    }
    default:
      return state;
  }
};
