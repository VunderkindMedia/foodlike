import {useSelector} from 'react-redux';
import {IAppState} from '../root-reducer';

const useStorageLoading = () =>
  useSelector((state: IAppState) => state.storage.loading);

export {useStorageLoading};
