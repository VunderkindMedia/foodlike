import {SET_STORAGE_LOADER} from './action-types';

export const setLoader = (value: boolean) => ({
  type: SET_STORAGE_LOADER,
  payload: value,
});
