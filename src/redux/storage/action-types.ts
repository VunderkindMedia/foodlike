import {AnyAction} from 'redux';

export const SET_STORAGE_LOADER = 'SET_STORAGE_LOADER';

export interface ISetStorageLoaderAction {
  type: typeof SET_STORAGE_LOADER;
  payload: boolean;
}

export type TStorageLoaderActionTypes = ISetStorageLoaderAction | AnyAction;
