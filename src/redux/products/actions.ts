import {Product} from '../../model/product';
import {
  SET_CATEGORIES,
  SET_FAVORITES_PRODUCTS,
  SET_PRODUCTS,
} from './action-types';
import {Dispatch} from 'redux';
import {runLongNetworkOperation} from '../request/actions';
import {api} from '../../api';
import {Category} from '../../model/category';
import {IAppState} from '../root-reducer';
import {StorageManager} from '../../storage-manager';
import moment from 'moment-timezone';
import {setCartList} from '../cart/actions';
import {getUniqueBy} from '../../helpers/lists';

export const setProducts = (products: Product[]) => ({
  type: SET_PRODUCTS,
  payload: products,
});
export const setCategories = (categories: Category[]) => ({
  type: SET_CATEGORIES,
  payload: categories,
});
export const setFavoritesProducts = (products: Product[]) => ({
  type: SET_FAVORITES_PRODUCTS,
  payload: products,
});

export const getProductsAndCategories =
  () =>
  async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
    await runLongNetworkOperation(
      async () => {
        console.log('Загрузка категорий и товаров');
        const responseCats = await api.products.getCategories();
        let products = await api.products.getProducts();
        const promotions = await api.products.getPromotions();
        const currentDate = moment().tz('Asia/Sakhalin').valueOf();
        const cartList = getState().cart.list;
        const favoritesList = getState().products.favorites;
        let actualCartList: Product[];
        let actualFavoritesList: Product[];

        products = products.map(product => {
          promotions.forEach(promo => {
            if (
              promo.params.conditions.some(
                condition => condition.id === product.pos_id.toString(),
              ) &&
              moment(promo.date_start)
                .utc(true)
                .utcOffset('+0800')
                .isBefore(currentDate) &&
              moment(promo.date_end)
                .utc(true)
                .utcOffset('+0800')
                .isAfter(currentDate)
            ) {
              const discountValue = +promo.params.discount_value / 100;
              product.discountPrice =
                product.price - product.price * discountValue;
            }
          });

          return product;
        });

        actualCartList = cartList.filter(cart_item =>
          products.find(product => product.pos_id === cart_item.pos_id),
        );
        actualFavoritesList = favoritesList.filter(favorite_item =>
          products.find(product => product.pos_id === favorite_item.pos_id),
        );

        const sortedCatsIds = getUniqueBy(products, 'categoryPosId').map(
          prod => prod.categoryPosId,
        );
        const categories: Category[] = [];
        sortedCatsIds.forEach(catId => {
          const category = responseCats.find(cat => cat.category_id === catId);
          if (category) {
            categories.push(category);
          }
        });
        dispatch(setCartList(actualCartList));
        StorageManager.setCart(actualCartList);
        dispatch(setFavoritesProducts(actualFavoritesList));
        StorageManager.setFavorites(actualFavoritesList);
        dispatch(setProducts(products));
        dispatch(setCategories(categories));
      },
      error => console.log('Ошибка загрузки категорий: ', error),
    )(dispatch, 'categories');
  };

export const addFavorite =
  (product: Product) =>
  (dispatch: Dispatch, getState: () => IAppState): void => {
    const cloneFavorites = [...getState().products.favorites];
    cloneFavorites.push(product);
    dispatch(setFavoritesProducts(cloneFavorites));
    StorageManager.setFavorites(cloneFavorites);
  };

export const removeFavorite =
  (product: Product) =>
  (dispatch: Dispatch, getState: () => IAppState): void => {
    const cloneFavorites = getState().products.favorites.filter(
      prod => prod.pos_id !== product.pos_id,
    );
    dispatch(setFavoritesProducts(cloneFavorites));
    StorageManager.setFavorites(cloneFavorites);
  };
