import {AnyAction} from 'redux';
import {Product} from '../../model/product';
import {Category} from '../../model/category';

export const SET_PRODUCTS = 'SET_PRODUCTS';
export const SET_FAVORITES_PRODUCTS = 'SET_FAVORITES_PRODUCTS';
export const SET_CATEGORIES = 'SET_CATEGORIES';

export interface ISetProductsAction {
  type: typeof SET_PRODUCTS;
  payload: Product[];
}

export interface ISetCategoriesAction {
  type: typeof SET_CATEGORIES;
  payload: Category[];
}

export interface ISetFavoritesProductsAction {
  type: typeof SET_FAVORITES_PRODUCTS;
  payload: Product[];
}

export type TProductsActionTypes =
  | ISetProductsAction
  | ISetCategoriesAction
  | ISetFavoritesProductsAction
  | AnyAction;
