import {useSelector} from 'react-redux';
import {IAppState} from '../root-reducer';

const useProducts = () => useSelector((state: IAppState) => state.products.all);
const useFavorites = () =>
  useSelector((state: IAppState) => state.products.favorites);
const useCategories = () =>
  useSelector((state: IAppState) => state.products.categories);

export {useProducts, useFavorites, useCategories};
