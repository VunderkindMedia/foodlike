import {Reducer} from 'redux';
import {Product} from '../../model/product';
import {
  SET_CATEGORIES,
  SET_FAVORITES_PRODUCTS,
  SET_PRODUCTS,
  TProductsActionTypes,
} from './action-types';
import {Category} from '../../model/category';

export interface IProductsReducerState {
  all: Product[];
  favorites: Product[];
  categories: Category[];
}
const initialState: IProductsReducerState = {
  all: [],
  favorites: [],
  categories: [],
};

export const products: Reducer<IProductsReducerState> = (
  state = initialState,
  action: TProductsActionTypes,
) => {
  switch (action.type) {
    case SET_PRODUCTS:
      return {...state, all: action.payload};
    case SET_FAVORITES_PRODUCTS:
      return {...state, favorites: action.payload};
    case SET_CATEGORIES:
      return {...state, categories: action.payload};
    default:
      return state;
  }
};
