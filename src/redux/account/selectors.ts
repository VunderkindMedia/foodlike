import {useSelector} from 'react-redux';
import {IAppState} from '../root-reducer';

const useClient = () => useSelector((state: IAppState) => state.account.client);
const useStorageClient = () =>
  useSelector((state: IAppState) => state.account.storage_client);
const useLoginStatus = () =>
  useSelector((state: IAppState) => state.account.status);
const useAddresses = () =>
  useSelector((state: IAppState) => state.account.addresses);
const useCheckedAddress = () =>
  useSelector((state: IAppState) => state.account.checked_address);
const useToken = () =>
  useSelector((state: IAppState) => state.account.notification_token);

export {
  useClient,
  useLoginStatus,
  useStorageClient,
  useAddresses,
  useCheckedAddress,
  useToken,
};
