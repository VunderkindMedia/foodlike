import {
  SET_ADDRESSES,
  SET_CLIENT,
  SET_LOGIN_STATUS,
  SET_STORAGE_CLIENT,
  SET_CHECKED_ADDRESS,
  SET_NOTIFICATION_TOKEN,
  TClientActionTypes,
} from './action-types';
import {Reducer} from 'redux';
import {Client} from '../../model/client';
import {LoginStatusTypes} from '../../constants/account';
import {Address} from '../../model/address';

export interface IAccountReducerState {
  client: Client | null;
  status: LoginStatusTypes;
  addresses: Address[];
  checked_address: string;
  storage_client: Client | null | undefined;
  notification_token: string;
}
const initialState: IAccountReducerState = {
  client: null,
  status: LoginStatusTypes.isEmpty,
  addresses: [],
  checked_address: '',
  storage_client: undefined,
  notification_token: '',
};

export const account: Reducer<IAccountReducerState> = (
  state = initialState,
  action: TClientActionTypes,
) => {
  switch (action.type) {
    case SET_CLIENT:
      return {...state, client: action.payload};
    case SET_STORAGE_CLIENT:
      return {...state, storage_client: action.payload};
    case SET_ADDRESSES:
      return {...state, addresses: action.payload};
    case SET_CHECKED_ADDRESS:
      return {...state, checked_address: action.payload};
    case SET_LOGIN_STATUS:
      return {...state, status: action.payload};
    case SET_NOTIFICATION_TOKEN:
      return {...state, notification_token: action.payload};
    default:
      return state;
  }
};
