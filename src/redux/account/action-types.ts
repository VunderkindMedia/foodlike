import {AnyAction} from 'redux';
import {Client} from '../../model/client';
import {LoginStatusTypes} from '../../constants/account';
import {Address} from '../../model/address';

export const SET_CLIENT = 'SET_CLIENT';
export const SET_STORAGE_CLIENT = 'SET_STORAGE_CLIENT';
export const SET_LOGIN_STATUS = 'SET_LOGIN_STATUS';
export const SET_ADDRESSES = 'SET_ADDRESSES';
export const SET_CHECKED_ADDRESS = 'SET_CHECKED_ADDRESS';
export const SET_NOTIFICATION_TOKEN = 'SET_NOTIFICATION_TOKEN';

export interface ISetClientAction {
  type: typeof SET_CLIENT;
  payload: Client | null;
}
export interface ISetStorageClientAction {
  type: typeof SET_STORAGE_CLIENT;
  payload: Client | null;
}
export interface ISetLoginAction {
  type: typeof SET_LOGIN_STATUS;
  payload: LoginStatusTypes;
}
export interface ISetAddressesAction {
  type: typeof SET_ADDRESSES;
  payload: Address[];
}
export interface ISetCheckedAddressAction {
  type: typeof SET_CHECKED_ADDRESS;
  payload: string;
}
export interface ISetNotificationTokenAction {
  type: typeof SET_NOTIFICATION_TOKEN;
  payload: string;
}

export type TClientActionTypes =
  | ISetClientAction
  | ISetLoginAction
  | ISetAddressesAction
  | ISetCheckedAddressAction
  | ISetStorageClientAction
  | ISetNotificationTokenAction
  | AnyAction;
