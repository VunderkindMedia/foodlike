import {Dispatch} from 'redux';
import {runLongNetworkOperation} from '../request/actions';
import {api} from '../../api';
import {
  SET_ADDRESSES,
  SET_CHECKED_ADDRESS,
  SET_CLIENT,
  SET_LOGIN_STATUS,
  SET_NOTIFICATION_TOKEN,
  SET_STORAGE_CLIENT,
} from './action-types';
import {Client} from '../../model/client';
import {LoginStatusTypes} from '../../constants/account';
import {Address} from '../../model/address';
import {StorageManager} from '../../storage-manager';
import {IAppState} from '../root-reducer';

export const setClient = (client: Client | null) => ({
  type: SET_CLIENT,
  payload: client,
});
export const setStorageClient = (client: Client | undefined | null) => {
  return {
    type: SET_STORAGE_CLIENT,
    payload: client,
  };
};
export const setLoginStatus = (status: LoginStatusTypes) => ({
  type: SET_LOGIN_STATUS,
  payload: status,
});
export const setAddresses = (addresses: Address[]) => ({
  type: SET_ADDRESSES,
  payload: addresses,
});
export const setNotificationToken = (token: string) => ({
  type: SET_NOTIFICATION_TOKEN,
  payload: token,
});

export const setCheckedAddress = (id: string) => ({
  type: SET_CHECKED_ADDRESS,
  payload: id,
});

export const getClient =
  (id: number) =>
  async (dispatch: Dispatch): Promise<Client | null> => {
    let client: Client | null = null;
    await runLongNetworkOperation(async () => {
      const response = await api.client.get(id);
      if (response) {
        client = response;
        dispatch(setClient(response));
      }
    })(dispatch, 'client_get');
    return client;
  };

export const findClient =
  (phone: string) =>
  async (dispatch: Dispatch): Promise<Client | null> => {
    let client: Client | null = null;
    await runLongNetworkOperation(
      async () => {
        const client = await api.client.find(phone.replace(/[^0-9]/g, ''));
        if (client) {
          await dispatch(setLoginStatus(LoginStatusTypes.isFounded));
          await dispatch(setClient(client));
          StorageManager.setLocalClient(client);
        } else {
          await dispatch(setLoginStatus(LoginStatusTypes.isNotFounded));
        }
      },
      error => console.log('Ошибка получения профиля пользователя', error),
    )(dispatch, 'client_find');
    return client;
  };

export const createClient =
  (client: Client) =>
  async (dispatch: Dispatch): Promise<void> => {
    let tempClient: Client | null = null;
    await runLongNetworkOperation(async () => {
      const new_client_id = await api.client.create(client);
      if (new_client_id) {
        const client_response = await api.client.get(new_client_id);
        if (client_response) {
          tempClient = client_response;
          dispatch(setClient(tempClient));
          StorageManager.setLocalClient(tempClient);
          dispatch(setStorageClient(client));
        }
        dispatch(setLoginStatus(LoginStatusTypes.isCreated));
      }
    })(dispatch, 'client_create');
  };

export const updateClient =
  (client: Client, cb: () => void) =>
  async (dispatch: Dispatch): Promise<void> => {
    let tempClient: Client | null = null;
    await runLongNetworkOperation(async () => {
      const new_client_id = await api.client.update(client);
      if (new_client_id) {
        const client_response = await api.client.get(new_client_id);
        if (client_response) {
          tempClient = client_response;
          dispatch(setClient(tempClient));
          StorageManager.setLocalClient(tempClient);
          dispatch(setStorageClient(tempClient));
        }
      }
      cb();
    })(dispatch, 'client_update');
  };

export const clearClient =
  () =>
  (dispatch: Dispatch): void => {
    dispatch(setClient(null));
  };

export const addAddress =
  (address: Address) =>
  (dispatch: Dispatch, getState: () => IAppState): void => {
    const addresses = [...getState().account.addresses, address];
    dispatch(setAddresses(addresses));
    dispatch(setCheckedAddress(address.id));
    StorageManager.setAddresses(addresses);
    StorageManager.setCheckedAddress(address.id);
  };
export const removeAddress =
  (address: Address) =>
  (dispatch: Dispatch, getState: () => IAppState): void => {
    const addresses = getState().account.addresses.filter(
      item => item.id !== address.id,
    );
    if (addresses.length > 0) {
      dispatch(setAddresses(addresses));
      StorageManager.setAddresses(addresses);
      dispatch(setCheckedAddress(addresses[0].id));
      StorageManager.setCheckedAddress(addresses[0].id);
    }
    if (addresses.length === 0) {
      dispatch(setAddresses(addresses));
      StorageManager.setAddresses(addresses);
      dispatch(setCheckedAddress(''));
      StorageManager.setCheckedAddress('');
    }
  };

export const checkAddress =
  (address: Address) =>
  (dispatch: Dispatch): void => {
    dispatch(setCheckedAddress(address.id));
    StorageManager.setCheckedAddress(address.id);
  };

export const saveClientToken =
  (token: string, os: string) =>
  async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
    const phone = `+${getState().account.client?.phone_number}`;
    if (!phone) {
      return;
    }
    await runLongNetworkOperation(async () => {
      await api.client.save_token(token, os, phone);
    })(dispatch, 'save_token');
  };
