import {useSelector} from 'react-redux';
import {IAppState} from '../root-reducer';
import {RequestStates} from '../../model/request-states';

interface IErrorRequest {
  [key: string]: RequestStates;
}
const useRequestState = (requestKey: string) =>
  useSelector((state: IAppState) => state.request.keys[requestKey]);

const useRequestStateIsLoading = (requestKey: string) =>
  useSelector((state: IAppState) => state.request.keys[requestKey]) ===
  RequestStates.Processing;

const useRequestStateIsSuccess = (requestKey: string) =>
  useSelector((state: IAppState) => state.request.keys[requestKey]) ===
  RequestStates.Succeeded;

const useRequestStates = () =>
  useSelector((state: IAppState) => state.request.keys);

const useRequestsError = (): IErrorRequest[] => {
  const errorRequests: IErrorRequest[] = [];
  const requestKeys = useSelector((state: IAppState) => state.request.keys);
  Object.keys(requestKeys).forEach(key => {
    if (requestKeys[key] === RequestStates.Failed) {
      errorRequests.push({[key]: requestKeys[key]});
    }
  });
  return errorRequests;
};

export {
  useRequestState,
  useRequestStates,
  useRequestsError,
  useRequestStateIsLoading,
  useRequestStateIsSuccess,
};
