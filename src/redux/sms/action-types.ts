import {AnyAction} from 'redux';

export const SET_SMS_LEFT_TIME = 'SET_SMS_LEFT_TIME';

export interface ISetSmsLeftTimeAction {
  type: typeof SET_SMS_LEFT_TIME;
  payload: number;
}

export type TSmsLeftTimeActionTypes = ISetSmsLeftTimeAction | AnyAction;
