import {Reducer} from 'redux';
import {SET_SMS_LEFT_TIME, TSmsLeftTimeActionTypes} from './action-types';

export interface ISmsReducerState {
  left_time: number;
}

const initialState: ISmsReducerState = {
  left_time: 0,
};

export const sms: Reducer<ISmsReducerState> = (
  state = initialState,
  action: TSmsLeftTimeActionTypes,
) => {
  switch (action.type) {
    case SET_SMS_LEFT_TIME: {
      return {...state, left_time: action.payload};
    }
    default:
      return state;
  }
};
