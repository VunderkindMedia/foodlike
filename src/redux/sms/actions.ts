import {SET_SMS_LEFT_TIME} from './action-types';

export const setLeftTime = (value: number) => ({
  type: SET_SMS_LEFT_TIME,
  payload: value,
});
