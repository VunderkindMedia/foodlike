import {useSelector} from 'react-redux';
import {IAppState} from '../root-reducer';

const useSmsLeftTime = () =>
  useSelector((state: IAppState) => state.sms.left_time);

export {useSmsLeftTime};
