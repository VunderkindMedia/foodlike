import {Reducer} from 'redux';
import {SET_ORDERS, TOrdersActionTypes} from './action-types';
import {Order} from '../../model/order';

export interface IOrdersReducerState {
  list: Order[];
}
const initialState: IOrdersReducerState = {
  list: [],
};

export const orders: Reducer<IOrdersReducerState> = (
  state = initialState,
  action: TOrdersActionTypes,
) => {
  switch (action.type) {
    case SET_ORDERS:
      return {...state, list: action.payload};
    default:
      return state;
  }
};
