import {Dispatch} from 'redux';
import {Product} from '../../model/product';
import {IAppState} from '../root-reducer';
import {IPDCreateOrderForm} from '../../screens/cart/form/form-options';
import {runLongNetworkOperation} from '../request/actions';
import {api} from '../../api';
import {OrderRequest} from '../../model/order_request';
import {Order} from '../../model/order';
import {SET_ORDERS} from './action-types';
import {Transaction} from '../../model/transaction';

export const setOrders = (orders: Order[]) => ({
  type: SET_ORDERS,
  payload: orders,
});

export const create =
  (
    values: IPDCreateOrderForm,
    products: Product[],
    onSuccess: (result: any) => void,
  ) =>
  async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
    const name = getState().account.client?.lastname.length
      ? `${getState().account.client?.firstname} ${
          getState().account.client?.lastname
        }`
      : `${getState().account.client?.firstname}`;

    const phone = '+' + getState().account.client?.phone_number!;
    const pay_title =
      getState().settings.all.payment_types.find(
        pay => pay.payment_method_id === values.payment_type!,
      )?.title ?? 'Оплата картой курьеру';

    const address = getState().account.addresses.find(
      address => address.id === getState().account.checked_address,
    );
    const request = OrderRequest.Builder.withClientAddress(address)
      .withDeliveryTime(values.delivery_time)
      .withProducts(products)
      .withUserComment(values.comment)
      .withBonuses(values.use_bonuses)
      .withRecall(values.recall)
      .withPaymentType(pay_title)
      .withTablewareCount(values.tableware_count)
      .withName(name)
      .withPhone(phone)
      .withUserName(values.user_name)
      .withUserPhone(values.user_phone)
      .build();
    await runLongNetworkOperation(
      async () => {
        const response = await api.orders.create(request);
        const json = await response.json();
        onSuccess(json);
      },
      error => console.log('Ошибка создания заказа: ', error),
    )(dispatch, 'orders_create');
  };

export const get =
  () =>
  async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
    const client_phone = getState().account.client?.phone_number ?? '0';
    await runLongNetworkOperation(
      async () => {
        const orders = await api.orders.get(client_phone);
        dispatch(setOrders(orders));
      },
      error => console.log('Ошибка получения заказов: ', error),
    )(dispatch, 'orders_get');
  };
export const getTransactions =
  (id: number, onSuccess: (transaction: Transaction) => void) =>
  async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
    await runLongNetworkOperation(
      async () => {
        const transaction = await api.orders.getTransaction(
          id,
          getState().products.all,
        );
        onSuccess(transaction);
      },
      error => console.log('Ошибка получения чека: ', error),
    )(dispatch, 'transaction_get');
  };
