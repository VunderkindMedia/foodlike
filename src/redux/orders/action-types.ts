import {AnyAction} from 'redux';
import {Order} from '../../model/order';

export const SET_ORDERS = 'SET_ORDERS';

export interface ISetOrdersListAction {
  type: typeof SET_ORDERS;
  payload: Order[];
}

export type TOrdersActionTypes = ISetOrdersListAction | AnyAction;
