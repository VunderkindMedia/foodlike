import {useSelector} from 'react-redux';
import {IAppState} from '../root-reducer';

const useOrders = () => useSelector((state: IAppState) => state.orders.list);

export {useOrders};
