import {Reducer} from 'redux';
import {
  SET_SETTINGS,
  SET_INTRO,
  SET_REVIEW_ORDER,
  TSettingsActionTypes,
} from './action-types';
import {Settings} from '../../model/settings';
import {Order} from '../../model/order';

export interface ISettingsReducerState {
  all: Settings;
  intro: boolean;
  review_order: Order | null;
}

const initialState: ISettingsReducerState = {
  all: new Settings(),
  intro: false,
  review_order: null,
};

export const settings: Reducer<ISettingsReducerState> = (
  state = initialState,
  action: TSettingsActionTypes,
) => {
  switch (action.type) {
    case SET_SETTINGS: {
      return {...state, all: action.payload};
    }
    case SET_INTRO: {
      return {...state, intro: action.payload};
    }
    case SET_REVIEW_ORDER: {
      return {...state, review_order: action.payload};
    }
    default:
      return state;
  }
};
