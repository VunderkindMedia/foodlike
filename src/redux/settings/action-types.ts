import {AnyAction} from 'redux';
import {Settings} from '../../model/settings';
import {Order} from '../../model/order';

export const SET_SETTINGS = 'SET_SETTINGS';
export const SET_INTRO = 'SET_INTRO';
export const SET_REVIEW_ORDER = 'SET_REVIEW_ORDER';

export interface ISetSettingsAction {
  type: typeof SET_SETTINGS;
  payload: Settings;
}

export interface ISetIntroAction {
  type: typeof SET_INTRO;
  payload: boolean;
}

export interface ISetReviewOrderAction {
  type: typeof SET_REVIEW_ORDER;
  payload: Order | null;
}

export type TSettingsActionTypes =
  | ISetSettingsAction
  | ISetIntroAction
  | AnyAction;
