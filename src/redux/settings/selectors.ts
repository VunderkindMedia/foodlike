import {useSelector} from 'react-redux';
import {IAppState} from '../root-reducer';

const useSettings = () => useSelector((state: IAppState) => state.settings.all);
const useIntro = () => useSelector((state: IAppState) => state.settings.intro);
const useReviewOrder = () =>
  useSelector((state: IAppState) => state.settings.review_order);

export {useSettings, useIntro, useReviewOrder};
