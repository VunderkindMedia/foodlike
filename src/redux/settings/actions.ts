import {Dispatch} from 'redux';
import {runLongNetworkOperation} from '../request/actions';
import {api} from '../../api';
import {Settings} from '../../model/settings';
import {SET_INTRO, SET_REVIEW_ORDER, SET_SETTINGS} from './action-types';
import {IAppState} from '../root-reducer';
import {Order} from '../../model/order';
import {IReviewInitial} from '../../screens/review/review';

export const setSettings = (settings: Settings) => ({
  type: SET_SETTINGS,
  payload: settings,
});

export const setIntro = (intro: boolean) => ({
  type: SET_INTRO,
  payload: intro,
});

export const setReviewOrder = (order: Order | null) => ({
  type: SET_REVIEW_ORDER,
  payload: order,
});

export const getSettings =
  () =>
  async (dispatch: Dispatch): Promise<void> => {
    await runLongNetworkOperation(
      async () => {
        console.log('Загрузка настроек');
        const backendSettings = await api.settings.getSettings();
        const payments = await api.settings.getPaymentTypes();
        dispatch(setSettings({...backendSettings, payment_types: payments}));
      },
      error => console.log('Ошибка загрузки настроек: ', error),
    )(dispatch, 'settings');
  };
export const getReviewOrder =
  () =>
  async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
    await runLongNetworkOperation(
      async () => {
        console.log('Загрузка заказа для отзыва');
        const reviewOrder = await api.settings.getReview(
          getState().account.client?.phone_number!,
        );
        dispatch(setReviewOrder(reviewOrder));
      },
      error => console.log('Ошибка загрузки заказа для отзыва: ', error),
    )(dispatch, 'review_order');
  };
export const sendReview =
  (values: IReviewInitial) =>
  async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
    if (!getState().settings.review_order?.id) {
      return;
    }
    await runLongNetworkOperation(
      async () => {
        console.log('Отправка отзыва');
        await api.settings.sendReview(
          values,
          getState().settings.review_order?.incoming_order_id!,
          getState().account.client?.phone_number!,
        );
      },
      error => console.log('Ошибка отправки отзыва: ', error),
    )(dispatch, 'review_send');
  };

export const cancelReview =
  () =>
  async (dispatch: Dispatch, getState: () => IAppState): Promise<void> => {
    await runLongNetworkOperation(
      async () => {
        console.log('Отмена отзыва');
        await api.settings.sendReview(
          undefined,
          getState().settings.review_order?.incoming_order_id!,
          getState().account.client?.phone_number!,
        );
      },
      error => console.log('Ошибка отмены отзыва: ', error),
    )(dispatch, 'review_send');
  };
