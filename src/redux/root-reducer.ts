import {combineReducers, Reducer} from 'redux';
import {INavigationReducerState, navigation} from './navigation/reducer';
import {IRequestReducerState, request} from './request/reducer';
import {ISettingsReducerState, settings} from './settings/reducer';
import {account, IAccountReducerState} from './account/reducer';
import {cart, ICartReducerState} from './cart/reducer';
import {IStorageReducerState, storage} from './storage/reducer';
import {ISmsReducerState, sms} from './sms/reducer';
import {IProductsReducerState, products} from './products/reducer';
import {orders, IOrdersReducerState} from './orders/reducer';
export interface IAppState {
  navigation: INavigationReducerState;
  request: IRequestReducerState;
  settings: ISettingsReducerState;
  account: IAccountReducerState;
  cart: ICartReducerState;
  storage: IStorageReducerState;
  sms: ISmsReducerState;
  products: IProductsReducerState;
  orders: IOrdersReducerState;
}

export const createRootReducer = (): Reducer<IAppState> =>
  combineReducers<IAppState>({
    navigation,
    request,
    settings,
    account,
    cart,
    storage,
    sms,
    products,
    orders,
  });
