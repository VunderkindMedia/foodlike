import {SET_NAVIGATION_REF} from './action-types';
import {NavigationContainerRef} from '@react-navigation/native';

export const setNavigation = (
  navigationRef: NavigationContainerRef<ReactNavigation.RootParamList>,
) => ({
  type: SET_NAVIGATION_REF,
  payload: navigationRef,
});
