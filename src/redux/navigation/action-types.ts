import {NavigationContainerRefWithCurrent} from '@react-navigation/native';
import {AnyAction} from 'redux';

export const SET_NAVIGATION_REF = 'SET_NAVIGATION_REF';

export interface ISetNavigationAction {
  type: typeof SET_NAVIGATION_REF;
  payload: NavigationContainerRefWithCurrent<ReactNavigation.RootParamList> | null;
}

export type TNavigationActionTypes = ISetNavigationAction | AnyAction;
