import {useSelector} from 'react-redux';
import {IAppState} from '../root-reducer';

const useNavigationRef = () =>
  useSelector((state: IAppState) => state.navigation.ref);

export {useNavigationRef};
