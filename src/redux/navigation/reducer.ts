import {SET_NAVIGATION_REF, TNavigationActionTypes} from './action-types';
import {Reducer} from 'redux';
import {NavigationContainerRefWithCurrent} from '@react-navigation/native';
export interface INavigationReducerState {
  ref: NavigationContainerRefWithCurrent<ReactNavigation.RootParamList> | null;
}
const initialState: INavigationReducerState = {
  ref: null,
};

export const navigation: Reducer<INavigationReducerState> = (
  state = initialState,
  action: TNavigationActionTypes,
) => {
  switch (action.type) {
    case SET_NAVIGATION_REF:
      return {...state, ref: action.payload};
    default:
      return state;
  }
};
