import {AnyAction} from 'redux';
import {Product} from '../../model/product';

export const SET_CART_LIST = 'SET_CART_LIST';
export const SET_CART_COUNT = 'SET_CART_COUNT';

export interface ISetCartListAction {
  type: typeof SET_CART_LIST;
  payload: Product[];
}
export interface ISetCartCountAction {
  type: typeof SET_CART_COUNT;
  payload: number;
}

export type TCartActionTypes =
  | ISetCartListAction
  | ISetCartCountAction
  | AnyAction;
