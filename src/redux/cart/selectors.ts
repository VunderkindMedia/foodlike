import {useSelector} from 'react-redux';
import {IAppState} from '../root-reducer';
import {Product} from '../../model/product';

const useCartCount = () =>
  useSelector((state: IAppState) => state.cart.list).reduce(
    (w, item) => w + item.cartCount,
    0,
  );
const useCartSum = () =>
  useSelector((state: IAppState) => state.cart.list).reduce((w, item) => {
    if (item.discountPrice) {
      return w + item.discountPrice * item.cartCount;
    } else {
      return w + item.price * item.cartCount;
    }
  }, 0);

const useCartDiscountSum = () =>
  useSelector((state: IAppState) => state.cart.list).reduce((w, item) => {
    if (item.discountPrice) {
      return w + (item.price - item.discountPrice) * item.cartCount;
    } else {
      return 0;
    }
  }, 0);

const useCartItemSum = (product: Product) => {
  const cartList = useSelector((state: IAppState) => state.cart.list);
  const productIndex = cartList.findIndex(
    item => item.product_id === product.product_id,
  );
  if (productIndex === -1) {
    return;
  }
  const inCartProd = cartList[productIndex];
  if (inCartProd.discountPrice) {
    return inCartProd.discountPrice * inCartProd.cartCount;
  } else {
    return inCartProd.price * inCartProd.cartCount;
  }
};

const useCartList = () => useSelector((state: IAppState) => state.cart.list);

export {
  useCartCount,
  useCartSum,
  useCartItemSum,
  useCartList,
  useCartDiscountSum,
};
