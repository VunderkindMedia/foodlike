import {SET_CART_LIST, TCartActionTypes} from './action-types';
import {Reducer} from 'redux';
import {Product} from '../../model/product';

export interface ICartReducerState {
  list: Product[];
}
const initialState: ICartReducerState = {
  list: [],
};

export const cart: Reducer<ICartReducerState> = (
  state = initialState,
  action: TCartActionTypes,
) => {
  switch (action.type) {
    case SET_CART_LIST:
      return {...state, list: action.payload};
    default:
      return state;
  }
};
