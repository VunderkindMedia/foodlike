import {Dispatch} from 'redux';
import {SET_CART_COUNT, SET_CART_LIST} from './action-types';
import {Product} from '../../model/product';
import {IAppState} from '../root-reducer';
import {getClone} from '../../helpers/json';
import {StorageManager} from '../../storage-manager';

export const setCartList = (cartList: Product[]) => ({
  type: SET_CART_LIST,
  payload: cartList,
});
export const setCartCount = (count: number) => ({
  type: SET_CART_COUNT,
  payload: count,
});

export const add =
  (product: Product) =>
  (dispatch: Dispatch, getState: () => IAppState): void => {
    const productClone = product.clone;
    productClone.cartCount = 1;
    const cartClone = getClone<Product[]>(getState().cart.list);
    cartClone.push(productClone);
    dispatch(setCartList(cartClone));
    StorageManager.setCart(cartClone);
  };

export const remove =
  (product: Product) =>
  (dispatch: Dispatch, getState: () => IAppState): void => {
    let cartClone = getClone<Product[]>(getState().cart.list);
    cartClone = cartClone.filter(
      item => item.product_id !== product.product_id,
    );
    dispatch(setCartList(cartClone));
    StorageManager.setCart(cartClone);
  };

export const increment =
  (product: Product, count: number = 1) =>
  (dispatch: Dispatch, getState: () => IAppState): void => {
    let cartClone = getClone<Product[]>(getState().cart.list);
    const itemIndex = cartClone.findIndex(
      item => item.product_id === product.product_id,
    );
    if (itemIndex === -1) {
      return;
    }
    cartClone[itemIndex].cartCount += count;
    dispatch(setCartList(cartClone));
    StorageManager.setCart(cartClone);
  };

export const decrement =
  (product: Product, count: number = 1) =>
  (dispatch: Dispatch, getState: () => IAppState): void => {
    let cartClone = getClone<Product[]>(getState().cart.list);
    const itemIndex = cartClone.findIndex(
      item => item.product_id === product.product_id,
    );
    if (itemIndex === -1) {
      return;
    }
    if (cartClone[itemIndex].cartCount !== 0) {
      cartClone[itemIndex].cartCount -= count;
      dispatch(setCartList(cartClone));
      StorageManager.setCart(cartClone);
    } else {
      remove(product);
    }
  };

export const clean =
  () =>
  (dispatch: Dispatch): void => {
    dispatch(setCartList([]));
    StorageManager.setCart([]);
  };
