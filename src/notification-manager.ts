import messaging, {
  FirebaseMessagingTypes,
} from '@react-native-firebase/messaging';
import {setNotificationToken} from './redux/account/actions';
import {Store} from 'redux';
import PushNotification from 'react-native-push-notification';

export class NotificationManager {
  requestUserPermission = async (
    store: Store,
  ): Promise<FirebaseMessagingTypes.AuthorizationStatus> => {
    const dispatch = store.dispatch;
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;
    if (enabled) {
      const token = await this.getFcmToken();
      dispatch(setNotificationToken(token));
      console.log('Authorization status:', authStatus);
    }
    return authStatus;
  };

  getFcmToken = async (): Promise<string> => {
    const fcmToken = await messaging().getToken();
    if (fcmToken) {
      console.log('Your Firebase Token is:', fcmToken);
    } else {
      console.log('Failed', 'No token received');
    }
    return fcmToken;
  };

  foregroundNotificationHandlers = async () => {
    return messaging().onMessage(async remoteMessage => {
      console.log('A new FCM message arrived!', JSON.stringify(remoteMessage));
      if (!remoteMessage.notification) {
        return;
      }
      this.showNotification(remoteMessage.notification!);
    });
  };

  backgroundNotificationHandlers = async () =>
    messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log('Message handled in the background!', remoteMessage);
    });

  showNotification = (notification: FirebaseMessagingTypes.Notification) => {
    PushNotification.localNotification({
      channelId: 'foodlike',
      title: notification.title,
      message: notification.body!,
    });
  };
}
