import React from 'react';

import PDColors from '../../../constants/colors/colors';
import {createStackNavigator} from '@react-navigation/stack';
import {
  Routes,
  SCREEN_NAME_ADDRESSES,
  SCREEN_NAME_NEW_ADDRESS,
  SCREEN_NAME_ORDERS,
} from '../../constants/routes';
import {forSlideX, forSlideY} from '../../animations';
import {PDProfile} from '../../../screens/profile/PDProfile';
import {PDProfileEdit} from '../../../screens/profile/PDProfileEdit';
import {PDOrders} from '../../../screens/orders/PDOrders';
import {PDOrder} from '../../../screens/orders/PDOrder';
import {PDAddressList} from '../../../screens/profile/PDAddressList';
import {PDAddressCreate} from '../../../screens/profile/PDAddressCreate';

interface IPDProfileStackProps {
  route: any;
}
export const PDProfileStack = (props: IPDProfileStackProps) => {
  const {route} = props;
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: PDColors.whiteBackground,
        },
      }}
      detachInactiveScreens={false}
      initialRouteName={
        (route.params && route.params.screen) || Routes.SCREEN_PROFILE
      }>
      <Stack.Screen
        name={Routes.SCREEN_PROFILE}
        options={{
          headerMode: 'screen',
          headerShown: false,
          cardStyleInterpolator: forSlideY,
          cardStyle: {
            backgroundColor: PDColors.lightBackground,
          },
        }}
        component={PDProfile}
      />
      <Stack.Screen
        name={Routes.SCREEN_PROFILE_EDIT}
        options={{
          headerShown: false,
          gestureEnabled: true,
          presentation: 'modal',
          gestureResponseDistance: 2000,
          cardStyle: {
            backgroundColor: PDColors.transparent,
          },
        }}
        component={PDProfileEdit}
      />
      <Stack.Screen
        name={Routes.SCREEN_ORDERS}
        options={{
          title: SCREEN_NAME_ORDERS,
          cardStyleInterpolator: forSlideX,
          headerTitleStyle: {
            fontFamily: 'SFProDisplay-Bold',
            fontSize: 20,
          },
          cardStyle: {
            backgroundColor: PDColors.lightBackground,
          },
          headerTitleAlign: 'center',
        }}
        component={PDOrders}
      />
      <Stack.Screen
        name={Routes.SCREEN_ORDER}
        options={{
          title: '',
          cardStyleInterpolator: forSlideX,
          headerTitleStyle: {
            fontFamily: 'SFProDisplay-Bold',
            fontSize: 20,
          },
          cardStyle: {
            backgroundColor: PDColors.lightBackground,
          },
          headerTitleAlign: 'center',
        }}
        component={PDOrder}
      />
      <Stack.Screen
        name={Routes.SCREEN_ADDRESSES}
        options={{
          title: SCREEN_NAME_ADDRESSES,
          cardStyleInterpolator: forSlideX,
          headerTitleStyle: {
            fontFamily: 'SFProDisplay-Bold',
            fontSize: 20,
          },
          cardStyle: {
            backgroundColor: PDColors.lightBackground,
          },
          headerTitleAlign: 'center',
        }}
        component={PDAddressList}
      />
      <Stack.Screen
        name={Routes.SCREEN_ADDRESS_CREATE}
        options={{
          title: SCREEN_NAME_NEW_ADDRESS,
          cardStyleInterpolator: forSlideY,
          headerTitleStyle: {
            fontFamily: 'SFProDisplay-Bold',
            fontSize: 20,
          },
          cardStyle: {
            backgroundColor: PDColors.lightBackground,
          },
          headerTitleAlign: 'center',
        }}
        component={PDAddressCreate}
      />
    </Stack.Navigator>
  );
};
