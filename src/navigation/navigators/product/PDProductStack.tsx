import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import PDColors from '../../../constants/colors/colors';
import {
  Routes,
  SCREEN_NAME_CATEGORIES,
  SCREEN_NAME_FAVORITES,
  SCREEN_NAME_SEARCH,
} from '../../constants/routes';
import {forFade, forSlideX} from '../../animations';
import {PDCategories} from '../../../screens/products/PDCategories';
import {PDProducts} from '../../../screens/products/PDProducts';
import {PDProduct} from '../../../screens/products/PDProduct';
import {PDFavorites} from '../../../screens/products/PDFavorites';
import {PDSearch} from '../../../screens/products/PDSearch';
import {PDShop} from '../../../screens/shop/PDShop';
import {PDAddressList} from '../../../screens/profile/PDAddressList';
import {PDIntro} from '../../../screens/intro/intro';
import {getFocusedRouteNameFromRoute} from '@react-navigation/native';
import {tabStyle} from '../tabs/PDTabsNavigator';
import {PDReview} from '../../../screens/review/review';
import {PDOrder} from '../../../screens/orders/PDOrder';
import {PDReviewFinish} from '../../../screens/review/review_finish';
interface IPDProductStackProps {
  route: any;
  navigation: any;
}

const defaultOptions = {
  cardStyleInterpolator: forFade,
  headerTitleStyle: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 20,
  },
  headerStyle: {
    backgroundColor: PDColors.whiteBackground,
  },
  headerShadowVisible: false,
};

export const PDProductStack = (props: IPDProductStackProps) => {
  const {navigation, route} = props;
  useEffect(() => {
    const routeName = getFocusedRouteNameFromRoute(route);
    if (routeName === Routes.SCREEN_PRODUCT) {
      navigation.setOptions({
        tabBarStyle: [tabStyle, {display: 'none'}],
      });
    } else {
      navigation.setOptions({
        tabBarStyle: [tabStyle, {display: 'flex'}],
      });
    }
  }, [navigation, route]);
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: PDColors.whiteBackground,
        },
      }}
      detachInactiveScreens={false}
      initialRouteName={
        (route.params && route.params.screen) || Routes.SCREEN_CATEGORIES
      }>
      <Stack.Screen
        name={Routes.SCREEN_CATEGORIES}
        options={{
          headerShown: true,
          title: SCREEN_NAME_CATEGORIES,
          headerTitleAlign: 'center',
          ...defaultOptions,
        }}
        component={PDCategories}
      />
      <Stack.Screen
        name={Routes.SCREEN_PRODUCTS}
        options={{
          headerShown: true,
          title: '',
          headerTitleAlign: 'center',
          ...defaultOptions,
        }}
        component={PDProducts}
      />
      <Stack.Screen
        name={Routes.SCREEN_PRODUCT}
        options={{
          headerShown: false,
          gestureEnabled: true,
          presentation: 'modal',
          gestureResponseDistance: 2000,
          cardStyle: {
            backgroundColor: 'transparent',
          },
        }}
        component={PDProduct}
      />
      <Stack.Screen
        name={Routes.SCREEN_FAVORITES}
        options={{
          headerShown: true,
          headerTitleAlign: 'center',
          title: SCREEN_NAME_FAVORITES,
          ...defaultOptions,
          cardStyle: {
            backgroundColor: PDColors.transparent,
          },
        }}
        component={PDFavorites}
      />
      <Stack.Screen
        name={Routes.SCREEN_SEARCH}
        options={{
          headerShown: true,
          title: SCREEN_NAME_SEARCH,
          ...defaultOptions,
          // headerLeft: props => <HeaderButton {...props} />,
        }}
        component={PDSearch}
      />
      <Stack.Screen
        name={Routes.SCREEN_SHOP}
        options={{
          headerShown: false,
          ...defaultOptions,
        }}
        component={PDShop}
      />
      <Stack.Screen
        name={Routes.SCREEN_ADDRESSES}
        options={{
          headerShown: false,
          ...defaultOptions,
        }}
        component={PDAddressList}
      />
      <Stack.Screen
        name={Routes.SCREEN_INTRO}
        options={{
          headerShown: false,
          gestureEnabled: true,
          presentation: 'modal',
          gestureResponseDistance: 2000,
          cardStyle: {
            backgroundColor: 'transparent',
          },
        }}
        component={PDIntro}
      />
      <Stack.Screen
        name={Routes.SCREEN_REVIEW}
        options={{
          headerShown: false,
          ...defaultOptions,
        }}
        component={PDReview}
      />
      <Stack.Screen
        name={Routes.SCREEN_REVIEW_FINISH}
        options={{
          headerShown: false,
          ...defaultOptions,
        }}
        component={PDReviewFinish}
      />
      <Stack.Screen
        name={Routes.SCREEN_ORDER}
        options={{
          title: '',
          cardStyleInterpolator: forSlideX,
          headerTitleStyle: {
            fontFamily: 'SFProDisplay-Bold',
            fontSize: 20,
          },
          cardStyle: {
            backgroundColor: PDColors.lightBackground,
          },
          headerTitleAlign: 'center',
        }}
        component={PDOrder}
      />
    </Stack.Navigator>
  );
};
