import {Routes} from '../../constants/routes';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';
import {Product} from '../../../model/product';
import {Address} from '../../../model/address';
import {Tabs} from '../../constants/tabs';
import {Order} from '../../../model/order';

export type TProductNavigatorParamsList = {
  [Tabs.TAB_PRODUCTS]: undefined;
  [Routes.SCREEN_CATEGORIES]?: {parent_id?: number};
  [Routes.SCREEN_PRODUCTS]: {category_id: number; title: string};
  [Routes.SCREEN_PRODUCT]: {item: Product};
  [Routes.SCREEN_SEARCH]: undefined;
  [Routes.SCREEN_CART]: undefined;
  [Routes.SCREEN_CART_ORDER]: {products: Product[]};
  [Routes.SCREEN_ADDRESSES]: {
    onConfirm?: (address?: Address) => void;
  };
  [Routes.SCREEN_ADDRESS_CREATE]: undefined;
  [Routes.SCREEN_DELIVERY_TIME]: {
    value?: string;
    onConfirm?: (date?: string) => void;
  };
  [Routes.SCREEN_ORDER_FINISH]: {
    order: Order;
  };
  [Routes.SCREEN_ORDER]: {
    order: Order;
  };
  [Routes.SCREEN_ORDERS]: undefined;
};
export type TCategoriesNavigatorProp = StackNavigationProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_CATEGORIES
>;
export type TProductsNavigatorProp = StackNavigationProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_PRODUCTS
>;
export type TProductNavigatorProp = StackNavigationProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_PRODUCT
>;
export type TSearchNavigatorProp = StackNavigationProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_SEARCH
>;

export type TCategoriesRouteProp = RouteProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_CATEGORIES
>;
export type TProductsRouteProp = RouteProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_PRODUCTS
>;
export type TProductRouteProp = RouteProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_PRODUCT
>;
export type TSearchRouteProp = StackNavigationProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_SEARCH
>;

export type TCartNavigatorProp = StackNavigationProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_CART
>;

export type TOrderCreateNavigatorProp = StackNavigationProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_CART_ORDER
>;
export type TOrderCreateRouteProp = RouteProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_CART_ORDER
>;

export type TOrderAddressesNavigatorProp = StackNavigationProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_ADDRESSES
>;
export type TOrderAddressesRouteProp = RouteProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_ADDRESSES
>;

export type TOrderDeliveryTimeNavigatorProp = StackNavigationProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_DELIVERY_TIME
>;
export type TOrderDeliveryTimeRouteProp = RouteProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_DELIVERY_TIME
>;

export type TOrderNavigationProp = StackNavigationProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_ORDER
>;
export type TOrderRouteProp = RouteProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_ORDER
>;

export type TOrderFinishNavigationProp = StackNavigationProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_ORDER_FINISH
>;
export type TOrderFinishRouteProp = RouteProp<
  TProductNavigatorParamsList,
  Routes.SCREEN_ORDER_FINISH
>;
