import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import PDColors from '../../../constants/colors/colors';
import {
  Routes,
  SCREEN_NAME_ADDRESSES,
  SCREEN_NAME_CART,
  SCREEN_NAME_CREATE_ORDER,
  SCREEN_NAME_NEW_ADDRESS,
  SCREEN_NAME_ORDERS,
} from '../../constants/routes';
import {forFade, forSlideX, forSlideY} from '../../animations';
import {PDCart} from '../../../screens/cart/PDCart';
import {PDAddressCreate} from '../../../screens/profile/PDAddressCreate';
import {PDOrders} from '../../../screens/orders/PDOrders';
import {PDOrder} from '../../../screens/orders/PDOrder';
import {PDOrderFinish} from '../../../screens/cart/PDOrderFinish';
import {PDProduct} from '../../../screens/products/PDProduct';
import {PDAddressList} from '../../../screens/profile/PDAddressList';
import {PDCreateOrder} from '../../../screens/cart/PDCreateOrder';
import {PDDateTimePicker} from '../../../components/date-picker/date-picker';
import {getFocusedRouteNameFromRoute} from '@react-navigation/native';
import {tabStyle} from '../tabs/PDTabsNavigator';
interface IPDCartStackProps {
  route: any;
  navigation: any;
}
export const PDCartStack = (props: IPDCartStackProps) => {
  const {route, navigation} = props;
  const Stack = createStackNavigator();
  useEffect(() => {
    const routeName = getFocusedRouteNameFromRoute(route);
    if (
      routeName === Routes.SCREEN_ORDER_FINISH ||
      routeName === Routes.SCREEN_DELIVERY_TIME ||
      routeName === Routes.SCREEN_PRODUCT
    ) {
      navigation.setOptions({tabBarStyle: [tabStyle, {display: 'none'}]});
    } else {
      navigation.setOptions({tabBarStyle: [tabStyle, {display: 'flex'}]});
    }
  }, [navigation, route]);
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: PDColors.whiteBackground,
        },
      }}
      detachInactiveScreens={false}
      initialRouteName={
        (route.params && route.params.screen) || Routes.SCREEN_CART
      }>
      <Stack.Screen
        name={Routes.SCREEN_CART}
        options={{
          headerShown: true,
          title: SCREEN_NAME_CART,
          cardStyleInterpolator: forFade,
          headerTitleStyle: {
            fontFamily: 'SFProDisplay-Bold',
            fontSize: 20,
          },
          headerTitleAlign: 'center',
          headerShadowVisible: false,
          cardStyle: {
            backgroundColor: PDColors.transparent,
          },
        }}
        component={PDCart}
      />
      <Stack.Screen
        name={Routes.SCREEN_PRODUCT}
        options={{
          headerShown: false,
          gestureEnabled: true,
          presentation: 'modal',
          gestureResponseDistance: 2000,
          cardStyle: {
            backgroundColor: PDColors.transparent,
          },
        }}
        component={PDProduct}
      />
      <Stack.Screen
        name={Routes.SCREEN_CART_ORDER}
        options={{
          title: SCREEN_NAME_CREATE_ORDER,
          cardStyleInterpolator: forSlideX,
          headerTitleStyle: {
            fontFamily: 'SFProDisplay-Bold',
            fontSize: 20,
          },
          headerTitleAlign: 'center',
        }}
        component={PDCreateOrder}
      />
      <Stack.Screen
        name={Routes.SCREEN_DELIVERY_TIME}
        options={{
          headerShown: false,
          gestureEnabled: true,
          presentation: 'modal',
          gestureResponseDistance: 2000,
          cardStyle: {
            backgroundColor: 'transparent',
          },
        }}
        component={PDDateTimePicker}
      />
      <Stack.Screen
        name={Routes.SCREEN_ADDRESSES}
        options={{
          title: SCREEN_NAME_ADDRESSES,
          cardStyleInterpolator: forSlideY,
          headerTitleStyle: {
            fontFamily: 'SFProDisplay-Bold',
            fontSize: 20,
          },
          cardStyle: {
            backgroundColor: PDColors.lightBackground,
          },
          headerTitleAlign: 'center',
        }}
        component={PDAddressList}
      />
      <Stack.Screen
        name={Routes.SCREEN_ADDRESS_CREATE}
        options={{
          title: SCREEN_NAME_NEW_ADDRESS,
          cardStyleInterpolator: forSlideY,
          headerTitleStyle: {
            fontFamily: 'SFProDisplay-Bold',
            fontSize: 20,
          },
          cardStyle: {
            backgroundColor: PDColors.lightBackground,
          },
          headerTitleAlign: 'center',
        }}
        component={PDAddressCreate}
      />
      <Stack.Screen
        name={Routes.SCREEN_ORDER_FINISH}
        options={{
          cardStyleInterpolator: forSlideX,
          headerShown: false,
          headerTitleStyle: {
            fontFamily: 'SFProDisplay-Bold',
            fontSize: 20,
          },
          headerShadowVisible: false,
        }}
        component={PDOrderFinish}
      />
      <Stack.Screen
        name={Routes.SCREEN_ORDERS}
        options={{
          headerShown: true,
          title: SCREEN_NAME_ORDERS,
          cardStyleInterpolator: forSlideX,
          headerTitleStyle: {
            fontFamily: 'Gilroy_Bold',
            fontSize: 20,
          },
          headerShadowVisible: false,
        }}
        component={PDOrders}
      />
      <Stack.Screen
        name={Routes.SCREEN_ORDER}
        options={{
          title: '',
          headerShown: true,
          cardStyleInterpolator: forSlideX,
          headerTitleStyle: {
            fontFamily: 'Gilroy_Bold',
            fontSize: 20,
          },
          headerShadowVisible: false,
        }}
        component={PDOrder}
      />
    </Stack.Navigator>
  );
};
