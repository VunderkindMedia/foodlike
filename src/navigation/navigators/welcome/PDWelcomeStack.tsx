import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {useClient, useStorageClient} from '../../../redux/account/selectors';
import {useGetClientRemote} from '../../../hooks/account';
import {PDLoginStack} from '../login/PDLoginStack';
import {forRotateX} from '../../animations';
import PDColors from '../../../constants/colors/colors';
import {PDWelcome} from '../../../screens/welcome/PDWelcome';

import {PDTabsNavigator} from '../tabs/PDTabsNavigator';
import {Routes} from '../../constants/routes';
import SplashScreen from 'react-native-splash-screen';
import {TWelcomeNavigatorParamsList} from './types';
import {useRequestStateIsSuccess} from '../../../redux/request/selectors';

export const PDWelcomeStack = () => {
  const client = useClient();
  const storageClient = useStorageClient();
  const getClientRemote = useGetClientRemote();
  const clientRequest = useRequestStateIsSuccess('client_find');

  const Stack = createStackNavigator<TWelcomeNavigatorParamsList>();
  const screenOptions = {
    title: '',
    headerShown: false,
  };
  useEffect(() => {
    if (storageClient) {
      getClientRemote(storageClient);
    }
  }, [getClientRemote, storageClient]);

  useEffect(() => {
    if (clientRequest && !client) {
      SplashScreen.hide();
    } else if (storageClient === undefined) {
      SplashScreen.hide();
    }
  }, [client, clientRequest, storageClient]);

  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: PDColors.whiteBackground,
        },
      }}>
      {client ? (
        <Stack.Screen
          name={Routes.SCREEN_MAIN}
          component={PDTabsNavigator}
          options={screenOptions}
        />
      ) : (
        <>
          <Stack.Screen
            name={Routes.SCREEN_WELCOME}
            component={PDWelcome}
            options={{...screenOptions, cardStyleInterpolator: forRotateX}}
          />
          <Stack.Screen
            name={Routes.SCREEN_LOGIN}
            component={PDLoginStack}
            options={screenOptions}
          />
        </>
      )}
    </Stack.Navigator>
  );
};
