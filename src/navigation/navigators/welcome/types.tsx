import {Routes} from '../../constants/routes';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';

export type TWelcomeNavigatorParamsList = {
  [Routes.SCREEN_MAIN]: undefined;
  [Routes.SCREEN_WELCOME]: undefined;
  [Routes.SCREEN_LOGIN]: undefined;
};
export type TMainScreenNavigatorProp = StackNavigationProp<
  TWelcomeNavigatorParamsList,
  Routes.SCREEN_MAIN
>;
export type TWelcomeScreenNavigatorProp = StackNavigationProp<
  TWelcomeNavigatorParamsList,
  Routes.SCREEN_WELCOME
>;
export type TLoginScreenNavigatorProp = StackNavigationProp<
  TWelcomeNavigatorParamsList,
  Routes.SCREEN_LOGIN
>;

export type TWelcomeScreenRouteProp = RouteProp<
  TWelcomeNavigatorParamsList,
  Routes.SCREEN_WELCOME
>;
export type TMainScreenRouteProp = RouteProp<
  TWelcomeNavigatorParamsList,
  Routes.SCREEN_MAIN
>;
export type TLoginScreenRouteProp = RouteProp<
  TWelcomeNavigatorParamsList,
  Routes.SCREEN_LOGIN
>;
