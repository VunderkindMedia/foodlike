import {StyleProp, Text, TextStyle} from 'react-native';
import PDColors from '../../../../constants/colors/colors';
import {TAB_NAME_MAIN_SHOP} from '../../../constants/tabs';
import React from 'react';

export interface IPDLabelProps {
  style: StyleProp<TextStyle>;
  focused: boolean;
  title: string;
}

export const PDLabel = (props: IPDLabelProps) => {
  const {style, focused, title} = props;
  return (
    <Text
      style={[
        style,
        {
          color: focused ? PDColors.textBlack : PDColors.textDark,
        },
      ]}>
      {title}
    </Text>
  );
};
