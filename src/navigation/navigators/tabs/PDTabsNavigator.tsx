import React, {useEffect} from 'react';
import {StyleSheet} from 'react-native';
import {
  BottomTabNavigationOptions,
  createBottomTabNavigator,
} from '@react-navigation/bottom-tabs';

import PDColors from '../../../constants/colors/colors';

import {useCartCount, useCartSum} from '../../../redux/cart/selectors';
import {
  TAB_NAME_CART,
  TAB_NAME_CATALOG,
  TAB_NAME_FAVORITES,
  TAB_NAME_MAIN_SHOP,
  TAB_NAME_PROFILE,
  Tabs,
} from '../../constants/tabs';
import {Routes} from '../../constants/routes';
import {PDCartStack} from '../cart/PDCartStack';
import {PDProductStack} from '../product/PDProductStack';
import {PDProfileStack} from '../profile/PDProfileStack';
import SplashScreen from 'react-native-splash-screen';
import {PDIcons} from '../../../components/icons/custom-icons';
import {PDLabel} from './label/label';
import {useLoadProductsAndCats} from '../../../hooks/products';
import {useRequestStateIsSuccess} from '../../../redux/request/selectors';
import {useSaveToken} from '../../../hooks/account';
import {useToken} from '../../../redux/account/selectors';

export const PDTabsNavigator = () => {
  const cartCount = useCartCount();
  const cartSum = useCartSum();
  const setToken = useSaveToken();
  const token = useToken();
  const loadProductsAndCats = useLoadProductsAndCats();
  const categoriesRequest = useRequestStateIsSuccess('categories');
  const Tab = createBottomTabNavigator();
  const tabBarOptions: BottomTabNavigationOptions = {
    tabBarActiveTintColor: PDColors.primary,
    tabBarInactiveTintColor: PDColors.textDark,
    tabBarStyle: tabStyle,
    tabBarLabelStyle: styles.tab,
  };
  useEffect(() => {
    setToken(token, 'android');
  }, [setToken, token]);
  useEffect(() => {
    loadProductsAndCats();
  }, [loadProductsAndCats]);
  useEffect(() => {
    if (categoriesRequest) {
      SplashScreen.hide();
    }
  }, [categoriesRequest]);

  return (
    <Tab.Navigator screenOptions={tabBarOptions}>
      <Tab.Screen
        name={Tabs.TAB_SHOP}
        component={PDProductStack}
        initialParams={{screen: Routes.SCREEN_SHOP}}
        options={{
          headerShown: false,
          title: TAB_NAME_MAIN_SHOP,
          tabBarIcon: ({size, color}) => (
            <PDIcons name="home" size={size} color={color} />
          ),
          tabBarLabel: ({focused}) => (
            <PDLabel
              focused={focused}
              title={TAB_NAME_MAIN_SHOP}
              style={styles.tab}
            />
          ),
        }}
      />
      <Tab.Screen
        name={Tabs.TAB_PRODUCTS}
        component={PDProductStack}
        options={{
          headerShown: false,
          title: TAB_NAME_CATALOG,
          tabBarIcon: ({size, color}) => (
            <PDIcons name="menu" size={size} color={color} />
          ),
          tabBarLabel: ({focused}) => (
            <PDLabel
              focused={focused}
              title={TAB_NAME_CATALOG}
              style={styles.tab}
            />
          ),
        }}
      />
      <Tab.Screen
        name={Tabs.TAB_CART}
        component={PDCartStack}
        initialParams={{screen: Routes.SCREEN_CART}}
        options={{
          headerShown: false,
          title: TAB_NAME_CART,
          tabBarIcon: ({size, color}) => (
            <PDIcons name="cart" size={size} color={color} />
          ),
          tabBarLabel: ({focused}) => (
            <PDLabel
              focused={focused}
              title={cartCount ? `${cartSum} ₽` : TAB_NAME_CART}
              style={styles.tab}
            />
          ),
          tabBarBadge: cartCount !== 0 ? cartCount : undefined,
          tabBarBadgeStyle:
            cartCount !== 0 ? styles.cartTabStyle : {opacity: 0},
        }}
      />
      <Tab.Screen
        name={Tabs.TAB_FAVORITES}
        component={PDProductStack}
        initialParams={{screen: Routes.SCREEN_FAVORITES}}
        options={{
          headerShown: false,
          title: TAB_NAME_FAVORITES,
          tabBarIcon: ({size, color}) => (
            <PDIcons name="favorites" size={size} color={color} />
          ),
          tabBarLabel: ({focused}) => (
            <PDLabel
              focused={focused}
              title={TAB_NAME_FAVORITES}
              style={styles.tab}
            />
          ),
        }}
      />
      <Tab.Screen
        name={Tabs.TAB_PROFILE}
        component={PDProfileStack}
        initialParams={{screen: Routes.SCREEN_PROFILE}}
        options={{
          headerShown: false,
          title: TAB_NAME_PROFILE,
          tabBarIcon: ({size, color}) => (
            <PDIcons name="profile" size={size} color={color} />
          ),
          tabBarLabel: ({focused}) => (
            <PDLabel
              focused={focused}
              title={TAB_NAME_PROFILE}
              style={styles.tab}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};
const styles = StyleSheet.create({
  tab: {
    fontWeight: '800',
    fontSize: 11,
    fontFamily: 'SFProDisplay-Bold',
  },
  cartTabStyle: {
    backgroundColor: PDColors.primary,
    color: PDColors.textWhite,
    fontSize: 11,
    borderRadius: 5,
    top: 0,
  },
});

export const tabStyle = {
  paddingBottom: 18,
  paddingTop: 10,
  height: 70,

  borderColor: PDColors.borderDefault,
  borderWidth: 1,
  shadowColor: PDColors.mintGray,
  shadowOffset: {
    width: 2,
    height: -5,
  },
  shadowRadius: 10,
  shadowOpacity: 0.1,
  elevation: 0,
};
