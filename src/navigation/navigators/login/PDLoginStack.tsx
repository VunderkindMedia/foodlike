import React from 'react';
import {forFade, forRotateX} from '../../animations';
import {createStackNavigator} from '@react-navigation/stack';

import PDColors from '../../../constants/colors/colors';
import {Routes} from '../../constants/routes';
import {PDLoginStep1, PDLoginStep2, PDLoginStep3} from '../../../screens/login';
import {TLoginNavigatorParamsList} from './types';

export const PDLoginStack = () => {
  const defaultScreenOptions = {
    title: '',
    headerShown: false,
  };
  const Stack = createStackNavigator<TLoginNavigatorParamsList>();
  return (
    <Stack.Navigator
      screenOptions={{
        cardStyle: {
          backgroundColor: PDColors.whiteBackground,
        },
      }}>
      <Stack.Screen
        name={Routes.SCREEN_LOGIN_STEP_1}
        component={PDLoginStep1}
        options={{
          ...defaultScreenOptions,
          cardStyleInterpolator: forFade,
          headerShadowVisible: false,
        }}
      />
      <Stack.Screen
        name={Routes.SCREEN_LOGIN_STEP_2}
        component={PDLoginStep2}
        options={{...defaultScreenOptions, cardStyleInterpolator: forRotateX}}
      />
      <Stack.Screen
        name={Routes.SCREEN_LOGIN_STEP_3}
        component={PDLoginStep3}
        options={{...defaultScreenOptions, cardStyleInterpolator: forRotateX}}
      />
    </Stack.Navigator>
  );
};
