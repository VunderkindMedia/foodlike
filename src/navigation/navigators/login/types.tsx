import {Routes} from '../../constants/routes';
import {StackNavigationProp} from '@react-navigation/stack';
import {RouteProp} from '@react-navigation/native';

export type TLoginNavigatorParamsList = {
  [Routes.SCREEN_LOGIN_STEP_1]: undefined;
  [Routes.SCREEN_LOGIN_STEP_2]: {phone: string};
  [Routes.SCREEN_LOGIN_STEP_3]: {phone: string};
};
export type TLoginStep1NavigatorProp = StackNavigationProp<
  TLoginNavigatorParamsList,
  Routes.SCREEN_LOGIN_STEP_1
>;
export type TLoginStep2NavigatorProp = StackNavigationProp<
  TLoginNavigatorParamsList,
  Routes.SCREEN_LOGIN_STEP_2
>;
export type TLoginStep3NavigatorProp = StackNavigationProp<
  TLoginNavigatorParamsList,
  Routes.SCREEN_LOGIN_STEP_3
>;

export type TLoginStep1RouteProp = RouteProp<
  TLoginNavigatorParamsList,
  Routes.SCREEN_LOGIN_STEP_1
>;
export type TLoginStep2RouteProp = RouteProp<
  TLoginNavigatorParamsList,
  Routes.SCREEN_LOGIN_STEP_2
>;
export type TLoginStep3RouteProp = RouteProp<
  TLoginNavigatorParamsList,
  Routes.SCREEN_LOGIN_STEP_3
>;
