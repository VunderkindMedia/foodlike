import React, {useEffect} from 'react';
import PDToast from '../components/toast/toast';
import {
  NavigationContainer,
  useNavigationContainerRef,
} from '@react-navigation/native';
import {useSetNavigation} from '../hooks/navigation';
import {useRequestsError} from '../redux/request/selectors';
import {useLoadReviewOrder, useLoadSettings} from '../hooks/settings';
import {PDWelcomeStack} from './navigators/welcome/PDWelcomeStack';
import {useStorageLoading} from '../redux/storage/selectors';
import {ErrorView} from '../components/error-view/error-view';
import {useSettings} from '../redux/settings/selectors';
import {OfflineView} from '../components/error-view/offline-view';
import {Root} from 'react-native-alert-notification';

const PDAppNavigator = () => {
  const navigationRef = useNavigationContainerRef();
  const setNavigationRef = useSetNavigation();
  const errorRequests = useRequestsError();
  const loadSettings = useLoadSettings();
  const storageLoading = useStorageLoading();
  const settings = useSettings();
  useEffect(() => {
    loadSettings();
  }, [loadSettings]);
  useEffect(() => {
    setNavigationRef(navigationRef);
  }, [navigationRef, setNavigationRef]);
  return (
    <NavigationContainer ref={navigationRef}>
      <Root theme={'light'}>
        {!storageLoading &&
          !errorRequests.length &&
          settings.mobile_app_enabled && <PDWelcomeStack />}
        {errorRequests.length > 0 && <ErrorView clickHandler={loadSettings} />}
        {!settings.mobile_app_enabled && !errorRequests.length && (
          <OfflineView
            clickHandler={loadSettings}
            text={settings.mobile_app_disabled_text}
          />
        )}
      </Root>
      <PDToast />
    </NavigationContainer>
  );
};
export default PDAppNavigator;
