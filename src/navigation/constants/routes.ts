import {TAB_NAME_CART, TAB_NAME_CATALOG, TAB_NAME_FAVORITES} from './tabs';

export enum Routes {
  STACK_PRODUCTS = 'STACK_PRODUCTS',
  STACK_CART = 'STACK_CART',
  STACK_LOGIN = 'STACK_LOGIN',
  STACK_WELCOME = 'STACK_WELCOME',

  SCREEN_WELCOME = 'SCREEN_WELCOME',
  SCREEN_MAIN = 'SCREEN_MAIN',
  SCREEN_LOGIN = 'SCREEN_LOGIN',
  SCREEN_PROFILE = 'SCREEN_PROFILE',
  SCREEN_CATEGORIES = 'SCREEN_CATEGORIES',
  SCREEN_SHOP = 'SCREEN_SHOP',
  SCREEN_CART = 'SCREEN_CART',
  SCREEN_CART_ORDER = 'SCREEN_CART_ORDER',
  SCREEN_DELIVERY_TIME = 'SCREEN_DELIVERY_TIME',
  SCREEN_FAVORITES = 'SCREEN_FAVORITES',
  SCREEN_LOGIN_STEP_1 = 'SCREEN_LOGIN_STEP_1',
  SCREEN_LOGIN_STEP_2 = 'SCREEN_LOGIN_STEP_2',
  SCREEN_LOGIN_STEP_3 = 'SCREEN_LOGIN_STEP_3',
  SCREEN_PRODUCTS = 'SCREEN_PRODUCTS',
  SCREEN_PRODUCT = 'SCREEN_PRODUCT',
  SCREEN_SEARCH = 'SCREEN_SEARCH',
  SCREEN_PROFILE_EDIT = 'SCREEN_PROFILE_EDIT',
  SCREEN_ORDERS = 'SCREEN_ORDERS',
  SCREEN_ORDER = 'SCREEN_ORDER',
  SCREEN_ADDRESSES = 'SCREEN_ADDRESSES',
  SCREEN_ADDRESS_CREATE = 'SCREEN_ADDRESS_CREATE',
  SCREEN_ORDER_FINISH = 'SCREEN_ORDER_FINISH',
  SCREEN_INTRO = 'SCREEN_INTRO',
  SCREEN_REVIEW = 'SCREEN_REVIEW',
  SCREEN_REVIEW_FINISH = 'SCREEN_REVIEW_FINISH',
}

export const SCREEN_NAME_CART = TAB_NAME_CART;
export const SCREEN_NAME_CATEGORIES = TAB_NAME_CATALOG;
export const SCREEN_NAME_FAVORITES = TAB_NAME_FAVORITES;
export const SCREEN_NAME_SEARCH = 'Поиск';
export const SCREEN_NAME_PROFILE_EDIT = 'Редактирование профиля';
export const SCREEN_NAME_ORDERS = 'Мои заказы';
export const SCREEN_NAME_CREATE_ORDER = 'Оформление заказа';
export const SCREEN_NAME_ADDRESSES = 'Адреса доставки';
export const SCREEN_NAME_NEW_ADDRESS = 'Новый адрес';
