export const TAB_NAME_MAIN_SHOP = 'Главное';
export const TAB_NAME_CATALOG = 'Меню';
export const TAB_NAME_CART = 'Корзина';
export const TAB_NAME_FAVORITES = 'Любимое';
export const TAB_NAME_PROFILE = 'Профиль';

export enum Tabs {
  TAB_SHOP = 'TAB_SHOP',
  TAB_PRODUCTS = 'TAB_PRODUCTS',
  TAB_CART = 'TAB_CART',
  TAB_FAVORITES = 'TAB_FAVORITES',
  TAB_PROFILE = 'TAB_PROFILE',
}
