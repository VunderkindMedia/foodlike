import {StackCardInterpolationProps} from '@react-navigation/stack';

export const forFade = (props: StackCardInterpolationProps) => {
  const {current} = props;
  return {
    cardStyle: {
      opacity: current.progress,
    },
  };
};
export const forSlideY = (props: StackCardInterpolationProps) => {
  const {current, layouts} = props;
  return {
    cardStyle: {
      transform: [
        {
          translateY: current.progress.interpolate({
            inputRange: [0, 1],
            outputRange: [1, 0],
          }),
        },
      ],
    },
    overlayStyle: {
      backgroundColor: '#fff',
      opacity: current.progress.interpolate({
        inputRange: [0, 1],
        outputRange: [1, 0],
      }),
    },
  };
};

export const forSlideX = (props: StackCardInterpolationProps) => {
  const {current, layouts} = props;
  return {
    cardStyle: {
      opacity: current.progress,
      transform: [
        {
          translateX: current.progress.interpolate({
            inputRange: [0, 1],
            outputRange: [layouts.screen.width, 0],
          }),
        },
      ],
    },
    overlayStyle: {
      backgroundColor: 'transparent',
      opacity: current.progress,
    },
  };
};

export const forRotateX = (props: StackCardInterpolationProps) => {
  const {current, next} = props;
  return {
    cardStyle: {
      opacity: current.progress,
      transform: [
        {
          rotateY: next
            ? next.progress.interpolate({
                inputRange: [0, 0.5, 1],
                outputRange: ['0deg', '-90deg', '-180deg'],
              })
            : current.progress.interpolate({
                inputRange: [0, 0.5, 1],
                outputRange: ['180deg', '90deg', '0deg'],
              }),
        },
        // {
        //   scaleY: next
        //     ? next.progress.interpolate({
        //         inputRange: [0, 1],
        //         outputRange: [1, 0.7],
        //       })
        //     : current.progress.interpolate({
        //         inputRange: [0, 1],
        //         outputRange: [0.7, 1],
        //       }),
        // },
      ],
    },
  };
};
