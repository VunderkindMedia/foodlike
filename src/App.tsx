import React from 'react';
import {applyMiddleware, createStore} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import {createRootReducer} from './redux/root-reducer';
import PDAppNavigator from './navigation/app-navigator';
import {StorageManager} from './storage-manager';
import {NotificationManager} from './notification-manager';
import codePush from 'react-native-code-push';
const App = () => {
  const notificationManager = new NotificationManager();
  const composeEnhancers = composeWithDevTools({trace: true});
  const store = createStore(
    createRootReducer(),
    composeEnhancers(applyMiddleware(thunk)),
  );
  StorageManager.init(store).catch(e => console.log(e));
  notificationManager.requestUserPermission(store);
  notificationManager.backgroundNotificationHandlers();
  notificationManager.foregroundNotificationHandlers();
  return (
    <Provider store={store}>
      <PDAppNavigator />
    </Provider>
  );
};

export default codePush(App);
