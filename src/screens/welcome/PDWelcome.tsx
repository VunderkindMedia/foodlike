import LinearGradient from 'react-native-linear-gradient';
import React, {useCallback} from 'react';
import {Text, View, Image, StatusBar, StyleSheet} from 'react-native';
import PDColors from '../../constants/colors/colors';
import {
  WELCOME_BUTTON_TITLE,
  WELCOME_SUBTITLE,
  WELCOME_TITLE,
} from '../../constants/strings';
import {DefaultPDButton} from '../../components/buttons/default-button';
import {Routes} from '../../navigation/constants/routes';
import {TMainScreenNavigatorProp} from '../../navigation/navigators/welcome/types';

interface IPDWelcomeProps {
  navigation: TMainScreenNavigatorProp;
}
export const PDWelcome = (props: IPDWelcomeProps) => {
  const {navigation} = props;

  const goTo = useCallback(
    () => navigation.push(Routes.SCREEN_LOGIN),
    [navigation],
  );
  return (
    <LinearGradient
      colors={PDColors.backgroundGradient!}
      start={{x: 0, y: 1}}
      end={{x: 0, y: 0}}
      style={styles.welcome_container}>
      <StatusBar backgroundColor={PDColors.transparent} translucent />
      <View style={styles.welcome_wrapper}>
        <Image
          style={styles.welcome_logo}
          source={require('./resources/images/courier.png')}
        />
        <View style={styles.welcome_footer_wrapper}>
          <View>
            <Text style={styles.welcome_title}>{WELCOME_TITLE}</Text>
            <Text style={styles.welcome_subtitle}>{WELCOME_SUBTITLE}</Text>
          </View>
          <DefaultPDButton
            onPress={goTo}
            title={WELCOME_BUTTON_TITLE}
            backgroundColor={PDColors.whiteBackground}
            titleColor={PDColors.primary}
            buttonStyle={styles.welcome_button}
          />
        </View>
      </View>
    </LinearGradient>
  );
};
const styles = StyleSheet.create({
  welcome_container: {flex: 1},
  welcome_wrapper: {alignItems: 'center', flex: 1},
  welcome_logo: {width: '100%', marginTop: 100, resizeMode: 'contain'},
  welcome_title: {
    fontFamily: 'SFProDisplay-Semibold',
    textAlign: 'center',
    fontSize: 36,
    color: PDColors.textWhite,
    marginBottom: 16,
  },
  welcome_subtitle: {
    fontFamily: 'SFProDisplay-Regular',
    textAlign: 'center',
    fontSize: 16,
    color: PDColors.lightestText,
    marginBottom: 67,
  },
  welcome_button_title: {
    color: PDColors.primary,
    fontFamily: 'SFProDisplay-Medium',
    fontSize: 18,
  },
  welcome_footer_wrapper: {
    paddingHorizontal: 24,
    justifyContent: 'space-between',
    flex: 1,
    paddingBottom: 76,
  },
  welcome_button: {
    justifyContent: 'center',
  },
});
