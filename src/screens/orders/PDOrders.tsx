import React, {useEffect, useMemo} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {useGetOrders} from '../../hooks/orders';
import {useRequestStateIsLoading} from '../../redux/request/selectors';
import {useOrders} from '../../redux/orders/selectors';
import {PDOrdersItem} from '../../components/orders/orders_item';
import {PDLoader} from '../../components/loader/loader';
interface IPDOrdersProps {
  navigation: any;
}
export const PDOrders = (props: IPDOrdersProps) => {
  const {navigation} = props;
  const getOrders = useGetOrders();
  const orders = useOrders();
  const reverseOrders = useMemo(() => orders.reverse(), [orders]);
  const loading = useRequestStateIsLoading('orders_get');
  useEffect(() => {
    getOrders();
  }, [getOrders]);
  return (
    <View style={styles.container}>
      {loading ? (
        <PDLoader fullscreen />
      ) : (
        <FlatList
          data={reverseOrders}
          renderItem={({item}) => (
            <PDOrdersItem navigation={navigation} item={item} />
          )}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingVertical: 16,
    paddingHorizontal: 24,
  },
});
