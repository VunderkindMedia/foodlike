import React, {useCallback, useEffect, useState} from 'react';
import {FlatList, Linking, StyleSheet, Text, View} from 'react-native';
import {FavoritesItem} from '../../components/products/favorites_item';
import {Routes} from '../../navigation/constants/routes';
import {
  TOrderNavigationProp,
  TOrderRouteProp,
} from '../../navigation/navigators/product/types';
import PDColors from '../../constants/colors/colors';
import {PDIcons} from '../../components/icons/custom-icons';
import RNBounceable from '@freakycoder/react-native-bounceable';
import {PDOrderTotal} from '../../components/orders/order_total';
import {Transaction} from '../../model/transaction';
import {useGetTransactions} from '../../hooks/orders';
import {useRequestStateIsLoading} from '../../redux/request/selectors';
import {PDLoader} from '../../components/loader/loader';
import {Order} from '../../model/order';
import {orderStatusToText} from '../../helpers/orders';

interface IPDOrderProps {
  navigation: TOrderNavigationProp;
  route: TOrderRouteProp;
}

export const PDOrder = (props: IPDOrderProps) => {
  const {navigation, route} = props;
  const {order} = route.params;
  const [transaction, setTransaction] = useState(new Transaction());
  const getTransactions = useGetTransactions();
  const loading = useRequestStateIsLoading('transaction_get');

  useEffect(() => {
    getTransactions(order.transaction_id, trans => {
      setTransaction(trans);
    });
  }, [getTransactions, order.transaction_id]);

  const goHandler = useCallback(
    item => navigation.navigate(Routes.SCREEN_PRODUCT, {item: item}),
    [navigation],
  );

  return loading ? (
    <PDLoader fullscreen />
  ) : (
    <FlatList
      data={transaction.products}
      ListHeaderComponent={() => (
        <PDOrderFinisHeaderComponent
          address_name={`ул. ${transaction.delivery.address1}, ${transaction.delivery.address2}`}
          delivery_time={transaction.delivery.delivery_time}
          order={order}
        />
      )}
      ListFooterComponent={() => (
        <View>
          <PDOrderTotal transaction={transaction} />
        </View>
      )}
      renderItem={({item}) => (
        <FavoritesItem item={item} onPress={goHandler} inOrder />
      )}
    />
  );
};

interface IPDOrderFinisHeaderComponentProps {
  address_name: string;
  delivery_time: string;
  order: Order;
}

export const PDOrderFinisHeaderComponent = (
  props: IPDOrderFinisHeaderComponentProps,
) => {
  const {address_name, delivery_time, order} = props;
  const phoneHandler = useCallback(() => Linking.openURL('tel:29-56-29'), []);

  return (
    <View style={styles.header}>
      <PDIcons
        name={'success'}
        size={40}
        color={PDColors.green}
        style={styles.status_icon}
      />
      <Text style={[styles.header__title, styles.header__top_title]}>
        Заказ №{order.incoming_order_id.toString()}{' '}
        {orderStatusToText(order.status, order.text_status)}
      </Text>
      <RNBounceable onPress={phoneHandler} style={styles.button}>
        <Text style={styles.button_title}>Связаться с нами</Text>
      </RNBounceable>
      <View style={[styles.header_row, styles.header_first_row]}>
        <PDIcons
          name={'map_pin'}
          size={22}
          color={PDColors.textBlack}
          style={styles.icon}
        />
        <Text style={styles.header__row_title}>{address_name}</Text>
      </View>
      <View style={[styles.header_row, styles.header_last_row]}>
        <PDIcons
          name={'clock'}
          size={22}
          color={PDColors.textBlack}
          style={styles.icon}
        />
        <Text style={styles.header__row_title}>
          Время доставки: {delivery_time}
        </Text>
      </View>
      <Text style={styles.header__title}>В вашем заказе</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  header__title: {
    fontSize: 24,
    fontWeight: '600',
    fontFamily: 'SFProDisplay-Bold',
    color: PDColors.textBlack,
  },
  header__top_title: {
    marginBottom: 25,
    alignSelf: 'center',
  },
  header: {
    paddingTop: 32,
    paddingHorizontal: 24,
  },
  header__row_title: {
    fontSize: 16,
    fontWeight: '400',
    fontFamily: 'SFProDisplay-Regular',
    color: PDColors.textBlack,
  },
  header_first_row: {
    paddingBottom: 16,
    borderBottomWidth: 1,
    borderBottomColor: PDColors.buttonLightGray,
    marginBottom: 18,
  },
  header_last_row: {
    marginBottom: 32,
  },
  header_row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    marginRight: 18,
  },
  button: {
    alignItems: 'center',
    backgroundColor: PDColors.primary,
    paddingHorizontal: 18,
    paddingVertical: 20,
    borderRadius: 24,
    marginBottom: 32,
  },
  button_title: {
    fontSize: 20,
    fontWeight: '400',
    fontFamily: 'SFProDisplay-Regular',
    color: PDColors.textWhite,
  },
  status_icon: {
    alignSelf: 'center',
    marginBottom: 20,
  },
});
