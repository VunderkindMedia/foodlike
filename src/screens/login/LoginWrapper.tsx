import React, {useEffect} from 'react';
import {
  Keyboard,
  StatusBar,
  View,
  TouchableOpacity,
  StyleSheet,
  TouchableNativeFeedback,
  BackHandler,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import PDColors from '../../constants/colors/colors';
import {PDIcons} from '../../components/icons/custom-icons';
import RNBounceable from '@freakycoder/react-native-bounceable';

interface ILoginWrapperProps {
  children: JSX.Element;
  navigation?: any;
  isNotBack?: boolean;
}
export const PDLoginWrapper = (props: ILoginWrapperProps) => {
  const {children, navigation, isNotBack} = props;
  useEffect(() => {
    if (isNotBack) {
      BackHandler.addEventListener('hardwareBackPress', () => true);
    }
  }, [isNotBack]);
  return (
    <View style={styles.container}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor={PDColors.transparent}
        translucent
      />
      <TouchableNativeFeedback onPress={Keyboard.dismiss}>
        <View style={styles.wrapper}>
          <View style={styles.header}>
            <RNBounceable
              onPress={() => !isNotBack && navigation.pop()}
              style={[
                styles.back_button,
                isNotBack && styles.back_button_hide,
              ]}>
              <PDIcons name="chevron" size={24} />
            </RNBounceable>
          </View>
          {children}
        </View>
      </TouchableNativeFeedback>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: PDColors.whiteBackground},
  wrapper: {flex: 1},
  header: {marginBottom: 65, marginTop: 50},
  back_button: {
    height: 60,
    width: 60,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 25,
  },
  back_button_hide: {
    opacity: 0,
  },
});
