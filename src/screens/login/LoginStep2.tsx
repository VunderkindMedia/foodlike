import React, {useEffect, useCallback, useState} from 'react';
import {View, Text} from 'react-native';
import {PDLoginWrapper} from './LoginWrapper';
import PDColors from '../../constants/colors/colors';
import {
  DEMO_CODE,
  DEMO_PHONE,
  SIGNUP_CODE_TITLE,
  SIGNUP_RESEND_CODE,
  SIGNUP_STEP2_BUTTON_TITLE,
  SIGNUP_STEP2_TITLE,
  SIGNUP_STEP2_TITLE_RECALL,
} from '../../constants/strings';
import {DefaultPDButton} from '../../components/buttons/default-button';
import {styles} from './styles';
import {CodeInput} from '../../components/inputs/code-input/code-input';
import {Formik} from 'formik';
import {useSendCall, useSendTimer} from '../../hooks/sms';
import {PDLoader} from '../../components/loader/loader';
import {useLogin} from '../../hooks/account';
import {
  TLoginStep2NavigatorProp,
  TLoginStep2RouteProp,
} from '../../navigation/navigators/login/types';
import {Routes} from '../../navigation/constants/routes';
import {formInitValStep2, schemaStep2} from './form/form-options';
import RNBounceable from '@freakycoder/react-native-bounceable';

interface ILoginStep2Props {
  navigation: TLoginStep2NavigatorProp;
  route: TLoginStep2RouteProp;
}

export const PDLoginStep2 = (props: ILoginStep2Props) => {
  const {navigation, route} = props;
  const phone = route.params.phone;
  const [loading, setLoading] = useState(false);
  const [timeLeft, start] = useSendTimer();
  const sendCode = useSendCall();
  const [randomCode, setRandomCode] = useState('9999');
  const [recall, setRecall] = useState(false);
  const login = useLogin();

  const resendHandler = useCallback(() => {
    start();
    sendCode(phone, result => {
      setRandomCode(result.code);
    });
    setRecall(true);
  }, [phone, sendCode, start]);

  useEffect(() => {
    start();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  useEffect(() => {
    if (phone.replace(/[^+0-9]/g, '') === DEMO_PHONE) {
      return;
    }
    // sendCode(phone, result => {
    //   setRandomCode(result.code);
    // });
  }, [phone, sendCode]);

  const submitHandler = useCallback(async () => {
    setLoading(true);
    await login(phone);
    navigation.push(Routes.SCREEN_LOGIN_STEP_3, {phone: phone});
  }, [login, navigation, phone]);

  if (loading) {
    return <PDLoader fullscreen />;
  }

  return (
    <PDLoginWrapper navigation={navigation}>
      <View style={styles.container}>
        <Text style={styles.signup_title}>
          {recall ? SIGNUP_STEP2_TITLE_RECALL : SIGNUP_STEP2_TITLE}
        </Text>
        <Text style={styles.signup_field_title}>
          {SIGNUP_CODE_TITLE} {phone}
        </Text>
        <Formik
          initialValues={formInitValStep2}
          onSubmit={submitHandler}
          validationSchema={
            phone.replace(/[^+0-9]/g, '') === DEMO_PHONE
              ? schemaStep2(DEMO_CODE)
              : schemaStep2(randomCode)
          }
          validateOnChange
          validateOnBlur>
          {({handleChange, handleSubmit, values, errors, isValid, dirty}) => {
            return (
              <>
                <CodeInput
                  styles={[
                    styles.signup_field_input,
                    {
                      borderColor: !errors.code
                        ? PDColors.borderDefault
                        : PDColors.errorRed,
                      marginBottom: errors.code && 8,
                    },
                  ]}
                  value={values.code}
                  placeholderTextColor={PDColors.lightestText}
                  onChange={handleChange('code')}
                />
                {errors.code && values.code.length === 4 && (
                  <Text style={styles.signup_field_error}>{errors.code}</Text>
                )}
                <View style={styles.signup_footer}>
                  <RNBounceable
                    onPress={resendHandler}
                    disabled={timeLeft !== 0}>
                    <Text
                      style={[
                        styles.signup_field_title,
                        styles.signup_field_resend,
                        {color: PDColors.primary},
                      ]}>
                      {SIGNUP_RESEND_CODE}{' '}
                      {timeLeft !== 0 && `(${timeLeft / 1000} с)`}
                    </Text>
                  </RNBounceable>
                  <DefaultPDButton
                    backgroundColor={
                      isValid && dirty && !loading
                        ? PDColors.primary
                        : PDColors.buttonLightGray
                    }
                    title={SIGNUP_STEP2_BUTTON_TITLE}
                    buttonStyle={styles.signup_submit_button}
                    onPress={handleSubmit}
                  />
                </View>
              </>
            );
          }}
        </Formik>
      </View>
    </PDLoginWrapper>
  );
};
