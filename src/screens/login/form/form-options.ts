import * as Yup from 'yup';

//Шаг авторизации №1
export interface IPDLoginStep1Form {
  phone: string;
}
export const formInitValStep1: IPDLoginStep1Form = {
  phone: '',
};
export const schemaStep1 = Yup.object().shape({
  phone: Yup.string().min(18).max(18).required(),
});

//Шаг авторизации №2
export interface IPDLoginStep2Form {
  code: string;
}
export const formInitValStep2: IPDLoginStep2Form = {
  code: '',
};
export const schemaStep2 = (randomCode: string) => {
  return Yup.object().shape({
    code: Yup.string()
      .matches(RegExp(`${randomCode}`), 'Неверный код!')
      .min(4)
      .max(4)
      .required(),
  });
};

//Шаг авторизации №3
export interface IPDLoginStep3Form {
  name: string;
}
export const formInitValStep3: IPDLoginStep3Form = {
  name: '',
};
export const schemaStep3 = Yup.object().shape({
  name: Yup.string()
    .min(3, 'Не менее 3 символов')
    .required('Поле обязательно к заполнению'),
});
