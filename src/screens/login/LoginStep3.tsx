import React, {useCallback, useState} from 'react';
import {Text, TextInput, View} from 'react-native';
import {PDLoginWrapper} from './LoginWrapper';
import {
  TLoginStep3NavigatorProp,
  TLoginStep3RouteProp,
} from '../../navigation/navigators/login/types';
import {styles} from './styles';
import {
  SIGNUP_NAME_PLACEHOLDER,
  SIGNUP_NAME_TITLE,
  SIGNUP_STEP3_BUTTON_TITLE,
  SIGNUP_STEP3_TITLE,
} from '../../constants/strings';
import {Formik} from 'formik';
import PDColors from '../../constants/colors/colors';
import {DefaultPDButton} from '../../components/buttons/default-button';
import {
  formInitValStep3,
  IPDLoginStep3Form,
  schemaStep3,
} from './form/form-options';
import {useCreateClient} from '../../hooks/account';
import {Client} from '../../model/client';
import {PDLoader} from '../../components/loader/loader';

interface IPDLoginStep3Props {
  navigation: TLoginStep3NavigatorProp;
  route: TLoginStep3RouteProp;
}

export const PDLoginStep3 = (props: IPDLoginStep3Props) => {
  const {navigation, route} = props;
  const createClient = useCreateClient();
  const phone = route.params.phone;
  const [loading, setLoading] = useState(false);

  const submitHandler = useCallback(
    async (values: IPDLoginStep3Form) => {
      setLoading(true);
      const client = Client.Builder.withClientName(values.name, '')
        .withPhone(phone)
        .build();
      createClient(client);
    },
    [createClient, phone],
  );
  if (loading) {
    return <PDLoader fullscreen />;
  }
  return (
    <PDLoginWrapper navigation={navigation} isNotBack>
      <View style={styles.container}>
        <Text style={styles.signup_title}>{SIGNUP_STEP3_TITLE}</Text>
        <Text style={styles.signup_field_title}>{SIGNUP_NAME_TITLE}</Text>
        <Formik
          initialValues={formInitValStep3}
          onSubmit={submitHandler}
          validationSchema={schemaStep3}
          validateOnChange
          validateOnBlur>
          {({handleChange, handleSubmit, values, errors, isValid, dirty}) => {
            return (
              <>
                <TextInput
                  style={[
                    styles.signup_field_input,
                    {
                      borderColor: !errors.name
                        ? PDColors.borderDefault
                        : PDColors.errorRed,
                      marginBottom: errors.name && 8,
                    },
                  ]}
                  value={values.name}
                  placeholder={SIGNUP_NAME_PLACEHOLDER}
                  placeholderTextColor={PDColors.lightestText}
                  onChangeText={handleChange('name')}
                />
                {errors.name && (
                  <Text style={styles.signup_field_error}>{errors.name}</Text>
                )}
                <View style={styles.signup_footer}>
                  <DefaultPDButton
                    backgroundColor={
                      isValid && dirty && !loading
                        ? PDColors.primary
                        : PDColors.buttonLightGray
                    }
                    title={SIGNUP_STEP3_BUTTON_TITLE}
                    buttonStyle={styles.signup_submit_button}
                    onPress={handleSubmit}
                  />
                </View>
              </>
            );
          }}
        </Formik>
      </View>
    </PDLoginWrapper>
  );
};
