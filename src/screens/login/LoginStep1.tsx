import {DefaultPDButton} from '../../components/buttons/default-button';
import {PhoneInput} from '../../components/inputs/phone-input/phone-input';
import PDColors from '../../constants/colors/colors';
import {Text, TouchableOpacity, View} from 'react-native';
import React, {useCallback} from 'react';
import {PDLoginWrapper} from './LoginWrapper';
import {
  SIGNUP_OFFER_END,
  SIGNUP_OFFER_START,
  SIGNUP_PHONE_TITLE,
  SIGNUP_STEP1_BUTTON_TITLE,
  SIGNUP_TITLE,
} from '../../constants/strings';
import {Routes} from '../../navigation/constants/routes';
import {Formik} from 'formik';
import {styles} from './styles';
import {TLoginStep1NavigatorProp} from '../../navigation/navigators/login/types';
import {
  formInitValStep1,
  IPDLoginStep1Form,
  schemaStep1,
} from './form/form-options';

interface IPDLoginStep1Props {
  navigation: TLoginStep1NavigatorProp;
}

export const PDLoginStep1 = (props: IPDLoginStep1Props) => {
  const {navigation} = props;

  const submitHandler = useCallback(
    async (values: IPDLoginStep1Form) => {
      navigation.push(Routes.SCREEN_LOGIN_STEP_2, {phone: values.phone});
    },
    [navigation],
  );

  const onPressOffer = useCallback(
    () => console.log('Нажали на ссылку офферты'),
    [],
  );

  return (
    <PDLoginWrapper navigation={navigation}>
      <View style={styles.container}>
        <Text style={styles.signup_title}>{SIGNUP_TITLE}</Text>
        <Text style={styles.signup_field_title}>{SIGNUP_PHONE_TITLE}</Text>
        <Formik
          initialValues={formInitValStep1}
          onSubmit={submitHandler}
          validationSchema={schemaStep1}
          validateOnChange
          validateOnBlur>
          {({handleChange, handleSubmit, values, errors, isValid, dirty}) => {
            return (
              <>
                <PhoneInput
                  styles={[
                    styles.signup_field_input,
                    {
                      borderColor: !errors.phone
                        ? PDColors.borderDefault
                        : PDColors.errorRed,
                    },
                  ]}
                  value={values.phone}
                  placeholderTextColor={PDColors.lightestText}
                  onChange={handleChange('phone')}
                />
                <View style={styles.signup_policy_wrapper}>
                  <Text style={styles.signup_policy_text}>
                    {SIGNUP_OFFER_START}
                  </Text>
                  <TouchableOpacity activeOpacity={0.5} onPress={onPressOffer}>
                    <Text style={styles.signup_policy_clickable_text}>
                      {' '}
                      {SIGNUP_OFFER_END}
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.signup_footer}>
                  <DefaultPDButton
                    backgroundColor={
                      isValid && dirty
                        ? PDColors.primary
                        : PDColors.buttonLightGray
                    }
                    title={SIGNUP_STEP1_BUTTON_TITLE}
                    buttonStyle={styles.signup_submit_button}
                    onPress={handleSubmit}
                  />
                </View>
              </>
            );
          }}
        </Formik>
      </View>
    </PDLoginWrapper>
  );
};
