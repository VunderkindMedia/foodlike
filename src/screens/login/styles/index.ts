import {StyleSheet} from 'react-native';
import PDColors from '../../../constants/colors/colors';

const styles = StyleSheet.create({
  container: {flex: 1, marginHorizontal: 25, paddingBottom: 30},
  signup_title: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 36,
    marginBottom: 32,
    color: PDColors.textBlack,
  },
  signup_field_title: {
    fontFamily: 'SFProDisplay-Medium',
    fontSize: 16,
    marginBottom: 16,
    color: PDColors.textDark,
  },
  signup_field_input: {
    paddingBottom: 15,
    marginBottom: 24,
    borderBottomWidth: 1,
    fontFamily: 'SFProDisplay-Medium',
    fontSize: 24,
    color: PDColors.textBlack,
  },
  signup_policy_wrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    flex: 1,
  },
  signup_policy_text: {
    color: PDColors.lightGrayText,
  },
  signup_policy_clickable_text: {
    fontSize: 14,
    fontFamily: 'SFProDisplay-Regular',
    borderBottomWidth: 1,
    borderBottomColor: PDColors.textDark,
    color: PDColors.textDark,
  },
  signup_footer: {flex: 1, justifyContent: 'flex-end'},
  signup_submit_button: {justifyContent: 'center'},
  signup_field_resend: {alignSelf: 'center'},
  signup_field_error: {color: PDColors.errorRed},
});

export {styles};
