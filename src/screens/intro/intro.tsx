import React, {useCallback, useEffect} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {PDIcons} from '../../components/icons/custom-icons';
import PDImages from '../../constants/images/images';
import {DefaultPDButton} from '../../components/buttons/default-button';
import {useSetIntro} from '../../hooks/settings';
import PDColors from '../../constants/colors/colors';
import {StorageManager} from '../../storage-manager';
import {useIntro} from '../../redux/settings/selectors';

interface IPDIntroProps {
  navigation: any;
}

export const PDIntro = (props: IPDIntroProps) => {
  const {navigation} = props;
  const setIntro = useSetIntro();
  const intro = useIntro();
  const rememberHandler = useCallback(async () => {
    await StorageManager.setIntro(true);
    navigation.pop();
  }, [navigation]);
  const acceptHandler = useCallback(() => {
    setIntro(true);
  }, [setIntro]);

  useEffect(() => {
    if (intro) {
      navigation.pop();
    }
  }, [intro, navigation]);

  return (
    <View style={styles.container}>
      <View style={styles.top_line} />
      <View style={styles.wrapper}>
        <View style={styles.body_wrapper}>
          <PDIcons
            color={PDColors.lightGrayText}
            name={'alert'}
            size={55}
            style={styles.icon}
          />
          <Text style={styles.title}>Внимание!</Text>
          <Text style={styles.subtitle}>
            Наше новое приложение имеет новую иконку. В скором времени мы
            прекратим поддержку старого приложения. Рекомендуем
            <Text style={styles.bold}> удалить</Text> его, чтобы вы не
            столкнулись с проблемами при оформлении заказов в дальнейшем.
          </Text>
          <View style={styles.logo_wrapper}>
            <Image style={styles.image} source={PDImages.old_logo} />
            <PDIcons color={PDColors.textDark} name={'long_arrow'} />
            <Image style={styles.image_new} source={PDImages.new_logo} />
          </View>
        </View>

        <View>
          <DefaultPDButton
            title={'Готово'}
            onPress={acceptHandler}
            buttonStyle={styles.accept_button}
          />
          <DefaultPDButton
            title={'Больше не напоминать'}
            onPress={rememberHandler}
            buttonStyle={styles.remember_button}
            titleColor={PDColors.textBlack}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  wrapper: {
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: PDColors.whiteBackground,
    height: '90%',
    overflow: 'hidden',
    paddingHorizontal: 24,
    paddingBottom: 30,
  },
  body_wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logo_wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  icon: {
    paddingBottom: 40,
  },
  title: {
    color: PDColors.textBlack,
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 24,
    marginBottom: 18,
  },
  subtitle: {
    marginBottom: 30,
    textAlign: 'center',
    fontSize: 16,
    fontFamily: 'SFProText-Regular',
    color: PDColors.textDark,
  },
  bold: {
    fontFamily: 'SFProDisplay-Bold',
  },
  image: {
    marginHorizontal: 20,
    width: 98,
    height: 98,
  },
  image_new: {
    marginHorizontal: 30,
    width: 64,
    height: 64,
  },
  accept_button: {
    justifyContent: 'center',
  },
  remember_button: {
    justifyContent: 'center',
    backgroundColor: PDColors.transparent,
  },
  top_line: {
    width: 48,
    height: 5,
    backgroundColor: PDColors.buttonLightGray,
    alignSelf: 'center',
    marginBottom: 7,
  },
});
