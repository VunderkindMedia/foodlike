import React, {useCallback} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {PDIcons} from '../../components/icons/custom-icons';
import PDColors from '../../constants/colors/colors';
import {DefaultPDButton} from '../../components/buttons/default-button';
import {Routes} from '../../navigation/constants/routes';
import {TOrderFinishNavigationProp} from '../../navigation/navigators/product/types';

interface IPDOrderFinishProps {
  navigation: TOrderFinishNavigationProp;
}
export const PDOrderFinish = (props: IPDOrderFinishProps): JSX.Element => {
  const {navigation} = props;

  const goMainPage = useCallback(() => navigation.popToTop(), [navigation]);
  const goToOrder = useCallback(
    () => navigation.navigate(Routes.SCREEN_ORDERS),
    [navigation],
  );

  return (
    <View style={styles.container}>
      <View style={styles.body_wrapper}>
        <PDIcons
          name={'success'}
          size={158}
          color={PDColors.backgroundLightPrimary}
          style={styles.icon}
        />
        <Text style={styles.title}>Спасибо за заказ!</Text>
        <Text>Ваш заказ принят. Мы свяжемся с вами в ближайшее время.</Text>
      </View>
      <View style={styles.buttons_wrapper}>
        <DefaultPDButton
          buttonStyle={styles.go_button}
          title={'Посмотреть заказ'}
          onPress={goToOrder}
        />
        <DefaultPDButton
          buttonStyle={styles.back_button}
          titleColor={PDColors.textBlack}
          title={'Вернуться на главную'}
          onPress={goMainPage}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  body_wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttons_wrapper: {
    paddingBottom: 30,
    paddingHorizontal: 24,
  },
  icon: {
    marginBottom: 32,
  },
  title: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 24,
    marginBottom: 16,
    color: PDColors.textBlack,
  },
  subtitle: {
    fontFamily: 'SFProText-Regular',
    color: PDColors.textDark,
  },
  go_button: {
    justifyContent: 'center',
  },
  back_button: {
    backgroundColor: PDColors.transparent,
    justifyContent: 'center',
  },
  back_button_title: {
    color: PDColors.textBlack,
  },
});
