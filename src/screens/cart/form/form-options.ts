import * as Yup from 'yup';
import {Address} from '../../../model/address';
import {FormikValues} from 'formik';

export interface IPDCreateOrderForm extends FormikValues {
  address?: Address;
  delivery_time?: Date;
  comment: string;
  payment_type?: number;
  use_bonuses: boolean;
  tableware_count: number;
  recall: boolean;
  user_name: string;
  user_phone: string;
}
export const formInitVal = (address?: Address): IPDCreateOrderForm => ({
  address: address,
  delivery_time: undefined,
  comment: '',
  payment_type: undefined,
  use_bonuses: false,
  tableware_count: 1,
  recall: false,
  user_name: '',
  user_phone: '',
});
export const schema = Yup.object().shape({
  address: Yup.string().required('Поле обязательно к заполнению'),
  delivery_time: Yup.string().required('Поле обязательно к заполнению'),
  payment_type: Yup.string().required(
    'Необходимо выбрать один из способов оплаты',
  ),
});
