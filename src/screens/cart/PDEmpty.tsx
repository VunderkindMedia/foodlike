import React from 'react';
import {View, Text, Image, ImageSourcePropType, StyleSheet} from 'react-native';
import PDColors from '../../constants/colors/colors';
import {DefaultPDButton} from '../../components/buttons/default-button';
import {EMPTY_GO_TO_MENU} from '../../constants/strings';
interface IPDCartEmpty {
  imagePath: ImageSourcePropType;
  title: string;
  subtitle: string;
  onClick: () => void;
}
export const PDEmpty = (props: IPDCartEmpty) => {
  const {imagePath, title, subtitle, onClick} = props;
  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <Image style={styles.image} source={imagePath} />
        <Text style={styles.title}>{title}</Text>
        <View style={styles.subtitle_wrapper}>
          <Text style={styles.subtitle}>{subtitle}</Text>
        </View>
      </View>
      <View style={styles.button_wrapper}>
        <DefaultPDButton
          buttonStyle={styles.button}
          title={EMPTY_GO_TO_MENU}
          onPress={onClick}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 23,
    paddingVertical: 10,
  },
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 200,
    height: 150,
    marginBottom: 35,
    resizeMode: 'contain',
  },
  title: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 24,
    color: PDColors.textBlack,
    marginBottom: 16,
  },
  subtitle_wrapper: {
    width: '80%',
  },
  subtitle: {
    fontFamily: 'SFProDisplay',
    fontSize: 16,
    color: PDColors.textDark,
    marginBottom: 16,
    textAlign: 'center',
  },
  button_wrapper: {width: '100%'},
  button: {
    justifyContent: 'center',
  },
});
