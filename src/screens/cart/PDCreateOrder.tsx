import React, {useCallback, useMemo, useState} from 'react';
import {ScrollView, StyleSheet, View} from 'react-native';
import {Formik} from 'formik';
import {PDInOrderCheckAddress} from '../../components/order_create/order_addresses';
import {PDInOrderDeliveryTime} from '../../components/order_create/order_delivery_time';
import {PDInOrderUserInfo} from '../../components/order_create/order_user_info';
import {PDInOrderPayType} from '../../components/order_create/order_pay_type';
import {PDInOrderOptions} from '../../components/order_create/order_options';
import {PDInCreateOrderTotal} from '../../components/order_create/order_total';
import {DefaultPDButton} from '../../components/buttons/default-button';
import {ORDER_CREATE_BUTTON_TITLE} from '../../constants/strings';
import {styles} from '../../components/order_create/styles';
import {PDInOrderComment} from '../../components/order_create/order_comment';
import {
  useAddresses,
  useCheckedAddress,
  useClient,
} from '../../redux/account/selectors';
import {PDivider} from '../../components/dividers/divider';
import {useSelector} from 'react-redux';
import {IAppState} from '../../redux/root-reducer';
import {
  TOrderCreateNavigatorProp,
  TOrderCreateRouteProp,
} from '../../navigation/navigators/product/types';
import {DELIVERY_PRICE} from '../../constants/payments';
import {
  useCartDiscountSum,
  useCartList,
  useCartSum,
} from '../../redux/cart/selectors';
import PDColors from '../../constants/colors/colors';
import {formInitVal, IPDCreateOrderForm, schema} from './form/form-options';
import {useCreateOrder} from '../../hooks/orders';
import {Routes} from '../../navigation/constants/routes';
import {PDLoader} from '../../components/loader/loader';
import {useCartClean} from '../../hooks/cart';
import {Order} from '../../model/order';
interface IPDCreateOrderProps {
  route: TOrderCreateRouteProp;
  navigation: TOrderCreateNavigatorProp;
}
export const PDCreateOrder = (props: IPDCreateOrderProps) => {
  const {navigation, route} = props;
  const settings = useSelector((state: IAppState) => state.settings.all);
  const client = useClient();
  const cartSum = useCartSum();
  const cartList = useCartList();
  const cartDiscountSum = useCartDiscountSum();
  const checkedAddress = useCheckedAddress();
  const createOrder = useCreateOrder();
  const addresses = useAddresses();
  const cartClean = useCartClean();
  const [loading, setLoading] = useState(false);
  const address = useMemo(
    () => addresses.find(add => add.id === checkedAddress),
    [addresses, checkedAddress],
  );
  const handleSubmit = useCallback(
    (values: IPDCreateOrderForm) => {
      setLoading(true);
      createOrder(values, cartList, result => {
        navigation.replace(Routes.SCREEN_ORDER_FINISH, {
          order: Order.parse(result.response),
        });
        cartClean();
      });
    },
    [cartClean, cartList, createOrder, navigation],
  );

  return (
    <View>
      {loading && <PDLoader fullscreen />}
      <ScrollView
        contentContainerStyle={styles.container}
        keyboardShouldPersistTaps="handled">
        <Formik
          initialValues={formInitVal(address)}
          validationSchema={schema}
          validateOnBlur
          onSubmit={handleSubmit}>
          {({handleSubmit, values, errors, isValid, dirty, setFieldValue}) => {
            return (
              <>
                <PDInOrderCheckAddress
                  setValue={setFieldValue}
                  checked_address={address}
                  errors={errors}
                  values={values}
                  navigation={navigation}
                  route={route}
                />
                <PDivider height={5} />
                <PDInOrderComment
                  setValue={setFieldValue}
                  errors={errors}
                  values={values}
                />
                <PDInOrderDeliveryTime
                  setValue={setFieldValue}
                  errors={errors}
                  values={values}
                  navigation={navigation}
                />

                <PDInOrderUserInfo
                  setValue={setFieldValue}
                  errors={errors}
                  values={values}
                />
                <PDInOrderPayType
                  payment_types={settings.payment_types}
                  setValue={setFieldValue}
                  errors={errors}
                  values={values}
                />
                <PDInOrderOptions
                  setValue={setFieldValue}
                  client={client}
                  values={values}
                />
                <PDInCreateOrderTotal
                  inFinish={false}
                  client={client}
                  cartSum={cartSum}
                  deliveryPrice={DELIVERY_PRICE}
                  discountSum={cartDiscountSum}
                  use_bonuses={values.use_bonuses}
                />
                <DefaultPDButton
                  backgroundColor={
                    isValid && dirty
                      ? PDColors.primary
                      : PDColors.buttonLightGray
                  }
                  suffixValue={
                    values.use_bonuses
                      ? (cartSum + DELIVERY_PRICE - (client!.bonus ?? 0))
                          .toFixed(2)
                          .replace('.', ',')
                      : (cartSum + DELIVERY_PRICE).toFixed(2).replace('.', ',')
                  }
                  suffixValueStyle={_styles.button_suffix_value}
                  suffixValueContainerStyle={
                    _styles.suffix_value_container_style
                  }
                  title={ORDER_CREATE_BUTTON_TITLE}
                  onPress={handleSubmit}
                />
              </>
            );
          }}
        </Formik>
      </ScrollView>
    </View>
  );
};

const _styles = StyleSheet.create({
  button_suffix_value: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 18,
  },
  suffix_value_container_style: {
    backgroundColor: PDColors.transparent,
  },
});
