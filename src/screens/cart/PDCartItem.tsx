import React, {useCallback} from 'react';
import {View, Image, TouchableOpacity, Text, StyleSheet} from 'react-native';
import {PDIcons} from '../../components/icons/custom-icons';
import {Product} from '../../model/product';
import PDImages from '../../constants/images/images';
import PDColors from '../../constants/colors/colors';
import {CartRemoveAlert} from '../../constants/allerts';
import RNBounceable from '@freakycoder/react-native-bounceable';

interface IPDCartItemProps {
  item: Product;
  changeCartDecrement: (product: Product) => void;
  changeCartIncrement: (product: Product) => void;
  cartRemove: (product: Product) => void;
  onPress: (item: Product) => void;
}

export const PDCartItem = (props: IPDCartItemProps) => {
  const {item, changeCartDecrement, changeCartIncrement, cartRemove, onPress} =
    props;

  const removeAlert = CartRemoveAlert();

  const removeHandler = useCallback(() => {
    removeAlert(cartRemove, item);
  }, [cartRemove, item, removeAlert]);

  const incrementHandler = useCallback(() => {
    changeCartIncrement(item);
  }, [changeCartIncrement, item]);

  const decrementHandler = useCallback(() => {
    if (item.cartCount === 1) {
      cartRemove(item);
    } else {
      changeCartDecrement(item);
    }
  }, [cartRemove, changeCartDecrement, item]);

  const pressHandler = useCallback(() => onPress(item), [item, onPress]);

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={pressHandler}
      style={styles.cart__item}>
      <View style={styles.cart__item_wrapper}>
        <View>
          <Image
            style={styles.cart__item_image}
            source={item.photo ? {uri: item.photo} : PDImages.no_photo}
          />
        </View>
        <View style={styles.cart__item_center}>
          <Text
            style={styles.cart__item_title}
            numberOfLines={2}
            ellipsizeMode="tail">
            {item.name}
          </Text>
          <Text style={styles.product__weight}>{item.seoDescription}</Text>

          <View style={styles.cart__item_btns_wrapper}>
            <RNBounceable
              style={styles.cart__item_btn}
              onPress={decrementHandler}>
              <PDIcons name="minus" size={15} color={PDColors.textBlack} />
            </RNBounceable>
            <View style={styles.cart__item_counter_wrapper}>
              <Text style={styles.cart__item_counter}>{item.cartCount} шт</Text>
            </View>
            <RNBounceable
              style={styles.cart__item_btn}
              onPress={incrementHandler}>
              <PDIcons name="plus" size={15} color={PDColors.textBlack} />
            </RNBounceable>
          </View>
        </View>
        <View style={styles.cart__item_right_wrapper}>
          <TouchableOpacity
            style={styles.cart__item_close}
            activeOpacity={0.8}
            onPress={removeHandler}>
            <PDIcons name="close" size={12} color="gray" />
          </TouchableOpacity>
          <View style={styles.cart__item_price_wrapper}>
            {item.discountPrice && (
              <Text style={styles.cart__item_price_old}>
                {item.price * item.cartCount} ₽
              </Text>
            )}
            <Text style={styles.cart__item_price}>
              {item.discountPrice
                ? item.discountPrice * item.cartCount
                : item.price * item.cartCount}{' '}
              ₽
            </Text>
          </View>
        </View>
      </View>
      <View style={styles.cart__item_divider} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  cart__item: {
    flex: 1,
  },
  cart__item_wrapper: {
    flexDirection: 'row',
    paddingHorizontal: 25,
    paddingVertical: 24,
  },
  cart__item_image: {
    width: 88,
    height: 88,
    resizeMode: 'cover',
    marginRight: 16,
    borderRadius: 4,
  },
  cart__item_center: {
    flex: 1,
  },
  cart__item_title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 16,
    marginBottom: 5,
    color: PDColors.textBlack,
  },
  product__weight: {
    fontFamily: 'SFProDisplay',
    fontSize: 16,
    color: PDColors.textDark,
    marginBottom: 12,
  },
  cart__item_btns_wrapper: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
  },
  cart__item_btn: {
    width: 32,
    height: 32,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: PDColors.buttonLightGray,
    borderRadius: 8,
  },
  cart__item_counter_wrapper: {
    height: 45,
    marginHorizontal: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cart__item_right_wrapper: {
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  cart__item_price_wrapper: {
    justifyContent: 'center',
    height: 45,
  },
  cart__item_price_old: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 14,
    color: PDColors.normalGrayText,
    textDecorationLine: 'line-through',
  },
  cart__item_counter: {
    fontFamily: 'SFProDisplay-Medium',
    fontSize: 16,
    color: PDColors.textBlack,
    textAlign: 'center',
  },
  cart__item_price: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 16,
    color: PDColors.textBlack,
  },
  cart__item_divider: {
    flex: 1,
    borderBottomWidth: 1,
    marginHorizontal: 25,
    borderColor: PDColors.borderDefault,
  },
  cart__item_close: {
    paddingHorizontal: 12,
    paddingVertical: 6,
  },
});
