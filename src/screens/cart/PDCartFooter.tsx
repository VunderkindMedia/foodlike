import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import PDColors from '../../constants/colors/colors';

interface IPDCartFooterProps {
  discountSum: number;
  deliveryPrice: number;
  cartSum: number;
}

export const PDCartFooter = (props: IPDCartFooterProps) => {
  const {deliveryPrice, cartSum} = props;
  return (
    <View style={styles.cart__footer_wrapper}>
      {/*<View style={styles.cart__discount_wrapper}>*/}
      {/*  <Text style={styles.cart__discount_title}>Скидка</Text>*/}
      {/*  <Text style={styles.cart__discount_value}>{discountSum} ₽</Text>*/}
      {/*</View>*/}
      <View style={styles.cart__delivery_wrapper}>
        <Text style={styles.cart__discount_title}>Стоимость доставки</Text>
        <Text style={styles.cart__delivery_value}>{deliveryPrice} ₽</Text>
      </View>
      <View style={styles.cart__discount_wrapper}>
        <Text style={styles.cart__footer_amount_title}>К оплате</Text>
        <Text style={styles.cart__footer_amount_title}>
          {cartSum + deliveryPrice} ₽
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  cart__footer_wrapper: {
    paddingVertical: 16,
    paddingHorizontal: 24,
  },
  cart__discount_wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  cart__discount_title: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 16,
    color: PDColors.textDark,
  },
  cart__discount_value: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 16,
    color: PDColors.primary,
  },
  cart__delivery_wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 24,
  },
  cart__delivery_value: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 16,
    color: PDColors.textDark,
  },
  cart__footer_amount_title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 16,
    color: PDColors.textBlack,
  },
});
