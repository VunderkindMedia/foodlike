import React, {useCallback, useEffect} from 'react';
import {View, FlatList, StyleSheet, TouchableOpacity} from 'react-native';
import {
  useCartDiscountSum,
  useCartList,
  useCartSum,
} from '../../redux/cart/selectors';
import {PDCartItem} from './PDCartItem';
import {
  useCartClean,
  useCartDecrement,
  useCartIncrement,
  useCartRemove,
} from '../../hooks/cart';
import {PDEmpty} from './PDEmpty';
import PDImages from '../../constants/images/images';
import {Routes} from '../../navigation/constants/routes';

import {
  TCategoriesNavigatorProp,
  TCategoriesRouteProp,
} from '../../navigation/navigators/product/types';
import {PDCartFooter} from './PDCartFooter';
import {DefaultPDButton} from '../../components/buttons/default-button';
import {
  CART_BUTTON_TITLE,
  CART_EMPTY_SUBTITLE,
  CART_EMPTY_TITLE,
} from '../../constants/strings';
import {DELIVERY_PRICE} from '../../constants/payments';
import PDColors from '../../constants/colors/colors';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {CartCleanAlert} from '../../constants/allerts';

interface IPDCartProps {
  navigation: TCategoriesNavigatorProp;
  route: TCategoriesRouteProp;
}

export const PDCart = (props: IPDCartProps) => {
  const {navigation} = props;
  const cartList = useCartList();
  const cartIncrement = useCartIncrement();
  const cartDecrement = useCartDecrement();
  const cartRemove = useCartRemove();
  const cartSum = useCartSum();
  const cartDiscountSum = useCartDiscountSum();
  const cartClean = useCartClean();
  const cleanAlert = CartCleanAlert();
  const cleanHandler = useCallback(
    () => cleanAlert(cartClean),
    [cartClean, cleanAlert],
  );

  useEffect(() => {
    navigation.setOptions({
      headerRight: () =>
        cartList.length > 0 ? (
          <TouchableOpacity onPress={cleanHandler}>
            <Ionicons
              name={'trash-outline'}
              size={24}
              style={styles.clean_icon}
              color={PDColors.textBlack}
            />
          </TouchableOpacity>
        ) : null,
    });
  }, [cartList, cleanHandler, navigation]);

  const emptyGoHandler = useCallback(() => {
    navigation.navigate(Routes.SCREEN_CATEGORIES);
  }, [navigation]);

  const goHandler = useCallback(
    item => navigation.navigate(Routes.SCREEN_PRODUCT, {item: item}),
    [navigation],
  );

  const submitHandler = useCallback(
    () => navigation.navigate(Routes.SCREEN_CART_ORDER, {products: cartList}),
    [cartList, navigation],
  );
  return (
    <View style={styles.container}>
      {cartList.length === 0 && (
        <PDEmpty
          title={CART_EMPTY_TITLE}
          imagePath={PDImages.cart_empty}
          subtitle={CART_EMPTY_SUBTITLE}
          onClick={emptyGoHandler}
        />
      )}
      {cartList.length !== 0 && (
        <View style={styles.container}>
          <View style={styles.wrapper}>
            <FlatList
              data={cartList}
              renderItem={({item}) => (
                <PDCartItem
                  item={item}
                  cartRemove={cartRemove}
                  changeCartDecrement={cartDecrement}
                  changeCartIncrement={cartIncrement}
                  onPress={goHandler}
                />
              )}
              keyExtractor={item => item.name + item.pos_id}
              ListFooterComponent={() => (
                <View style={styles.footer}>
                  <PDCartFooter
                    cartSum={cartSum}
                    deliveryPrice={DELIVERY_PRICE}
                    discountSum={cartDiscountSum}
                  />
                </View>
              )}
            />
          </View>

          <DefaultPDButton
            title={CART_BUTTON_TITLE}
            onPress={submitHandler}
            buttonContainerStyle={styles.button}
            suffixValue={cartSum + DELIVERY_PRICE}
            suffixValueStyle={styles.button_suffix_value}
            suffixValueContainerStyle={styles.suffix_value_container_style}
          />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: 10,
  },
  wrapper: {},
  footer: {marginBottom: 80},
  button: {position: 'absolute', bottom: 8, right: 23, left: 23},
  button_suffix_value: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 18,
  },
  suffix_value_container_style: {
    backgroundColor: PDColors.transparent,
  },
  clean_icon: {
    paddingHorizontal: 16,
  },
});
