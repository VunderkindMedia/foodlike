import React, {useCallback} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import {DefaultPDButton} from '../../components/buttons/default-button';
import {PDIcons} from '../../components/icons/custom-icons';
import PDColors from '../../constants/colors/colors';
import {AirbnbRating} from 'react-native-ratings';
import {Formik} from 'formik';
import {useCancelReview, useSendReview} from '../../hooks/settings';
import {Routes} from '../../navigation/constants/routes';
import {useReviewOrder} from '../../redux/settings/selectors';

interface IPDReviewProps {
  navigation: any;
}
export const initialValues: IReviewInitial = {
  products_quality: 3,
  service_quality: 3,
  delivery_quality: 3,
  comment: '',
};
export interface IReviewInitial {
  products_quality: number;
  service_quality: number;
  delivery_quality: number;
  comment: string;
}

export const PDReview = (props: IPDReviewProps) => {
  const {navigation} = props;
  const sendReview = useSendReview();
  const review_order = useReviewOrder();
  const cancelReview = useCancelReview();
  const closeHandler = useCallback(() => {
    cancelReview();
    navigation.pop();
  }, [cancelReview, navigation]);
  const orderViewHandler = useCallback(
    () => navigation.navigate(Routes.SCREEN_ORDER, {order: review_order}),
    [navigation, review_order],
  );
  const submitHandler = useCallback(
    (values: IReviewInitial) => {
      sendReview(values);
      navigation.replace(Routes.SCREEN_REVIEW_FINISH);
    },
    [navigation, sendReview],
  );
  return (
    <View style={styles.container}>
      <Formik
        initialValues={initialValues}
        onSubmit={submitHandler}
        validateOnChange
        validateOnBlur>
        {({handleSubmit, setFieldValue, values}) => {
          return (
            <>
              <View style={styles.header}>
                <TouchableOpacity onPress={closeHandler}>
                  <PDIcons name={'close'} color={PDColors.textBlack} />
                </TouchableOpacity>
              </View>
              <View style={styles.body}>
                <Text style={styles.title}>Оцените заказ</Text>
                <TouchableOpacity onPress={orderViewHandler}>
                  <Text style={styles.order_view_title}>Посмотреть заказ</Text>
                </TouchableOpacity>
                <View style={styles.row}>
                  <Text>Качество товара</Text>
                  <AirbnbRating
                    count={5}
                    size={20}
                    onFinishRating={val =>
                      setFieldValue('products_quality', val)
                    }
                    showRating={false}
                  />
                </View>
                <View style={styles.row}>
                  <Text>Качество сервиса</Text>
                  <AirbnbRating
                    count={5}
                    size={20}
                    onFinishRating={val =>
                      setFieldValue('service_quality', val)
                    }
                    showRating={false}
                  />
                </View>
                <View style={[styles.row, styles.row_last]}>
                  <Text>Качество доставки</Text>
                  <AirbnbRating
                    count={5}
                    size={20}
                    onFinishRating={val =>
                      setFieldValue('delivery_quality', val)
                    }
                    showRating={false}
                  />
                </View>
                <Text style={styles.comment_title}>Комментарий к заказу</Text>
                <TextInput
                  onChangeText={val => setFieldValue('comment', val)}
                  value={values.comment}
                  numberOfLines={5}
                  style={styles.comment_area}
                />
              </View>
              <View style={styles.footer}>
                <DefaultPDButton
                  buttonStyle={styles.button}
                  title={'Отправить отзыв'}
                  onPress={handleSubmit}
                />
              </View>
            </>
          );
        }}
      </Formik>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 60,
    paddingBottom: 40,
    paddingHorizontal: 24,
    flex: 1,
  },
  header: {
    marginBottom: 96,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  body: {
    flex: 1,
  },
  title: {
    fontSize: 36,
    color: PDColors.textBlack,
    fontFamily: 'SFProDisplay-Bold',
    alignSelf: 'center',
    marginBottom: 16,
  },
  footer: {},
  row: {
    alignItems: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 16,
    borderBottomWidth: 1,
    borderColor: PDColors.buttonLightGray,
  },
  row_last: {
    borderBottomWidth: 0,
    marginBottom: 16,
  },
  order_view_title: {
    color: PDColors.primary,
    alignSelf: 'center',
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 16,
    marginBottom: 50,
  },
  comment_area: {
    borderWidth: 1,
    borderRadius: 8,
    borderColor: PDColors.buttonLightGray,
  },
  comment_title: {
    fontFamily: 'SFProDisplay-Regular',
    color: PDColors.textDark,
    marginBottom: 6,
  },
  button: {
    justifyContent: 'center',
  },
});
