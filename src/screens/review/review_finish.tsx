import React, {useCallback} from 'react';
import {View, Text, StyleSheet, Image, Dimensions} from 'react-native';
import PDColors from '../../constants/colors/colors';
import {DefaultPDButton} from '../../components/buttons/default-button';
import PDImages from '../../constants/images/images';
interface IPDReviewFinishProps {
  navigation: any;
}
export const PDReviewFinish = (props: IPDReviewFinishProps) => {
  const {navigation} = props;
  const goHandler = useCallback(() => navigation.pop(), [navigation]);
  return (
    <View style={styles.container}>
      <View style={styles.body}>
        <View style={styles.wrapper}>
          <Image
            source={PDImages.review_finish}
            resizeMethod={'resize'}
            resizeMode={'cover'}
            style={styles.image}
          />
          <Text style={styles.title}>Спасибо за отзыв!</Text>
          <Text style={styles.subtitle}>Мы стараемся быть лучше для вас</Text>
        </View>
      </View>
      <DefaultPDButton
        title={'Продолжить покупки'}
        onPress={goHandler}
        buttonStyle={styles.button}
      />
    </View>
  );
};
const dimensions = Dimensions.get('window');
const imageHeight = (dimensions.width * 59) / 82;
const imageWidth = dimensions.width - 24;
const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 24,
    paddingVertical: 50,
    flex: 1,
  },
  body: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapper: {
    alignItems: 'center',
  },
  title: {
    fontSize: 36,
    color: PDColors.textBlack,
    fontFamily: 'SFProDisplay-Bold',
  },
  subtitle: {
    fontSize: 16,
    color: PDColors.textDark,
    fontFamily: 'SFProDisplay-Regular',
  },
  image: {
    width: imageWidth,
    height: imageHeight,
    marginBottom: 32,
  },
  button: {
    justifyContent: 'center',
  },
});
