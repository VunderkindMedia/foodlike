import React, {useEffect, useMemo} from 'react';
import {StatusBar, View, StyleSheet, FlatList, Text} from 'react-native';
import {PDBanners} from '../../components/shop/banners/banners';
import PDColors from '../../constants/colors/colors';
import {PDSearchField} from '../../components/shop/search/search';
import {useProducts} from '../../redux/products/selectors';
import {PDProductsItem} from '../../components/products/products_item';
import {useCartList} from '../../redux/cart/selectors';
import {
  useCartAdd,
  useCartDecrement,
  useCartIncrement,
  useCartRemove,
} from '../../hooks/cart';
import {useIntro, useReviewOrder} from '../../redux/settings/selectors';
import {Routes} from '../../navigation/constants/routes';
import {useLoadReviewOrder} from '../../hooks/settings';
interface IPDShopProps {
  navigation: any;
}
export const PDShop = (props: IPDShopProps) => {
  const products = useProducts();
  const cartList = useCartList();
  const cartAdd = useCartAdd();
  const cartIncrement = useCartIncrement();
  const cartDecrement = useCartDecrement();
  const cartRemove = useCartRemove();
  const recommendedProducts = useMemo(
    () => products.filter(prod => prod.popular),
    [products],
  );
  const {navigation} = props;
  const intro = useIntro();
  const reviewOrder = useReviewOrder();
  const loadReviews = useLoadReviewOrder();

  useEffect(() => {
    loadReviews();
  }, [loadReviews]);
  useEffect(() => {
    if (!intro) {
      navigation.navigate(Routes.SCREEN_INTRO);
    }
    if (reviewOrder) {
      navigation.navigate(Routes.SCREEN_REVIEW);
    }
  }, [intro, navigation, reviewOrder]);
  const header: JSX.Element = (
    <View style={styles.header}>
      <PDSearchField navigation={navigation} style={styles.search} />
      <PDBanners />
      <Text style={styles.title}>Новинки</Text>
    </View>
  );
  return (
    <View style={styles.container}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor={PDColors.transparent}
        translucent
      />

      <FlatList
        contentContainerStyle={styles.list}
        numColumns={2}
        columnWrapperStyle={styles.list_columns}
        data={recommendedProducts}
        ListHeaderComponent={header}
        renderItem={({item}) => (
          <PDProductsItem
            hideLabel
            navigation={navigation}
            item={item}
            cartAdd={cartAdd}
            cartDecrement={cartDecrement}
            cartIncrement={cartIncrement}
            cartList={cartList}
            cartRemove={cartRemove}
          />
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: 40,
    alignItems: 'center',
    flex: 1,
    backgroundColor: PDColors.lightBackground,
  },
  body: {paddingHorizontal: 10, paddingBottom: 180},
  header: {
    marginBottom: 16,
  },
  list: {
    marginTop: 10,
    paddingBottom: 40,
  },
  search: {width: '100%', paddingHorizontal: 12, marginBottom: 12},
  title: {
    fontSize: 20,
    color: PDColors.textBlack,
    marginTop: 32,
    marginHorizontal: 15,
    fontFamily: 'SFProDisplay-Semibold',
  },
  list_columns: {
    justifyContent: 'space-between',
    paddingHorizontal: 14,
  },
});
