import React, {useCallback} from 'react';
import {StyleSheet, View} from 'react-native';
import {PDAddressEdit} from '../../components/addresses/address_edit';
import {Formik, FormikValues} from 'formik';
import {DefaultPDButton} from '../../components/buttons/default-button';
import PDColors from '../../constants/colors/colors';
import {Address} from '../../model/address';
import {useAddAddress} from '../../hooks/account';
import {TOrderAddressesNavigatorProp} from '../../navigation/navigators/product/types';
import {ADDRESS_LIST_BUTTON_TITLE} from '../../constants/strings';
interface IPDAddressCreateProps {
  navigation: TOrderAddressesNavigatorProp;
}
export const PDAddressCreate = (props: IPDAddressCreateProps) => {
  const {navigation} = props;
  const addAddress = useAddAddress();
  const createHandler = useCallback(
    (val: FormikValues) => {
      const address = Address.Builder.withApartment(val.apart)
        .withEntrance(val.entrance)
        .withFloor(val.floor)
        .withStreet(val.street)
        .build();
      addAddress(address);
      navigation.pop();
    },
    [addAddress, navigation],
  );
  return (
    <View style={styles.container}>
      <Formik
        initialValues={{
          street: '',
          floor: '',
          entrance: '',
          apart: '',
        }}
        onSubmit={createHandler}>
        {({handleSubmit, values, errors, isValid, dirty, handleChange}) => (
          <>
            <View style={styles.wrapper}>
              <PDAddressEdit
                street={values.street}
                apart={values.apart}
                entrance={values.entrance}
                floor={values.floor}
                onApartChange={handleChange('apart')}
                onEntranceChange={handleChange('entrance')}
                onFloorChange={handleChange('floor')}
                onStreetChange={handleChange('street')}
              />
            </View>
            <DefaultPDButton
              backgroundColor={
                isValid && dirty ? PDColors.primary : PDColors.buttonLightGray
              }
              title={ADDRESS_LIST_BUTTON_TITLE}
              onPress={handleSubmit}
              buttonStyle={styles.button}
            />
          </>
        )}
      </Formik>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 24,
    paddingBottom: 20,
    paddingTop: 14,
    flex: 1,
  },
  wrapper: {
    flex: 1,
  },
  button: {
    justifyContent: 'center',
  },
});
