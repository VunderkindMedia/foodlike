import React, {useCallback} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {useAddresses} from '../../redux/account/selectors';
import {PDAddressItem} from '../../components/addresses/address_item';
import {PDAddressListFooter} from '../../components/addresses/address_list_footer';
import {
  TOrderAddressesNavigatorProp,
  TOrderAddressesRouteProp,
} from '../../navigation/navigators/product/types';
import {Routes} from '../../navigation/constants/routes';
import {DefaultPDButton} from '../../components/buttons/default-button';
import PDColors from '../../constants/colors/colors';
import {ADDRESS_LIST_BUTTON_TITLE} from '../../constants/strings';
interface IPDAddressListProps {
  navigation: TOrderAddressesNavigatorProp;
  route: TOrderAddressesRouteProp;
}
export const PDAddressList = (props: IPDAddressListProps) => {
  const {navigation, route} = props;
  const onConfirm = route.params?.onConfirm;
  const addresses = useAddresses();
  const addHandler = useCallback(
    () => navigation.push(Routes.SCREEN_ADDRESS_CREATE),
    [navigation],
  );
  const handleSubmit = useCallback(() => {
    if (onConfirm) {
      onConfirm();
    }
  }, [onConfirm]);
  return (
    <View style={styles.container}>
      <View style={styles.list_wrapper}>
        <FlatList
          contentContainerStyle={styles.list_container}
          data={addresses}
          renderItem={({item}) => <PDAddressItem address={item} />}
          ListFooterComponent={<PDAddressListFooter onPress={addHandler} />}
        />
      </View>
      {onConfirm && (
        <View style={styles.button_wrapper}>
          <DefaultPDButton
            backgroundColor={PDColors.primary}
            title={ADDRESS_LIST_BUTTON_TITLE}
            onPress={handleSubmit}
            buttonStyle={styles.button}
          />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list_container: {
    paddingBottom: 32,
    paddingHorizontal: 24,
  },
  list_wrapper: {
    flex: 0.9,
  },
  button_wrapper: {
    flex: 0.1,
    paddingVertical: 10,
    paddingHorizontal: 24,
    backgroundColor: PDColors.textWhite,
  },
  button: {
    justifyContent: 'center',
  },
});
