import React, {useCallback} from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  Image,
  Linking,
} from 'react-native';
import {useClient} from '../../redux/account/selectors';
import PDColors from '../../constants/colors/colors';
import {PDIcons} from '../../components/icons/custom-icons';
import PDImages from '../../constants/images/images';
import {PDTileDefault} from '../../components/tiles/default-tile';
import {useLogout} from '../../hooks/account';
import {LogoutAlert} from '../../constants/allerts';
import RNBounceable from '@freakycoder/react-native-bounceable';
import {Routes} from '../../navigation/constants/routes';
interface IPDProfileProps {
  navigation: any;
}
export const PDProfile = (props: IPDProfileProps) => {
  const {navigation} = props;
  const logoutAlert = LogoutAlert();
  const logout = useLogout();
  const phoneHandler = useCallback(() => Linking.openURL('tel:29-56-29'), []);
  const instaHandler = useCallback(
    () => Linking.openURL('https://instagram.com/foodlike_65'),
    [],
  );
  const exitHandler = useCallback(
    () => logoutAlert(logout),
    [logout, logoutAlert],
  );
  const addressesHandler = useCallback(
    () => navigation.navigate(Routes.SCREEN_ADDRESSES),
    [navigation],
  );
  const ordersHandler = useCallback(
    () => navigation.navigate(Routes.SCREEN_ORDERS),
    [navigation],
  );
  const editHandler = useCallback(
    () => navigation.push(Routes.SCREEN_PROFILE_EDIT),
    [navigation],
  );
  const client = useClient();
  return (
    <ScrollView style={styles.profile__container}>
      <View style={styles.profile__header}>
        <View style={styles.profile__personal_info}>
          <Text style={styles.profile__name}>
            {client?.firstname ?? ''} {client?.lastname ?? ''}
            {!client?.firstname && !client?.lastname && 'Имя не указано'}
          </Text>
          <Text style={styles.profile__phone}>{client?.phone}</Text>
        </View>
        <View>
          <RNBounceable
            style={styles.profile__edit_icon_wrapper}
            onPress={editHandler}>
            <PDIcons name={'pencil'} color={PDColors.primary} size={14} />
          </RNBounceable>
        </View>
      </View>
      <View style={styles.profile__bonuses}>
        <Image style={styles.profile__bonuses_image} source={PDImages.money} />
        <Text style={styles.profile__bonuses_title}>{`${(client?.bonus ?? 0)
          .toFixed(2)
          .replace('.', ',')} бонусов`}</Text>
      </View>
      <PDTileDefault
        icon_name={'file'}
        text={'Мои заказы'}
        onPress={ordersHandler}
      />
      <PDTileDefault
        icon_name={'map_pin'}
        text={'Адреса доставки'}
        onPress={addressesHandler}
      />
      <PDTileDefault icon_name={'info'} text={'О нас'} onPress={() => {}} />
      <View style={styles.profile__contact_wrapper}>
        <Text style={styles.profile__contact_title}>Связаться с нами</Text>
        <PDTileDefault
          icon_name={'phone'}
          text={'Позвонить'}
          onPress={phoneHandler}
        />
        <PDTileDefault
          icon_name={'instagram'}
          text={'Instagram'}
          onPress={instaHandler}
        />
      </View>

      <TouchableOpacity
        style={styles.profile__exit_button_wrapper}
        onPress={exitHandler}>
        <Text style={styles.profile__exit_button}>Выйти</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  profile__container: {
    paddingTop: 90,
    paddingHorizontal: 24,
  },
  profile__wrapper: {
    flex: 1,
  },
  profile__header: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginBottom: 32,
  },
  profile__personal_info: {
    flexDirection: 'column',
  },
  profile__name: {
    fontFamily: 'SFProDisplay-Semibold',
    fontWeight: '600',
    fontSize: 24,
    color: PDColors.textBlack,
    marginBottom: 8,
  },
  profile__phone: {
    fontSize: 16,
    color: PDColors.textDark,
  },
  profile__edit_icon_wrapper: {
    backgroundColor: PDColors.lightDefaultBackground,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 8,
    borderRadius: 8,
  },
  profile__bonuses: {
    flexDirection: 'row',
    backgroundColor: PDColors.lightDefaultBackground,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 26,
    borderRadius: 16,
    marginBottom: 32,
  },
  profile__bonuses_image: {
    marginRight: 10,
  },
  profile__bonuses_title: {
    fontSize: 24,
    fontFamily: 'SFProDisplay-Semibold',
    color: PDColors.textBlack,
  },
  profile__contact_wrapper: {
    paddingVertical: 40,
  },
  profile__contact_title: {
    color: PDColors.textBlack,
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 16,
    marginBottom: 6,
  },
  profile__exit_button_wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  profile__exit_button: {
    color: PDColors.primary,
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 20,
  },
});
