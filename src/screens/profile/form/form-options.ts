import {FormikValues} from 'formik';
import {Client} from '../../../model/client';

export interface IPDProfileEditForm extends FormikValues {
  firstname: string;
  lastname: string;
  phone: string;
  email: string;
  birthday: string;
  client_sex: string;
}
export const formInitVal = (client: Client): IPDProfileEditForm => ({
  firstname: client.firstname,
  lastname: client.lastname,
  phone: client.phone,
  email: client.email,
  birthday: client.birthday,
  client_sex: client.client_sex,
});
