import React, {useCallback, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  StyleSheet,
} from 'react-native';
import {useClient} from '../../redux/account/selectors';
import PDColors from '../../constants/colors/colors';
import {PDIcons} from '../../components/icons/custom-icons';
import {Formik} from 'formik';
import {formInitVal, IPDProfileEditForm} from './form/form-options';
import {PDInput} from '../../components/inputs/default_input/input';
import {PDCheckbox} from '../../components/checkboxes/checkbox';
import {DefaultPDButton} from '../../components/buttons/default-button';
import {useUpdateClient} from '../../hooks/account';
import {Client} from '../../model/client';
import {PDLoader} from '../../components/loader/loader';

interface IPDProfileEditProps {
  navigation: any;
}
export const PDProfileEdit = (props: IPDProfileEditProps) => {
  const {navigation} = props;
  const [loading, setLoading] = useState(false);
  const client = useClient();
  const clientSave = useUpdateClient();
  const handleSubmit = useCallback(
    async (values: IPDProfileEditForm) => {
      if (!client) {
        return;
      }
      setLoading(true);
      const new_client = Client.fromForm(values, client);
      await clientSave(new_client, () => navigation.pop());
    },
    [client, clientSave, navigation],
  );
  return (
    <View style={styles.profile__container}>
      <StatusBar translucent backgroundColor="transparent" />
      <View style={styles.profile__top_line} />
      {loading && <PDLoader fullscreen />}
      <View style={styles.profile__wrapper}>
        <View style={styles.profile__header_wrapper}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.profile__header_close}
            onPress={() => navigation.pop()}>
            <PDIcons name="close" size={12} color={PDColors.textBlack} />
          </TouchableOpacity>
          <Text style={styles.profile__edit_title}>Мои данные</Text>
        </View>
        <View style={styles.profile__body_wrapper}>
          <Formik initialValues={formInitVal(client!)} onSubmit={handleSubmit}>
            {({handleSubmit, values, setFieldValue}) => {
              return (
                <>
                  <PDInput
                    style={styles.profile__edit_input}
                    onChangeText={val => setFieldValue('firstname', val)}
                    label={'Имя'}
                    value={values.firstname}
                  />
                  <PDInput
                    style={styles.profile__edit_input}
                    onChangeText={val => setFieldValue('lastname', val)}
                    label={'Фамилия'}
                    value={values.lastname}
                  />
                  <PDInput
                    type={'phone'}
                    style={styles.profile__edit_input}
                    onChangeText={val => setFieldValue('phone', val)}
                    label={'Телефон'}
                    value={values.phone}
                  />
                  <PDInput
                    style={styles.profile__edit_input}
                    onChangeText={val => setFieldValue('email', val)}
                    label={'Эл. почта'}
                    value={values.email}
                  />
                  <PDInput
                    style={styles.profile__edit_input}
                    onChangeText={val => setFieldValue('birthday', val)}
                    label={'Дата рождения'}
                    value={values.birthday}
                  />
                  <Text style={styles.profile__edit_checkbox_title}>Пол</Text>
                  <View style={styles.profile__edit_sex_wrapper}>
                    <PDCheckbox
                      style={styles.profile__edit_checkbox}
                      reverse
                      onChange={val => setFieldValue('client_sex', val)}
                      keyId={'2'}
                      checked={values.client_sex === '2'}
                      title={'Женский'}
                    />
                    <PDCheckbox
                      style={styles.profile__edit_checkbox}
                      reverse
                      onChange={val => setFieldValue('client_sex', val)}
                      keyId={'1'}
                      checked={values.client_sex === '1'}
                      title={'Мужской'}
                    />
                  </View>
                  <View style={styles.profile__edit_button_wrapper}>
                    <DefaultPDButton
                      buttonStyle={styles.profile_edit_button}
                      title={'Сохранить'}
                      onPress={handleSubmit}
                    />
                  </View>
                </>
              );
            }}
          </Formik>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  profile__container: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  profile__top_line: {
    width: 48,
    height: 5,
    backgroundColor: PDColors.buttonLightGray,
    alignSelf: 'center',
    marginBottom: 7,
  },
  profile__wrapper: {
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: PDColors.whiteBackground,
    height: '90%',
    overflow: 'hidden',
  },
  profile__edit_title: {
    fontSize: 20,
    fontFamily: 'SFProDisplay-Bold',
    marginTop: 25,
    color: PDColors.textBlack,
  },
  profile__header_wrapper: {
    position: 'relative',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  profile__header_close: {
    position: 'absolute',
    top: 20,
    right: 20,
    height: 32,
    width: 32,

    borderRadius: 50,
    zIndex: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },

  profile__body_wrapper: {
    paddingHorizontal: 24,
    paddingVertical: 16,
    flex: 1,
  },
  profile__edit_sex_wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  profile__edit_checkbox: {
    marginRight: 42,
  },
  profile__edit_checkbox_title: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 16,
    marginTop: 16,
    marginBottom: 16,
  },
  profile__edit_input: {
    marginTop: 16,
    paddingBottom: 8,
  },
  profile__edit_button_wrapper: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  profile_edit_button: {
    justifyContent: 'center',
  },
});
