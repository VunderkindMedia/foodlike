import React, {useCallback, useMemo, useState} from 'react';
import {StyleSheet, RefreshControl, View, FlatList} from 'react-native';
import {
  TCategoriesNavigatorProp,
  TCategoriesRouteProp,
} from '../../navigation/navigators/product/types';
import {Routes} from '../../navigation/constants/routes';
import PDColors from '../../constants/colors/colors';
import {PDSearchField} from '../../components/shop/search/search';
import {useRequestStateIsSuccess} from '../../redux/request/selectors';
import {useCategories} from '../../redux/products/selectors';
import {useLoadProductsAndCats} from '../../hooks/products';
import {paginate} from '../../helpers/lists';
import {PDCategoryItem} from '../../components/products/category_item';

interface IPDCategoriesProps {
  navigation: TCategoriesNavigatorProp;
  route: TCategoriesRouteProp;
}

export const PDCategories = (props: IPDCategoriesProps) => {
  const {navigation, route} = props;
  const [page, setPage] = useState(1);
  const loadCategories = useLoadProductsAndCats();
  const categoriesRequest = useRequestStateIsSuccess('categories');
  const categories = useCategories();

  const selectedCategories = useMemo(
    () =>
      categories.filter(
        category => category.parent_category === (route.params?.parent_id ?? 0),
      ),
    [categories, route.params?.parent_id],
  );
  const goToSearchHandler = useCallback(
    () => navigation.push(Routes.SCREEN_SEARCH),
    [navigation],
  );

  const handleRefresh = useCallback(() => {
    loadCategories();
  }, [loadCategories]);
  return (
    <View style={styles.categories__container}>
      <PDSearchField navigation={navigation} style={styles.header} />
      <FlatList
        refreshControl={
          <RefreshControl
            colors={[PDColors.primary, PDColors.primary]}
            refreshing={!categoriesRequest}
            onRefresh={handleRefresh}
            progressViewOffset={0}
          />
        }
        onEndReached={() => setPage(prev => prev + 1)}
        onEndReachedThreshold={0.05}
        data={paginate(selectedCategories, 10, page)}
        renderItem={({item}) => (
          <PDCategoryItem
            item={item}
            navigation={navigation}
            categories={categories}
          />
        )}
        numColumns={1}
        keyExtractor={item => item.category_name}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  categories__container: {
    flex: 1,
    paddingHorizontal: 14,
  },

  header: {
    width: '100%',
    marginBottom: 10,
  },
});
