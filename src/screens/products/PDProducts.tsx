import React, {useLayoutEffect} from 'react';
import {IPDProductList} from '../../components/products/product_list';
import {
  TProductsNavigatorProp,
  TProductsRouteProp,
} from '../../navigation/navigators/product/types';
interface IPDProducts {
  navigation: TProductsNavigatorProp;
  route: TProductsRouteProp;
}
export const PDProducts = (props: IPDProducts) => {
  const {navigation, route} = props;
  useLayoutEffect(() => {
    navigation.setOptions({headerTitle: route.params.title});
  }, [navigation, route.params.title]);

  return (
    <IPDProductList
      category_id={route.params.category_id}
      navigation={navigation}
    />
  );
};
