import React, {useCallback, useMemo} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  StatusBar,
  StyleSheet,
  Dimensions,
} from 'react-native';
import {
  useCartAdd,
  useCartDecrement,
  useCartIncrement,
  useCartRemove,
} from '../../hooks/cart';
import {useCartList} from '../../redux/cart/selectors';
import PDImages from '../../constants/images/images';
import {useFavorites} from '../../redux/products/selectors';
import {useAddFavorite, useRemoveFavorite} from '../../hooks/products';
import {useClient} from '../../redux/account/selectors';
import {
  TProductNavigatorProp,
  TProductRouteProp,
} from '../../navigation/navigators/product/types';
import PDColors from '../../constants/colors/colors';
import {DefaultPDButton} from '../../components/buttons/default-button';
import {PDIcons} from '../../components/icons/custom-icons';
import {discount} from '../../helpers/formatters';
import RNBounceable from '@freakycoder/react-native-bounceable';
import {
  PRODUCT_ADD_CART_TITLE,
  PRODUCT_BONUSES_TITLE,
  PRODUCT_DESCRIPTION_TITLE,
} from '../../constants/strings';
import RenderHtml from 'react-native-render-html';
import {useWindowDimensions} from 'react-native';

interface IPDProductProps {
  route: TProductRouteProp;
  navigation: TProductNavigatorProp;
}
export const PDProduct = (props: IPDProductProps) => {
  const {route, navigation} = props;
  const cartIncrement = useCartIncrement();
  const cartDecrement = useCartDecrement();
  const cartAdd = useCartAdd();
  const cartRemove = useCartRemove();
  const client = useClient();
  const addFavorite = useAddFavorite();
  const removeFavorite = useRemoveFavorite();
  const favorites = useFavorites();
  const cartList = useCartList();
  const item = route.params.item;

  const cartItem = useMemo(
    () => cartList.find(product => product.pos_id === item.pos_id),
    [cartList, item.pos_id],
  );

  const cartCount = useMemo(() => cartItem?.cartCount ?? 0, [cartItem]);
  const isFavorite = useMemo(
    () => favorites.find(favorite_item => item.pos_id === favorite_item.pos_id),
    [favorites, item],
  );
  const bonuses = useMemo(
    () =>
      discount(
        client?.client_groups_discount ?? 0,
        item.price,
        cartCount === 0 ? 1 : cartCount,
      ),
    [cartCount, client?.client_groups_discount, item.price],
  );

  const cartAddHandler = useCallback(() => {
    cartAdd(item);
  }, [cartAdd, item]);

  const favoriteHandler = useCallback(
    () => (isFavorite ? removeFavorite(item) : addFavorite(item)),

    [addFavorite, isFavorite, item, removeFavorite],
  );

  const minusHandler = useCallback(() => {
    if (cartCount > 1) {
      cartDecrement(item);
    } else {
      cartRemove(item);
    }
  }, [cartCount, cartDecrement, cartRemove, item]);

  const plusHandler = useCallback(
    () => cartIncrement(item),
    [cartIncrement, item],
  );
  const dimensions = useWindowDimensions();
  return (
    <View style={styles.product__container}>
      <StatusBar translucent backgroundColor="transparent" />
      <View style={styles.product__top_line} />
      <View style={styles.product__wrapper}>
        <View style={styles.product__image_wrapper}>
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.product__header_close}
            onPress={() => navigation.pop()}>
            <PDIcons name="close" size={12} color="#fff" />
          </TouchableOpacity>
          <Image
            style={styles.product__image}
            source={item.photo ? {uri: item.photo} : PDImages.no_photo}
          />
        </View>
        <View style={styles.product__body_wrapper}>
          <View style={styles.product__body_header}>
            <View style={styles.product__body_title_wrapper}>
              <Text style={styles.product__title}>{item.name}</Text>
              <TouchableOpacity
                onPress={favoriteHandler}
                style={styles.product__favorite_wrapper}>
                {isFavorite ? (
                  <PDIcons
                    name={'heart_fill'}
                    size={24}
                    color={
                      isFavorite ? PDColors.primary : PDColors.normalGrayText
                    }
                  />
                ) : (
                  <PDIcons
                    name={'favorites'}
                    size={28}
                    color={
                      isFavorite ? PDColors.primary : PDColors.normalGrayText
                    }
                  />
                )}
              </TouchableOpacity>
            </View>
            <Text style={styles.product__weight}>{item.seoDescription}</Text>
          </View>
          <View style={styles.product__body_middle}>
            <Text style={styles.product__description_title}>
              {PRODUCT_DESCRIPTION_TITLE}
            </Text>
            {item.description && (
              <RenderHtml
                contentWidth={dimensions.width}
                defaultTextProps={{style: styles.product__description}}
                source={{html: item.description?.replace('b', 'div')}}
              />
            )}
          </View>
          <View style={styles.product__body_bottom}>
            {!!client?.client_groups_discount && (
              <View style={styles.product__bonuses_wrapper}>
                <Image
                  style={styles.product__bonuses_image}
                  source={PDImages.money}
                />
                <Text style={styles.product__bonuses_title}>
                  {PRODUCT_BONUSES_TITLE}
                </Text>

                <Text style={styles.product__bonuses_value}>{bonuses} ₽</Text>
              </View>
            )}
            {cartCount === 0 ? (
              <DefaultPDButton
                title={PRODUCT_ADD_CART_TITLE}
                onPress={cartAddHandler}
                suffixValue={item.discountPrice ?? item.price}
                crossedSuffixValue={item.discountPrice ? item.price : undefined}
              />
            ) : (
              <View style={styles.product__cart_block}>
                <View style={styles.product__item_bottom_wrapper}>
                  <RNBounceable
                    style={styles.products__item_plus_btn}
                    onPress={minusHandler}>
                    <PDIcons name="minus" size={18} color={PDColors.primary} />
                  </RNBounceable>
                  <View>
                    <Text style={[styles.product__count]}>{cartCount} шт</Text>
                  </View>
                  <RNBounceable
                    style={[
                      styles.products__item_plus_btn,
                      {backgroundColor: PDColors.primary},
                    ]}
                    onPress={plusHandler}>
                    <PDIcons name="plus" size={18} color={'#fff'} />
                  </RNBounceable>
                </View>
                <Text style={styles.product__amount}>
                  {item.discountPrice ?? item.price * cartCount} ₽
                </Text>
              </View>
            )}
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  product__container: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  product__top_line: {
    width: 48,
    height: 5,
    backgroundColor: PDColors.buttonLightGray,
    alignSelf: 'center',
    marginBottom: 7,
  },
  product__wrapper: {
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: PDColors.whiteBackground,
    height: '90%',
    overflow: 'hidden',
  },
  product__image_wrapper: {
    position: 'relative',
    width: '100%',
    justifyContent: 'flex-start',
  },
  product__header_close: {
    position: 'absolute',
    top: 20,
    right: 20,
    height: 32,
    width: 32,
    backgroundColor: '#8F8F8FA1',
    borderRadius: 50,
    zIndex: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  product__image: {
    width: '100%',
    height: Dimensions.get('window').width / 1.5,
    resizeMode: 'contain',
  },
  product__body_wrapper: {
    paddingHorizontal: 24,
    paddingVertical: 16,
    flex: 1,
  },
  product__body_header: {
    paddingBottom: 16,
    borderBottomWidth: 1,
    borderBottomColor: PDColors.buttonLightGray,
  },
  product__body_title_wrapper: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  product__title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 24,
    color: PDColors.textBlack,
    flex: 1,
  },
  product__favorite_wrapper: {
    height: 36,
    width: 36,
    // marginTop: -18,
    justifyContent: 'center',
    alignItems: 'center',
  },
  product__weight: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 16,
    color: PDColors.textDark,
  },
  product__body_middle: {
    paddingVertical: 16,
    flex: 1,
  },
  product__description_title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 16,
    color: PDColors.textDark,
    marginBottom: 8,
  },
  product__description: {
    fontFamily: 'SFProDisplay',
    fontSize: 16,
    color: PDColors.textBlack,
  },
  product__body_bottom: {},
  product__bonuses_wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 17,
    marginBottom: 16,
    borderBottomWidth: 1,
    borderBottomColor: PDColors.buttonLightGray,
  },
  product__bonuses_image: {
    marginRight: 8,
  },
  product__bonuses_title: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 16,
    color: PDColors.textDark,
  },
  product__bonuses_value: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 16,
    color: PDColors.textBlack,
  },
  product__item_bottom_wrapper: {
    flexDirection: 'row',
    width: '45%',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  product__cart_block: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  products__item_plus_btn: {
    height: 36,
    width: 36,
    borderRadius: 8,
    backgroundColor: PDColors.whiteBackground,
    borderColor: PDColors.primary,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 8,
  },
  product__amount: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 24,
    color: PDColors.textBlack,
  },
  product__count: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 16,
    color: PDColors.textBlack,
  },
  product__html_view: {
    backgroundColor: PDColors.transparent,
    padding: 0,
    margin: 0,
  },
});
