import React, {
  useState,
  useLayoutEffect,
  useRef,
  useEffect,
  useCallback,
} from 'react';
import {View, Text, TextInput, FlatList, StyleSheet} from 'react-native';
import {useProducts} from '../../redux/products/selectors';
import {Product} from '../../model/product';
import {PDProductsItem} from '../../components/products/products_item';
import {
  useCartAdd,
  useCartDecrement,
  useCartIncrement,
  useCartRemove,
} from '../../hooks/cart';
import {useCartList} from '../../redux/cart/selectors';
import {PDIcons} from '../../components/icons/custom-icons';
import PDColors from '../../constants/colors/colors';

interface IPDSearchProps {
  navigation: any;
}

export const PDSearch = (props: IPDSearchProps) => {
  const {navigation} = props;
  const [searchValue, setSearchValue] = useState('');
  const [filteredProducts, setFilteredProducts] = useState<Product[]>([]);
  const products = useProducts();
  const cartIncrement = useCartIncrement();
  const cartDecrement = useCartDecrement();
  const cartAdd = useCartAdd();
  const cartRemove = useCartRemove();
  const cartList = useCartList();
  const searchInput = useRef<TextInput>(null);

  const searchHandler = useCallback(val => setSearchValue(val), []);

  useLayoutEffect(() => {
    setFilteredProducts(
      products.filter(item => {
        if (item.name.toLowerCase().includes(searchValue.toLowerCase())) {
          return item;
        }
      }),
    );
  }, [products, searchValue]);

  useEffect(() => {
    searchInput.current?.focus();
  }, []);

  return (
    <View style={styles.search__container}>
      <View style={styles.categories__search_wrapper}>
        <PDIcons
          name="search"
          size={20}
          color={'#8E8E93'}
          style={styles.search_icon}
        />
        <TextInput
          style={styles.categories__search_input}
          placeholder="Поиск..."
          value={searchValue}
          ref={searchInput}
          onChangeText={searchHandler}
          placeholderTextColor={PDColors.normalGrayText}
        />
      </View>
      {searchValue.length !== 0 && filteredProducts.length === 0 ? (
        <View style={styles.emptySearchTextWrapper}>
          <Text style={[styles.emptySearchText, styles.emptySearchTextBig]}>
            Ничего не найдено
          </Text>
        </View>
      ) : searchValue.length === 0 ? (
        <View style={styles.emptySearchTextWrapper}>
          <Text style={styles.emptySearchText}>Введите слово для поиска</Text>
        </View>
      ) : (
        <FlatList
          columnWrapperStyle={styles.products__flat_container}
          data={filteredProducts}
          keyboardShouldPersistTaps={'handled'}
          renderItem={({item}) => (
            <PDProductsItem
              cartAdd={cartAdd}
              cartDecrement={cartDecrement}
              cartIncrement={cartIncrement}
              cartList={cartList}
              cartRemove={cartRemove}
              hideLabel={false}
              item={item}
              navigation={navigation}
            />
          )}
          numColumns={2}
          keyExtractor={item => item.name + item.pos_id}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  search__container: {
    flex: 1,
  },
  header: {
    fontFamily: 'SFProDisplay-Regular',
    color: PDColors.normalGrayText,
    marginHorizontal: 10,
    marginBottom: 5,
  },
  emptyCategories: {
    fontFamily: 'SFProDisplay-Regular',
    color: PDColors.normalGrayText,
    fontSize: 14,
  },
  emptySearchTextWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptySearchText: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 16,
    color: PDColors.normalGrayText,
  },
  emptySearchTextBig: {fontSize: 20},
  iconEmpty: {
    marginBottom: 15,
  },
  categories__search_wrapper: {
    backgroundColor: 'rgba(118, 118, 128, 0.12)',
    height: 52,
    borderRadius: 15,
    paddingHorizontal: 15,
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 12.5,
    marginTop: 16,
    marginHorizontal: 10,
  },
  categories__search_input: {
    flex: 1,
  },
  categories__search_icon: {
    marginRight: 10,
  },
  categories__flat_container: {
    width: '100%',
    paddingHorizontal: 10,
  },
  products__flat_container: {
    paddingHorizontal: 14,
    justifyContent: 'space-between',
  },
  search_icon: {
    marginRight: 10,
  },
  text: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 18,
    marginVertical: 10,
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    height: 20,
    width: 20,
    borderRadius: 50,
    marginRight: 10,
  },
});
