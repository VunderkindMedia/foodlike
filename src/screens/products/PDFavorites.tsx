import React, {useCallback} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {PDEmpty} from '../cart/PDEmpty';
import PDImages from '../../constants/images/images';
import {useFavorites} from '../../redux/products/selectors';
import {Routes} from '../../navigation/constants/routes';
import {
  TCategoriesNavigatorProp,
  TCategoriesRouteProp,
} from '../../navigation/navigators/product/types';
import {FavoritesItem} from '../../components/products/favorites_item';
import {Tabs} from '../../navigation/constants/tabs';

interface IPDFavoritesProps {
  navigation: TCategoriesNavigatorProp;
  route: TCategoriesRouteProp;
}

export const PDFavorites = (props: IPDFavoritesProps) => {
  const {navigation} = props;
  const favorites = useFavorites();

  const emptyGoHandler = useCallback(() => {
    navigation.navigate(Tabs.TAB_PRODUCTS);
  }, [navigation]);

  const goHandler = useCallback(
    item => navigation.navigate(Routes.SCREEN_PRODUCT, {item: item}),
    [navigation],
  );

  return (
    <View style={styles.container}>
      {favorites.length === 0 && (
        <PDEmpty
          title={'Любимые товары'}
          imagePath={PDImages.favorites_empty}
          subtitle={
            'Здесь будут любимые товары. Для этого откройте карточку товара и нажмите на сердечко.'
          }
          onClick={emptyGoHandler}
        />
      )}
      {favorites.length !== 0 && (
        <View style={styles.container}>
          <FlatList
            data={favorites}
            renderItem={({item}) => (
              <FavoritesItem item={item} onPress={goHandler} inOrder={false} />
            )}
            keyExtractor={item => item.name + item.pos_id}
          />
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
