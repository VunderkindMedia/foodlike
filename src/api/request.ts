import {Store} from 'redux';
import {IAppState} from '../redux/root-reducer';

let instance: Request | null = null;

export enum RequestMethods {
  GET = 'GET',
  POST = 'POST',
  DELETE = 'DELETE',
}

export interface IRequestOptions {
  method?: RequestMethods;
  body?: string;
  headers?: {[key: string]: string};
}

class Request {
  private _store!: Store<IAppState>;
  private _defaultOptions: IRequestOptions = {
    method: RequestMethods.GET,
  };

  constructor() {
    if (!instance) {
      instance = this;
    }

    return instance;
  }

  init(store: Store<IAppState>): void {
    this._store = store;
  }

  fetch = async (url: string, options?: IRequestOptions): Promise<Response> => {
    options = {
      ...this._defaultOptions,
      ...options,
    };

    const response = await fetch(url, options);

    return response;
  };
}

export const request = new Request();
