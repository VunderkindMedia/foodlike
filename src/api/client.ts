import {Client, IClientJson} from '../model/client';
import {
  API_CLIENT_CREATE,
  API_CLIENT_FIND,
  API_CLIENT_GET,
  API_CLIENT_UPDATE,
  API_CLIENT_SAVE_TOKEN,
} from '../constants/api';
import {request, RequestMethods} from './request';

export const client = {
  get: async (id: number): Promise<Client> => {
    const response = await request.fetch(API_CLIENT_GET(id));
    const jsonResponse = await response.json();
    const json = jsonResponse.response[0] as IClientJson;
    return Client.parse(json);
  },
  find: async (phone: string): Promise<Client | null> => {
    const response = await request.fetch(API_CLIENT_FIND(phone));
    const jsonResponse = await response.json();
    const json = jsonResponse.response[0] as IClientJson;
    if (!json) {
      return null;
    }
    return Client.parse(json);
  },
  create: async (client: Client): Promise<number> => {
    const response = await request.fetch(API_CLIENT_CREATE, {
      method: RequestMethods.POST,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(client),
    });
    const json = await response.json();

    return json.response as number;
  },
  update: async (client: Client): Promise<number> => {
    const response = await request.fetch(API_CLIENT_UPDATE, {
      method: RequestMethods.POST,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(client),
    });
    const json = await response.json();

    return json.response as number;
  },
  save_token: async (
    token: string,
    os: string,
    phone: string,
  ): Promise<void> => {
    const response = await request.fetch(
      API_CLIENT_SAVE_TOKEN(token, os, phone),
    );
    const json = await response.json();
  },
};
