import {request} from './request';
import {API_SEND_CALL, API_SEND_SMS} from '../constants/api';

export interface CallResponse {
  status: string;
  code: string;
  call_id: string;
  cost: number;
  balance: number;
}

export const sms = {
  send: async (code: string, phone: string): Promise<void> => {
    await request.fetch(API_SEND_SMS(code, phone));
  },
  call: async (phone: string): Promise<CallResponse> => {
    const response = await request.fetch(API_SEND_CALL(phone));
    return await response.json();
  },
};
