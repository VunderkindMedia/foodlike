import {request, RequestMethods} from './request';
import {IBackClientsJSON, ISettingsJSON, Settings} from '../model/settings';
import {
  API_CANCEL_REVIEW,
  API_PAYMENT_TYPES,
  API_REVIEWS,
  API_SEND_REVIEW,
  API_SETTINGS,
} from '../constants/api';
import {IPaymentJson, Payment} from '../model/payment';
import {lockedPayTypes} from '../constants/payments';
import {Order} from '../model/order';
import {IReviewInitial} from '../screens/review/review';

export const settings = {
  getSettings: async (): Promise<Settings> => {
    const response = await request.fetch(API_SETTINGS);
    const jsonResponse = await response.json();
    const json = jsonResponse.data as ISettingsJSON;

    return Settings.parse(json);
  },
  getReview: async (client_phone: string): Promise<Order | null> => {
    const response = await request.fetch(API_REVIEWS(client_phone));
    const jsonResponse = await response.json();
    const json = jsonResponse.data as IBackClientsJSON[];
    if (json.length === 0 || json[0].last_order.length === 0) {
      return null;
    }
    return Order.parse(json[0].last_order[json[0].last_order.length - 1]);
  },
  getPaymentTypes: async (): Promise<Payment[]> => {
    const response = await request.fetch(API_PAYMENT_TYPES);
    const jsonResponse = await response.json();
    const json = jsonResponse.response as IPaymentJson[];
    return json
      .filter(
        p => p.is_active === 1 && !lockedPayTypes.includes(p.payment_type),
      )
      .map(p => Payment.parse(p));
  },
  sendReview: async (
    values: IReviewInitial | undefined,
    id: number,
    phone: string,
  ): Promise<void> => {
    await request.fetch(API_SEND_REVIEW, {
      method: RequestMethods.POST,
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({...values, id: id, phone: phone}),
    });
  },
  cancelReview: async (phone: string, id: number): Promise<void> => {
    await request.fetch(API_CANCEL_REVIEW(phone, id), {
      method: RequestMethods.GET,
    });
  },
};
