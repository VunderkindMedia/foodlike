import {settings} from './settings';
import {request} from './request';
import {client} from './client';
import {sms} from './sms';
import {products} from './products';
import {orders} from './orders';

export const api = {
  settings,
  request,
  client,
  sms,
  products,
  orders,
};
