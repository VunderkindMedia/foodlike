import {request} from './request';
import {
  API_CATEGORIES_GET,
  API_PRODUCTS_GET,
  API_PROMOTIONS_GET,
} from '../constants/api';
import {IProductJson, Product} from '../model/product';
import {Category, ICategoryJson} from '../model/category';
import {IPromotionJson, Promotion} from '../model/promotion';

export const products = {
  getProducts: async (): Promise<Product[]> => {
    const response = await request.fetch(API_PRODUCTS_GET);
    const jsonResponse = await response.json();
    const json = jsonResponse.data as IProductJson[];

    return json.map(product => Product.parse(product));
  },
  getPromotions: async (): Promise<Promotion[]> => {
    const response = await request.fetch(API_PROMOTIONS_GET);
    const jsonResponse = await response.json();
    const json = jsonResponse.response as IPromotionJson[];
    return json.map(promotion => Promotion.parse(promotion));
  },
  getCategories: async (): Promise<Category[]> => {
    const response = await request.fetch(API_CATEGORIES_GET);
    const jsonResponse = await response.json();
    const json = jsonResponse.response as ICategoryJson[];
    return json
      .map(category => Category.parse(category))
      .filter(category => category.category_hidden === '0');
  },
};
