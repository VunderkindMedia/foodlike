import {request, RequestMethods} from './request';
import {
  API_ORDERS_CREATE,
  API_ORDERS_GET,
  API_TRANSACTION_GET,
} from '../constants/api';
import {OrderRequest} from '../model/order_request';
import {privateReplacer} from '../helpers/json';
import {IOrderJson, Order} from '../model/order';
import {ITransactionJson, Transaction} from '../model/transaction';
import {Product} from '../model/product';

export const orders = {
  create: async (requestBody: OrderRequest): Promise<any> => {
    return await request.fetch(API_ORDERS_CREATE, {
      method: RequestMethods.POST,
      body: JSON.stringify(requestBody, privateReplacer),
      headers: {'Content-Type': 'application/json'},
    });
  },
  get: async (client_phone: string): Promise<Order[]> => {
    const response = await request.fetch(API_ORDERS_GET(client_phone), {
      method: RequestMethods.GET,
    });
    const jsonResponse = await response.json();
    const json = jsonResponse.data as IOrderJson[];
    return json.map(orderJson => Order.parse(orderJson));
  },
  getTransaction: async (
    id: number,
    products: Product[],
  ): Promise<Transaction> => {
    const response = await request.fetch(API_TRANSACTION_GET(id), {
      method: RequestMethods.GET,
    });
    const jsonResponse = await response.json();
    const json = jsonResponse.response[0] as ITransactionJson;
    return Transaction.parse(json, products);
  },
};
