import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import PDColors from '../../constants/colors/colors';
import PDImages from '../../constants/images/images';
import {Transaction} from '../../model/transaction';
import {DELIVERY_PRICE} from '../../constants/payments';

interface IPDInOrderTotalProps {
  transaction: Transaction;
}

export const PDOrderTotal = (props: IPDInOrderTotalProps) => {
  const {transaction} = props;
  return (
    <View
      style={[
        styles.order_total__footer_wrapper,
        styles.order_total__isFinish,
      ]}>
      <View style={styles.order_total__row_wrapper}>
        <Text style={styles.order_total__discount_title}>Товары</Text>
        <Text style={styles.order_total__delivery_value}>
          {(+transaction.sum / 100).toFixed(2).replace('.', ',')} ₽
        </Text>
      </View>

      {transaction.payed_bonus.length > 0 && (
        <View style={styles.order_total__row_wrapper}>
          <Text style={styles.order_total__discount_title}>Бонусы</Text>
          <Text style={styles.order_total__discount_value}>
            {+transaction.payed_bonus > 0 ? '-' : ''}
            {(+transaction.payed_bonus / 100).toFixed(2).replace('.', ',')} ₽
          </Text>
        </View>
      )}
      <View style={styles.order_total__delivery_wrapper}>
        <Text style={styles.order_total__discount_title}>
          Стоимость доставки
        </Text>
        <Text style={styles.order_total__delivery_value}>
          {DELIVERY_PRICE} ₽
        </Text>
      </View>
      <View style={styles.order_total__row_wrapper}>
        <Text
          style={[
            styles.order_total__footer_amount_title,
            styles.order_total__footer_amount_title_big,
          ]}>
          Итого
        </Text>
        <Text
          style={[
            styles.order_total__footer_amount_title,
            styles.order_total__footer_amount_title_big,
          ]}>
          {(+transaction.payed_sum / 100).toFixed(2).replace('.', ',')} ₽
        </Text>
      </View>
      <View style={styles.order_total__bonuses_wrapper}>
        <Image
          style={styles.order_total__bonuses__icon}
          source={PDImages.money}
        />
        <Text style={styles.order_total__bonuses__text}>{`Будет начислено ${(
          (+transaction.payed_sum * (+transaction.bonus / 100)) /
          100
        )
          .toFixed(2)
          .replace('.', ',')} бонусов`}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  order_total__title: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 24,
    color: PDColors.textBlack,
    marginBottom: 24,
  },
  order_total__footer_wrapper: {
    paddingVertical: 16,
  },
  order_total__row_wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  order_total__discount_title: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 16,
    color: PDColors.textDark,
  },
  order_total__discount_value: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 16,
    color: PDColors.primary,
  },
  order_total__delivery_wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 24,
  },
  order_total__delivery_value: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 16,
    color: PDColors.textDark,
  },
  order_total__footer_amount_title: {
    fontFamily: 'SFProDisplay',
    fontWeight: '700',
    fontSize: 16,
    color: PDColors.textBlack,
  },
  order_total__bonuses_wrapper: {
    marginTop: 32,
    marginBottom: 32,
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderBottomColor: PDColors.buttonLightGray,
    borderTopColor: PDColors.buttonLightGray,
  },
  order_total__bonuses__icon: {
    marginRight: 8,
  },
  order_total__bonuses__text: {
    fontSize: 16,
    fontWeight: '400',
    color: PDColors.textBlack,
  },
  order_total__isFinish: {
    paddingHorizontal: 24,
  },
  order_total__footer_amount_title_big: {
    fontSize: 24,
  },
});
