import React, {useCallback} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Order} from '../../model/order';
import {orderStatusToText} from '../../helpers/orders';
import PDColors from '../../constants/colors/colors';
import {PDIcons} from '../icons/custom-icons';
import {Routes} from '../../navigation/constants/routes';
export interface IPDOrdersItemProps {
  item: Order;
  navigation: any;
}
export const PDOrdersItem = (props: IPDOrdersItemProps) => {
  const {item, navigation} = props;
  const {
    created_at,
    total_sum,
    delivery_price,
    incoming_order_id,
    status,
    text_status,
  } = item;
  const order_status = orderStatusToText(status, text_status);
  const pressHandler = useCallback(() => {
    if (status === 1) {
      navigation.push(Routes.SCREEN_ORDER, {order: item});
    }
  }, [item, navigation, status]);
  return (
    <TouchableOpacity
      style={styles.container}
      onPress={pressHandler}
      activeOpacity={1}>
      <View style={styles.wrapper}>
        <Text style={styles.date}>{created_at}</Text>
        <Text style={styles.title}>
          Заказ №{incoming_order_id} {order_status}
        </Text>
        <Text style={styles.total}>
          {(total_sum + +delivery_price) / 100} ₽
        </Text>
      </View>
      {status === 1 && (
        <View style={styles.chevron_wrapper}>
          <PDIcons size={16} name={'right_chevron'} color={PDColors.textDark} />
        </View>
      )}
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderColor: PDColors.buttonLightGray,
    paddingVertical: 16,
    flexDirection: 'row',
  },
  wrapper: {
    flex: 1,
  },
  total: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 14,
    color: PDColors.textDark,
  },
  title: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 16,
    color: PDColors.textBlack,
    marginBottom: 8,
  },
  date: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 14,
    color: PDColors.textDark,
    marginBottom: 8,
  },
  chevron_wrapper: {
    justifyContent: 'center',
  },
});
