import React from 'react';
import {View, Text} from 'react-native';
import Toast, {BaseToast, BaseToastProps} from 'react-native-toast-message';
// @ts-ignore
import {icons} from 'react-native-toast-message/src/assets';
import PDColors from '../../constants/colors/colors';
interface IToastProps extends BaseToastProps {
  props: {[name: string]: any};
}
const toastConfig = {
  my_type: ({text1}: IToastProps) => (
    <View style={{height: 60, width: '100%', backgroundColor: 'tomato'}}>
      <Text>{text1}</Text>
    </View>
  ),
  success: ({text1, props, ...rest}: IToastProps) => (
    <BaseToast
      {...rest}
      style={{borderLeftColor: PDColors.primary}}
      contentContainerStyle={{padding: 15}}
      text1Style={{
        fontSize: 16,
        fontWeight: '400',
        color: '#000',
      }}
      leadingIcon={icons.success}
      text1={text1}
      text2={props.uuid}
    />
  ),
  error: ({text1, props, ...rest}: IToastProps) => (
    <BaseToast
      {...rest}
      style={{borderLeftColor: 'red'}}
      contentContainerStyle={{padding: 15}}
      text1Style={{
        fontSize: 16,
        fontWeight: '400',
        color: '#000',
      }}
      leadingIcon={icons.error}
      text1={text1}
      text2={props.uuid}
    />
  ),
};

const PDToast = () => {
  return <Toast config={toastConfig} ref={ref => Toast.setRef(ref)} />;
};

export default PDToast;
