import React from 'react';

import {View, Text, ViewStyle, StyleSheet, TextStyle} from 'react-native';
import PDColors from '../../constants/colors/colors';
interface IPDStickyLabel {
  value: string;
  style?: ViewStyle;
  titleStyle?: TextStyle;
}
export const PDStickyLabel = (props: IPDStickyLabel) => {
  const {value, style} = props;
  return (
    <View style={[styles.sticky_label__wrapper, style]}>
      <Text style={styles.sticky_label__title}>{value}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  sticky_label__wrapper: {
    zIndex: 100,
    opacity: 0.8,
    borderRadius: 4,
    paddingVertical: 4,
    paddingHorizontal: 5,
    alignSelf: 'flex-start',
  },
  sticky_label__title: {
    fontFamily: 'SFProDisplay',
    fontWeight: '400',
    fontSize: 14,
    color: PDColors.textWhite,
  },
});
