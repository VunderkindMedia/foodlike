import React from 'react';
import {
  StyleProp,
  TextStyle,
  ViewStyle,
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import PDColors from '../../constants/colors/colors';
import RNBounceable from '@freakycoder/react-native-bounceable';

interface IDefaultPDButton {
  title: string;
  suffixValue?: string | number;
  suffixValueContainerStyle?: StyleProp<ViewStyle>;
  suffixValueStyle?: StyleProp<TextStyle>;
  crossedSuffixValue?: string | number;
  crossedSuffixValueStyle?: StyleProp<TextStyle>;
  crossedSuffixValueContainerStyle?: StyleProp<TextStyle>;
  onPress: () => void;
  buttonStyle?: StyleProp<ViewStyle>;
  buttonContainerStyle?: StyleProp<ViewStyle>;
  backgroundColor?: string;
  titleColor?: string;
  titleStyle?: StyleProp<TextStyle>;
  prefixIcon?: JSX.Element;
  disabled?: boolean;
  underButtonText?: string;
  titleFontSize?: number;
}

export const DefaultPDButton = (props: IDefaultPDButton) => {
  const {
    title,
    suffixValue,
    suffixValueStyle,
    suffixValueContainerStyle,
    crossedSuffixValue,
    crossedSuffixValueStyle,
    crossedSuffixValueContainerStyle,
    onPress,
    buttonStyle,
    buttonContainerStyle,
    backgroundColor = PDColors.primary,
    titleColor = PDColors.textWhite,
    prefixIcon,
    disabled,
    underButtonText,
    titleFontSize,
    titleStyle,
  } = props;
  return (
    <View style={buttonContainerStyle}>
      {underButtonText && (
        <Text style={styles.under_button_text}>{underButtonText}</Text>
      )}
      <TouchableOpacity
        activeOpacity={0.8}
        disabled={disabled}
        onPress={onPress}
        style={[
          styles.button,
          {
            backgroundColor: disabled
              ? PDColors.borderDefault
              : backgroundColor,
          },
          buttonStyle,
        ]}>
        {prefixIcon}
        <Text
          style={[
            styles.title_style,
            {color: titleColor, fontSize: titleFontSize || 20.0},
            titleStyle,
          ]}>
          {title}
        </Text>
        {(crossedSuffixValue || suffixValue) && (
          <View style={styles.suffix_wrapper}>
            {crossedSuffixValue && (
              <View
                style={[
                  styles.crossed_suffix_value_container_style,
                  {
                    backgroundColor: disabled
                      ? PDColors.disabledBackground
                      : PDColors.primary,
                  },
                  crossedSuffixValueContainerStyle,
                ]}>
                <Text
                  style={[
                    styles.crossed_suffix_value_style,
                    crossedSuffixValueStyle,
                  ]}>
                  {crossedSuffixValue} ₽
                </Text>
              </View>
            )}
            {suffixValue && (
              <View
                style={[
                  styles.suffix_value_container_style,
                  {
                    backgroundColor: disabled
                      ? PDColors.normalGrayText
                      : PDColors.primary,
                  },
                  suffixValueContainerStyle,
                ]}>
                <Text style={[styles.suffix_value_style, suffixValueStyle]}>
                  {suffixValue} ₽
                </Text>
              </View>
            )}
          </View>
        )}
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    height: 67,
    borderRadius: 19,
    paddingHorizontal: 24,
    position: 'relative',
  },
  under_button_text: {
    color: PDColors.normalGrayText,
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 14,
    textAlign: 'center',
    marginBottom: 15,
  },
  title_style: {
    fontFamily: 'SFProDisplay-Regular',
    textAlign: 'center',
    fontSize: 20,
  },
  suffix_value_container_style: {
    position: 'relative',
    borderRadius: 4,
    paddingVertical: 2,
    paddingHorizontal: 5,
  },
  suffix_value_style: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 16,

    color: PDColors.textWhite,
  },
  crossed_suffix_value_container_style: {
    position: 'relative',
    borderRadius: 4,
    paddingVertical: 2,
    paddingHorizontal: 5,
  },
  crossed_suffix_value_style: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 16,
    color: PDColors.textWhite,
  },
  suffix_wrapper: {
    flexDirection: 'row',
  },
});
