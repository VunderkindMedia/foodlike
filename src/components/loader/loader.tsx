import {StyleSheet, ViewStyle} from 'react-native';
import React from 'react';
import * as Animatable from 'react-native-animatable';
import PDImages from '../../constants/images/images';
import PDColors from '../../constants/colors/colors';

interface IPDLoaderProps {
  fullscreen?: boolean;
  style?: ViewStyle;
}
export const PDLoader = (props: IPDLoaderProps) => {
  const {fullscreen = false, style} = props;
  const heartBeet = {
    0: {
      opacity: 1,
      scale: 1,
    },
    0.2: {
      opacity: 0.8,
      scale: 0.8,
    },
    0.5: {
      opacity: 0.9,
      scale: 0.9,
    },
    0.7: {
      opacity: 1,
      scale: 0.8,
    },
    1: {
      opacity: 1,
      scale: 1,
    },
  };
  return (
    <Animatable.View
      animation={'fadeIn'}
      style={[
        styles.loader_container,
        fullscreen && styles.loader_container_fullscreen,
        style,
      ]}>
      <Animatable.Image
        easing="ease-in-out"
        iterationCount="infinite"
        animation={heartBeet}
        duration={1000}
        style={styles.loader_image}
        source={PDImages.logo}
        useNativeDriver
      />
    </Animatable.View>
  );
};

const styles = StyleSheet.create({
  loader_container: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
  loader_container_fullscreen: {
    flex: 1,
    backgroundColor: PDColors.whiteBackground,
    zIndex: 99,
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  loader_image: {
    width: 80,
    height: 80,
  },
});
