import React, {forwardRef, useImperativeHandle, useRef, useState} from 'react';
import {
  Animated,
  NativeScrollEvent,
  NativeSyntheticEvent,
  StyleSheet,
  View,
} from 'react-native';
interface IStickyHeaderProps {
  children: React.ReactNode;
  maxHeight: number;
}

export interface IOnScrollHandler {
  onScroll: (event: NativeSyntheticEvent<NativeScrollEvent>) => void;
  height: number;
}

export const StickyHeader = forwardRef<IOnScrollHandler, IStickyHeaderProps>(
  (props, ref) => {
    const {children, maxHeight} = props;
    const curY = useRef(new Animated.Value(0)).current;
    const [height, setHeight] = useState(maxHeight);

    const headerDistance = Animated.diffClamp(curY, 0, maxHeight).interpolate({
      inputRange: [0, 1],
      outputRange: [0, -1],
    });

    useImperativeHandle(ref, () => ({
      onScroll: Animated.event([{nativeEvent: {contentOffset: {y: curY}}}], {
        useNativeDriver: true,
      }),

      height: height,
    }));
    return (
      <Animated.View
        style={{
          transform: [
            {
              translateY: headerDistance,
            },
          ],
          position: 'absolute',
          top: 0,
        }}
        onLayout={({nativeEvent}) => setHeight(nativeEvent.layout.height)}>
        {children}
      </Animated.View>
    );
  },
);
