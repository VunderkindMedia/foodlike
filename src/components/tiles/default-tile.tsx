import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  StyleProp,
  ViewStyle,
  TextStyle,
  TouchableOpacity,
} from 'react-native';
import {PDIcons} from '../icons/custom-icons';
import PDColors from '../../constants/colors/colors';

interface PDTileDefaultProps {
  text: string;
  icon_name?: string;
  onPress: () => void;
  style?: StyleProp<ViewStyle>;
  titleStyle?: StyleProp<TextStyle>;
}

export const PDTileDefault = (props: PDTileDefaultProps) => {
  const {text, icon_name, onPress, style, titleStyle} = props;
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={[styles.tile__container, style]}
      onPress={onPress}>
      <View style={styles.tile__wrapper}>
        {icon_name && (
          <PDIcons
            style={styles.tile__icon}
            color={PDColors.textBlack}
            name={icon_name}
            size={24}
          />
        )}
        <Text style={[styles.tile__text, titleStyle]}>{text}</Text>
      </View>
      <View>
        <PDIcons color={PDColors.textDark} name={'right_chevron'} size={16} />
      </View>
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  tile__container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 20,
    borderColor: PDColors.buttonLightGray,
  },
  tile__wrapper: {
    flexDirection: 'row',
  },
  tile__text: {
    color: PDColors.textBlack,
    fontSize: 20,
  },
  tile__icon: {
    marginRight: 16,
  },
});
