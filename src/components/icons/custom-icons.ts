import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from '../../constants/icons/selection.json';
export const PDIcons = createIconSetFromIcoMoon(icoMoonConfig);
