// @ts-nocheck
import React, {Component} from 'react';
import {
  View,
  Image,
  ActivityIndicator,
  TouchableOpacity,
  Dimensions,
  StyleSheet,
  StyleProp,
  ViewStyle,
} from 'react-native';

import Carousel, {CarouselProps, Pagination} from 'react-native-snap-carousel'; //Thank From distributer(s) of this lib

// -------------------Props---------------------
// images
// onCurrentImagePressed
// sliderBoxHeight
// parentWidth
// dotColor
// inactiveDotColor
// dotStyle
// paginationBoxVerticalPadding
// circleLoop
// autoplay
// ImageComponent
// paginationBoxStyle
// resizeMethod
// resizeMode
// ImageComponentStyle,
// imageLoadingColor = "#E91E63"

const width = Dimensions.get('window').width;
interface ISliderProps {
  onCurrentImagePressed: (image: number) => void;
  currentImageEmitter?: (image: number) => void;
  autoplay: boolean;
  circleLoop: boolean;
  paginationBoxVerticalPadding: number;
  images: string[];
  sliderBoxHeight?: number;
  paginationDot: boolean;
  ImageComponentStyle: StyleProp<ViewStyle>;
}
export class Slider extends Component<CarouselProps<any> | ISliderProps> {
  constructor(props: CarouselProps<any> | ISliderProps) {
    super(props);
    this.state = {
      currentImage: 0,
      loading: [],
    };
    this.onCurrentImagePressedHandler =
      this.onCurrentImagePressedHandler.bind(this);
    this.onSnap = this.onSnap.bind(this);
  }
  // componentDidMount() {
  //   let a = [...Array(this.props.images.length).keys()].map(i => false);
  // }
  onCurrentImagePressedHandler() {
    // @ts-ignore
    if (this.props.onCurrentImagePressed) {
      // @ts-ignore
      this.props.onCurrentImagePressed(this.state.currentImage);
    }
  }
  // @ts-ignore
  onSnap(index) {
    // @ts-ignore
    const {currentImageEmitter} = this.props;
    this.setState({currentImage: index}, () => {
      if (currentImageEmitter) {
        // @ts-ignore
        currentImageEmitter(this.state.currentImage);
      }
    });
  }
  // @ts-ignore
  _renderItem({item, index}) {
    const {
      ImageComponentStyle = {},
      sliderBoxHeight,
      disableOnPress,
      resizeMethod,
      resizeMode,
      imageLoadingColor = '#E91E63',
    } = this.props;
    const dimensions = Dimensions.get('window');
    const imageHeight = (dimensions.width * 3) / 10;
    const imageWidth = dimensions.width - 24;
    return (
      <View style={styles.container}>
        <TouchableOpacity
          key={index}
          onPress={() => !disableOnPress && this.onCurrentImagePressedHandler()}
          activeOpacity={0.9}>
          <Image
            resizeMethod={resizeMethod}
            resizeMode={resizeMode}
            style={[
              // eslint-disable-next-line react-native/no-inline-styles
              {
                width: imageWidth,
                height: sliderBoxHeight || imageHeight,
                alignSelf: 'center',
              },
              ImageComponentStyle,
            ]}
            source={typeof item === 'string' ? {uri: item} : item}
            onLoad={() => {}}
            onLoadStart={() => {}}
            onLoadEnd={() => {
              let t = this.state.loading;
              t[index] = true;
              this.setState({loading: t});
            }}
            {...this.props}
          />
        </TouchableOpacity>
        {!this.state.loading[index] && (
          <ActivityIndicator
            size="large"
            color={imageLoadingColor}
            style={styles.progress}
          />
        )}
      </View>
    );
  }

  get pagination() {
    const {currentImage} = this.state;
    const {
      images,
      dotStyle,
      dotColor,
      inactiveDotColor,
      paginationBoxStyle,
      paginationBoxVerticalPadding,
    } = this.props;
    return (
      <Pagination
        borderRadius={2}
        dotsLength={images.length}
        activeDotIndex={currentImage}
        dotStyle={dotStyle || styles.dotStyle}
        dotColor={dotColor || colors.dotColors}
        inactiveDotColor={inactiveDotColor || colors.white}
        inactiveDotScale={0.8}
        carouselRef={this._ref}
        inactiveDotOpacity={0.8}
        tappableDots={!!this._ref}
        containerStyle={[
          styles.paginationBoxStyle,
          paginationBoxVerticalPadding
            ? {paddingVertical: paginationBoxVerticalPadding}
            : {},
          paginationBoxStyle ? paginationBoxStyle : {},
        ]}
        {...this.props}
      />
    );
  }

  render() {
    const {
      images,
      circleLoop,
      autoplay,
      parentWidth,
      loopClonesPerSide,
      paginationDot = true,
    } = this.props;
    return (
      <View>
        <Carousel
          layout={'default'}
          data={images}
          ref={c => (this._ref = c)}
          loop={circleLoop || false}
          enableSnap={true}
          autoplay={autoplay || false}
          itemWidth={parentWidth || width}
          sliderWidth={parentWidth || width}
          loopClonesPerSide={loopClonesPerSide || 5}
          renderItem={item => this._renderItem(item)}
          onSnapToItem={index => this.onSnap(index)}
          {...this.props}
        />
        {images.length > 1 && paginationDot && this.pagination}
      </View>
    );
  }
}

const colors = {
  dotColors: '#BDBDBD',
  white: '#FFFFFF',
};

Slider.defaultProps = {
  ImageComponent: Image,
};

const styles = StyleSheet.create({
  paginationBoxStyle: {
    position: 'absolute',
    bottom: 0,
    padding: 0,
    alignItems: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
  },
  dotStyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 0,
    padding: 0,
    margin: 0,
    backgroundColor: 'rgba(128, 128, 128, 0.92)',
  },
  container: {
    position: 'relative',
    justifyContent: 'center',
  },
  progress: {
    position: 'absolute',
    alignSelf: 'center',
  },
});
