import React, {useCallback} from 'react';
import BouncyCheckbox from 'react-native-bouncy-checkbox';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native';
import PDImages from '../../constants/images/images';
import PDColors from '../../constants/colors/colors';
import {PDIcons} from '../icons/custom-icons';

interface IPDCheckboxProps {
  title?: string | JSX.Element;
  onChange: (key: number | string) => void;
  keyId: number | string;
  checked: boolean;
  prefixIconName?: string;
  checkbox?: boolean;
  reverse?: boolean;
  style?: ViewStyle;
}
export const PDCheckbox = (props: IPDCheckboxProps) => {
  const {
    title,
    onChange,
    keyId,
    checked,
    prefixIconName,
    checkbox,
    reverse,
    style,
  } = props;
  const changeHandler = useCallback(() => onChange(keyId), [keyId, onChange]);
  return (
    <TouchableOpacity
      activeOpacity={0.7}
      onPress={changeHandler}
      style={[styles.container, style]}>
      {title && !reverse && (
        <View style={styles.titleWrapper}>
          {prefixIconName && (
            <PDIcons style={styles.image} size={24} name={prefixIconName} />
          )}
          <Text style={styles.textStyle}>{title}</Text>
        </View>
      )}
      <BouncyCheckbox
        disableBuiltInState
        disableText
        isChecked={checked}
        checkIconImageSource={
          checkbox ? (PDImages.checkbox_true as Image) : undefined
        }
        ImageComponent={checkbox ? Image : View}
        onPress={changeHandler}
        iconImageStyle={styles.iconImageStyle}
        iconStyle={[
          styles.icon,
          // eslint-disable-next-line react-native/no-inline-styles
          {
            width: checkbox ? 32 : 24,
            height: checkbox ? 32 : 24,
            borderColor: checked ? PDColors.primary : PDColors.buttonLightGray,
            borderWidth: checked ? (checkbox ? 0 : 8) : 1,
            borderRadius: checkbox ? 8 : 50,
            paddingHorizontal: 0,
            backgroundColor: checkbox
              ? checked
                ? PDColors.primary
                : PDColors.transparent
              : PDColors.transparent,
          },
          reverse && {
            marginRight: 15,
          },
          !reverse && {
            marginLeft: 15,
          },
        ]}
      />
      {title && reverse && (
        <View style={styles.titleWrapper}>
          {prefixIconName && (
            <PDIcons style={styles.image} size={24} name={prefixIconName} />
          )}
          <Text style={styles.textStyle}>{title}</Text>
        </View>
      )}
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  textStyle: {
    fontSize: 16,
    color: PDColors.textBlack,
    fontFamily: 'SFProDisplay-Regular',
  },
  icon: {
    backgroundColor: PDColors.transparent,
    width: 24,
    height: 24,
  },
  image: {
    width: 24,
    height: 24,
    marginRight: 16,
  },
  titleWrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  iconImageStyle: {width: 12, height: 12},
});
