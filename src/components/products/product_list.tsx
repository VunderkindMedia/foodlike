import {FlatList, RefreshControl, StyleSheet} from 'react-native';
import PDColors from '../../constants/colors/colors';
import {paginate} from '../../helpers/lists';
import React, {useCallback, useMemo, useState} from 'react';
import {useRequestStateIsSuccess} from '../../redux/request/selectors';
import {useProducts} from '../../redux/products/selectors';
import {useLoadProductsAndCats} from '../../hooks/products';
import {Product} from '../../model/product';
import {ProductFilters} from '../../model/product_filters';
import {
  useCartAdd,
  useCartDecrement,
  useCartIncrement,
  useCartRemove,
} from '../../hooks/cart';
import {useCartList} from '../../redux/cart/selectors';
import {PDProductsItem} from './products_item';
interface IPDProductListProps {
  filter?: ProductFilters;
  category_id?: number;
  navigation: any;
}
export const IPDProductList = (props: IPDProductListProps) => {
  const {filter, category_id, navigation} = props;
  const [page, setPage] = useState(1);
  const productsRequest = useRequestStateIsSuccess('categories');
  const products = useProducts();
  const loadProducts = useLoadProductsAndCats();
  const cartIncrement = useCartIncrement();
  const cartDecrement = useCartDecrement();
  const cartAdd = useCartAdd();
  const cartRemove = useCartRemove();
  const cartList = useCartList();
  const filteredProducts = useMemo(() => {
    let tempProducts: Product[] = products;
    if (category_id) {
      tempProducts = tempProducts.filter(
        product => product.categoryPosId === category_id,
      );
    }
    if (filter === ProductFilters.RECOMMENDED) {
      tempProducts = tempProducts.filter(product => product.popular);
    }
    if (filter === ProductFilters.ACTIONED) {
      tempProducts = tempProducts.filter(product => product.discountPrice);
    }
    return tempProducts;
  }, [category_id, filter, products]);

  const handleRefresh = useCallback(() => {
    if (
      filter === ProductFilters.RECOMMENDED ||
      filter === ProductFilters.ACTIONED
    ) {
      return;
    }
    setPage(1);
    loadProducts();
  }, [filter, loadProducts]);
  return (
    <FlatList
      columnWrapperStyle={styles.products__column_wrapper}
      scrollEnabled={!filter}
      refreshControl={
        <RefreshControl
          colors={[PDColors.primary, PDColors.primary]}
          refreshing={!productsRequest}
          onRefresh={handleRefresh}
          progressViewOffset={0}
        />
      }
      onEndReachedThreshold={0.05}
      onEndReached={() => {
        setPage(prev => prev + 1);
      }}
      contentContainerStyle={styles.products__flat_container}
      data={paginate(filteredProducts, 10, page)}
      renderItem={({item}) => (
        <PDProductsItem
          hideLabel={false}
          item={item}
          navigation={navigation}
          cartList={cartList}
          cartAdd={cartAdd}
          cartRemove={cartRemove}
          cartDecrement={cartDecrement}
          cartIncrement={cartIncrement}
        />
      )}
      numColumns={2}
      keyExtractor={item => item.product_id + item.name}
    />
  );
};

const styles = StyleSheet.create({
  products__flat_container: {
    marginTop: 10,
    paddingBottom: 40,
  },
  products__column_wrapper: {
    paddingHorizontal: 14,
    justifyContent: 'space-between',
  },
});
