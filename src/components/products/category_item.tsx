import {Category} from '../../model/category';
import React, {useMemo, useState} from 'react';
import {TouchableOpacity, View, Image, Text, StyleSheet} from 'react-native';
import PDColors from '../../constants/colors/colors';
import Config from 'react-native-config';
import {Routes} from '../../navigation/constants/routes';
import PDImages from '../../constants/images/images';
import {randomColorForCat} from '../../constants/categories';
interface IPDCategoryItem {
  item: Category;
  navigation: any;
  categories: Category[];
}

export const PDCategoryItem = (props: IPDCategoryItem) => {
  const {item, navigation, categories} = props;
  const [touched, setTouched] = useState(false);

  const color = useMemo(() => PDColors.pink as string, []);

  const navigateHandler = (id: number) => {
    if (!touched) {
      setTouched(true);
      if (categories.filter(cat => cat.parent_category === id).length > 0) {
        navigation.push(Routes.SCREEN_CATEGORIES, {
          parent_id: id,
        });
      } else {
        navigation.push(Routes.SCREEN_PRODUCTS, {
          title: item.category_name,
          category_id: item.category_id,
        });
      }
      setTimeout(() => {
        setTouched(false);
      }, 500);
    }
  };
  return (
    <TouchableOpacity
      onPress={() => navigateHandler(item.category_id)}
      activeOpacity={0.8}
      style={[
        styles.categories__container,
        {
          backgroundColor: color,
        },
      ]}>
      <Text numberOfLines={2} style={styles.categories__item_title}>
        {item.category_name}
      </Text>
      <View style={styles.categories__image_wrapper}>
        <Image
          style={styles.categories__item_image}
          source={
            item.category_photo_origin
              ? {uri: Config.POSTER_URL + item.category_photo_origin}
              : PDImages.no_photo
          }
        />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  categories__container: {
    height: 120,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    marginHorizontal: 8,
    marginBottom: 16,
    borderRadius: 16,
    overflow: 'hidden',
    elevation: 2,
    shadowColor: '#000000',
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.5,
  },
  categories__item_title: {
    fontFamily: 'SFProDisplay_Bold',
    fontSize: 24,
    textAlign: 'left',
    marginLeft: 8,
    color: PDColors.textWhite,
    flex: 1,
  },
  categories__image_wrapper: {
    height: 120,
    width: 150,
  },
  categories__item_image: {
    width: 150,
    resizeMode: 'cover',
    height: 120,
  },
});
