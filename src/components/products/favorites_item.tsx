import React, {useCallback} from 'react';
import {View, Image, TouchableOpacity, Text, StyleSheet} from 'react-native';
import {Product} from '../../model/product';
import PDImages from '../../constants/images/images';
import PDColors from '../../constants/colors/colors';
import {PDIcons} from '../icons/custom-icons';

interface IPDCartItemProps {
  item: Product;
  onPress: (item: Product) => void;
  inOrder: boolean;
}

export const FavoritesItem = (props: IPDCartItemProps) => {
  const {item, onPress, inOrder = false} = props;
  const pressHandler = useCallback(
    () => !inOrder && onPress(item),
    [inOrder, item, onPress],
  );

  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={pressHandler}
      style={styles.cart__item}>
      <View style={styles.cart__item_wrapper}>
        <View>
          <Image
            style={styles.cart__item_image}
            source={item.photo ? {uri: item.photo} : PDImages.no_photo}
          />
        </View>
        <View style={styles.cart__item_center}>
          <Text
            style={styles.cart__item_title}
            numberOfLines={2}
            ellipsizeMode="tail">
            {item.name}
          </Text>
          <Text style={styles.product__weight}>
            {inOrder ? `${item.cartCount} шт` : item.seoDescription}
          </Text>
        </View>
        <View style={styles.cart__item_right_wrapper}>
          <View style={styles.cart__item_price_wrapper}>
            {item.discountPrice && (
              <Text style={styles.cart__item_price_old}>
                {item.price * item.cartCount} ₽
              </Text>
            )}
            <View style={styles.cart__item_price__row_wrapper}>
              <Text style={styles.cart__item_price}>
                {item.discountPrice ? item.discountPrice : item.price} ₽
              </Text>
              {!inOrder && (
                <PDIcons style={styles.chevron} name={'right_chevron'} />
              )}
            </View>
          </View>
        </View>
      </View>
      <View style={styles.cart__item_divider} />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  cart__item: {
    flex: 1,
  },
  cart__item_wrapper: {
    flexDirection: 'row',
    paddingHorizontal: 25,
    paddingVertical: 24,
  },
  cart__item_image: {
    width: 88,
    height: 88,
    resizeMode: 'cover',
    marginRight: 16,
    borderRadius: 4,
  },
  cart__item_center: {
    flex: 1,
    justifyContent: 'center',
  },
  cart__item_title: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 16,
    marginBottom: 5,
    color: PDColors.textBlack,
  },
  product__weight: {
    fontFamily: 'SFProDisplay',
    fontSize: 16,
    color: PDColors.textDark,
    marginBottom: 12,
  },
  cart__item_btns_wrapper: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
  },
  cart__item_btn: {
    width: 32,
    height: 32,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: PDColors.buttonLightGray,
    borderRadius: 8,
  },
  cart__item_counter_wrapper: {
    width: 34,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
  },
  cart__item_right_wrapper: {
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  cart__item_price_wrapper: {
    justifyContent: 'center',
    height: 45,
  },
  cart__item_price_old: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 14,
    color: PDColors.normalGrayText,
    textDecorationLine: 'line-through',
  },
  cart__item_counter: {
    fontFamily: 'SFProDisplay-Medium',
    fontSize: 16,
    color: PDColors.textBlack,
    textAlign: 'center',
  },
  cart__item_price: {
    fontFamily: 'SFProDisplay-Bold',
    fontSize: 16,
    color: PDColors.textBlack,
  },
  cart__item_divider: {
    flex: 1,
    borderBottomWidth: 1,
    marginHorizontal: 25,
    borderColor: PDColors.borderDefault,
  },
  cart__item_close: {
    paddingHorizontal: 12,
    paddingVertical: 6,
  },
  cart__item_price__row_wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  chevron: {
    marginLeft: 8,
  },
});
