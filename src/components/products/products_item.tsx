import React, {useCallback, useMemo} from 'react';
import {
  Image,
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
} from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {Product} from '../../model/product';
import {Routes} from '../../navigation/constants/routes';
import PDImages from '../../constants/images/images';
import PDColors from '../../constants/colors/colors';
import {PDStickyLabel} from '../sticky_label/sticky_label';
import RNBounceable from '@freakycoder/react-native-bounceable';
interface IPDProductsItem {
  item: Product;
  navigation: any;
  cartList: Product[];
  cartAdd: (product: Product) => void;
  cartRemove: (product: Product) => void;
  cartIncrement: (product: Product) => void;
  cartDecrement: (product: Product) => void;
  hideLabel: boolean;
}
export const PDProductsItem = (props: IPDProductsItem) => {
  const {
    item,
    navigation,
    cartList,
    cartAdd,
    cartRemove,
    cartIncrement,
    cartDecrement,
    hideLabel,
  } = props;

  const cartItem = useMemo(
    () => cartList.find(product => product.pos_id === item.pos_id),
    [cartList, item.pos_id],
  );

  const goToProduct = useCallback(() => {
    navigation.navigate(Routes.SCREEN_PRODUCT, {
      item: item,
    });
  }, [navigation, item]);

  const decrementHandler = useCallback(() => {
    if (!cartItem || !cartItem.cartCount) {
      return;
    }
    if (cartItem.cartCount > 1) {
      cartDecrement(item);
    } else {
      cartRemove(item);
    }
  }, [cartDecrement, cartRemove, item, cartItem]);

  const incrementHandler = useCallback(() => {
    if (!cartItem) {
      cartAdd(item);
      return;
    }
    cartIncrement(item);
  }, [cartAdd, cartIncrement, cartItem, item]);

  const isInCart = cartItem && cartItem.cartCount > 0;

  const itemAmount = cartItem ? cartItem.cartCount * item.price : item.price;
  const itemDiscountAmount = cartItem
    ? cartItem.cartCount * item.discountPrice!
    : item.discountPrice;
  return (
    <TouchableOpacity
      style={styles.products__container}
      onPress={goToProduct}
      activeOpacity={1}>
      {item.photo ? (
        <Image style={styles.products__item_image} source={{uri: item.photo}} />
      ) : (
        <Image
          style={[styles.products__item_image, styles.no_photo]}
          source={PDImages.no_photo}
        />
      )}

      <View style={styles.products__item_image_overlay_discount}>
        {item.discountPrice && (
          <PDStickyLabel
            value={'TEST'}
            style={styles.products__discount_label}
          />
        )}
        {item.popular && !hideLabel && (
          <PDStickyLabel
            value={'Новинка'}
            style={styles.products__popular_label}
          />
        )}
      </View>
      {isInCart && (
        <View style={styles.products__item_image_overlay}>
          <Text style={styles.products__item_image_overlay_counter}>
            {cartItem?.cartCount}
          </Text>
        </View>
      )}
      <Text
        style={styles.products__item_title}
        numberOfLines={2}
        ellipsizeMode="tail">
        {item.name}
      </Text>
      <View style={styles.products__item_bottom_wrapper}>
        {isInCart && (
          <RNBounceable
            style={styles.products__item_plus_btn}
            onPress={decrementHandler}>
            <AntDesign name="minus" size={24} color={PDColors.primary} />
            {/*TODO: Заменить на нашу иконку*/}
          </RNBounceable>
        )}
        <View>
          <Text
            style={[
              styles.products__item_old_price,
              !item.discountPrice ? styles.transparent : null,
            ]}>
            {itemAmount}
          </Text>
          <Text
            style={[
              styles.products__item_price,
              item.discountPrice ? {color: PDColors.primary} : null,
            ]}>
            {item.discountPrice ? itemDiscountAmount : itemAmount} ₽
          </Text>
        </View>
        <RNBounceable
          style={[
            styles.products__item_plus_btn,
            isInCart ? {backgroundColor: PDColors.primary} : null,
          ]}
          onPress={incrementHandler}>
          <AntDesign
            name="plus"
            size={24}
            color={isInCart ? PDColors.whiteBackground : PDColors.primary}
          />
        </RNBounceable>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  products__container: {
    flexBasis: Dimensions.get('window').width / 2 - 20,
    marginVertical: 7.5,
    borderRadius: 16,
    borderColor: PDColors.borderDefault,
    backgroundColor: PDColors.whiteBackground,
    overflow: 'hidden',
    elevation: 2,
    shadowColor: '#000000',
    shadowOffset: {width: 0, height: 2},
    shadowRadius: 10,
    shadowOpacity: 0.5,
  },
  products__item_title: {
    fontFamily: 'SFProDisplay_Normal',
    color: PDColors.textBlack,
    fontSize: 16,
    marginTop: 8,
    marginHorizontal: 12,
    marginBottom: 27,
    flex: 1,
  },
  products__item_description: {
    fontFamily: 'SFProDisplay_Medium',
    fontSize: 14,
    marginVertical: 5,
    marginHorizontal: 12,
    color: PDColors.normalGrayText,
  },
  products__item_price: {
    fontFamily: 'SFProDisplay_Bold',
    fontWeight: '700',
    marginHorizontal: 12,
    color: PDColors.textBlack,
    fontSize: 16,
    marginBottom: 22,
  },
  products__item_old_price: {
    fontFamily: 'SFProDisplay',
    fontWeight: '600',
    marginHorizontal: 8,
    marginBottom: 4,
    color: PDColors.textDark,
    fontSize: 14,
    textDecorationLine: 'line-through',
  },
  products__item_bottom_wrapper: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  products__item_plus_btn: {
    height: 36,
    width: 36,
    borderRadius: 8,
    backgroundColor: PDColors.whiteBackground,
    borderColor: PDColors.primary,
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 12,
  },
  products__item_image: {
    resizeMode: 'cover',
    width: Dimensions.get('window').width / 2 - 20,
    zIndex: 10,
    height: Dimensions.get('window').width / 2 - 20,
    alignSelf: 'center',
  },
  products__item_image_overlay_discount: {
    height: Dimensions.get('window').width / 2 - 20,
    width: Dimensions.get('window').width / 2 - 20,
    position: 'absolute',
    top: 0,
    zIndex: 99,
    justifyContent: 'flex-end',
    padding: 8,
  },
  products__item_image_overlay: {
    height: Dimensions.get('window').width / 2 - 20,
    width: Dimensions.get('window').width / 2 - 20,
    position: 'absolute',
    top: 0,
    backgroundColor: '#00000066',
    zIndex: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  no_photo: {
    resizeMode: 'center',
  },
  products__item_image_overlay_counter: {
    color: PDColors.whiteBackground,
    fontFamily: 'SFProDisplay_Bold',
    fontSize: 48,
  },
  transparent: {
    opacity: 0,
  },
  products__discount_label: {
    backgroundColor: PDColors.primary,
  },
  products__popular_label: {
    backgroundColor: PDColors.pink,
    opacity: 0.8,
    position: 'absolute',
    top: 7.5,
    right: 7.5,
  },
});
