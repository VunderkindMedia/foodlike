import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, StatusBar} from 'react-native';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import PDColors from '../../constants/colors/colors';
import {ERROR_BUTTON_TITLE} from '../../constants/strings';
import {DefaultPDButton} from '../buttons/default-button';
import {PDLoader} from '../loader/loader';
import SplashScreen from 'react-native-splash-screen';
interface IErrorViewProps {
  clickHandler: () => void;
}
export const ErrorView = (props: IErrorViewProps) => {
  const {clickHandler} = props;
  const [loading, setLoading] = useState(false);
  useEffect(() => {
    SplashScreen.hide();
    return () => {
      setLoading(false);
    };
  }, []);
  if (loading) {
    return <PDLoader fullscreen />;
  }
  return (
    <View style={styles.container}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor={PDColors.transparent}
        translucent
      />
      <View style={styles.body}>
        <MaterialIcons
          name={'error'}
          size={164}
          color={PDColors.backgroundLightPrimary}
        />
        <Text style={styles.title}>Произошла ошибка</Text>
        <Text style={styles.subtitle}>
          Возможно проблемы с сетью или на сервере ведутся технические работы
        </Text>
      </View>

      <DefaultPDButton
        backgroundColor={PDColors.primary}
        title={ERROR_BUTTON_TITLE}
        buttonStyle={styles.submit_button}
        onPress={async () => {
          setLoading(true);
          await clickHandler();
          setLoading(false);
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: PDColors.whiteBackground,
    justifyContent: 'flex-end',
    paddingHorizontal: 25,
    paddingVertical: 60,
  },
  image: {
    width: 200,
    resizeMode: 'contain',
    marginLeft: 0,
    marginTop: '50%',
  },
  title: {
    fontFamily: 'SFProDisplay-Semibold',
    color: PDColors.textBlack,
    fontSize: 28,
  },
  subtitle: {
    fontSize: 16,
    textAlign: 'center',
    color: PDColors.textDark,
    marginHorizontal: 40,
    marginVertical: 20,
  },
  imageBack: {flex: 1, alignItems: 'center'},
  btns: {
    marginHorizontal: 20,
  },
  mainBtn: {
    marginBottom: 60,
  },
  body: {
    flex: 0.7,
    alignItems: 'center',
  },
  submit_button: {
    justifyContent: 'center',
  },
});
