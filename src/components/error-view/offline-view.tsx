import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  Image,
  Dimensions,
} from 'react-native';
import PDColors from '../../constants/colors/colors';
import {OFFLINE_BUTTON_TITLE} from '../../constants/strings';
import {DefaultPDButton} from '../buttons/default-button';
import {PDLoader} from '../loader/loader';
import PDImages from '../../constants/images/images';
import SplashScreen from 'react-native-splash-screen';
interface IOfflineViewProps {
  clickHandler: () => void;
  text: string;
}
export const OfflineView = (props: IOfflineViewProps) => {
  const {clickHandler, text} = props;
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    SplashScreen.hide();
    return () => {
      setLoading(false);
    };
  }, []);
  if (loading) {
    return <PDLoader fullscreen />;
  }
  return (
    <View style={styles.container}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor={PDColors.transparent}
        translucent
      />
      <View style={styles.body}>
        <Image source={PDImages.offline_logo} style={styles.image} />
        <Text style={styles.title}>Приложение в данный момент недоступно</Text>
        <Text style={styles.subtitle}>
          {text ??
            'Возможно проблемы с сетью или на сервере ведутся технические работы'}
        </Text>
      </View>

      <DefaultPDButton
        backgroundColor={PDColors.primary}
        title={OFFLINE_BUTTON_TITLE}
        buttonStyle={styles.submit_button}
        titleColor={PDColors.primary}
        titleStyle={styles.submit_button_title}
        onPress={async () => {
          setLoading(true);
          await clickHandler();
          setLoading(false);
        }}
      />
    </View>
  );
};
const dimensions = Dimensions.get('window');
const imageHeight = Math.round((dimensions.width * 129) / 164);
const imageWidth = dimensions.width - 48;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: PDColors.whiteBackground,
    justifyContent: 'center',
    paddingHorizontal: 24,
  },
  image: {
    width: imageWidth,
    height: imageHeight,
    marginBottom: 32,
  },
  title: {
    fontFamily: 'SFProDisplay-Bold',
    color: PDColors.textBlack,
    fontSize: 36,
    textAlign: 'center',
  },
  subtitle: {
    fontSize: 16,
    textAlign: 'center',
    fontFamily: 'SFProDisplay-Regular',
    color: PDColors.textDark,
    marginHorizontal: 40,
    marginVertical: 20,
  },
  btns: {
    marginHorizontal: 20,
  },
  mainBtn: {
    marginBottom: 60,
  },
  body: {
    alignItems: 'center',
    flex: 0.7,
  },
  submit_button: {
    justifyContent: 'center',
    backgroundColor: PDColors.transparent,
  },
  submit_button_title: {
    fontSize: 20,
    textAlign: 'center',
    fontFamily: 'SFProDisplay-Regular',
  },
});
