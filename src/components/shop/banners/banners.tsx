import React from 'react';
import {Linking, StyleSheet, View} from 'react-native';
import {useSettings} from '../../../redux/settings/selectors';
import Config from 'react-native-config';
import {Slider} from '../../slider/slider';

export const PDBanners = () => {
  const settings = useSettings();
  if (!settings.bannerList) {
    return null;
  }
  return (
    <View>
      <Slider
        autoplay
        circleLoop
        paginationBoxVerticalPadding={20}
        images={settings.bannerList.map(
          item => `${Config.API_BACK_URL}${item.src}`,
        )}
        onCurrentImagePressed={(index: number) => {
          const url = settings.bannerList[index]?.adsURL;
          url ? Linking.openURL(url) : null;
        }}
        paginationDot={false}
        ImageComponentStyle={styles.image_style}
      />
    </View>
  );
};
const styles = StyleSheet.create({
  image_style: {
    borderRadius: 15,
  },
});
