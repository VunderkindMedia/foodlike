import React, {useCallback} from 'react';
import {View, Text, StyleSheet, StyleProp, ViewStyle} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {Routes} from '../../../navigation/constants/routes';
import Config from 'react-native-config';
import PDColors from '../../../constants/colors/colors';
import {PDIcons} from '../../icons/custom-icons';

interface IPDSearchFieldProps {
  navigation: any;
  style?: StyleProp<ViewStyle>;
}
export const PDSearchField = (props: IPDSearchFieldProps) => {
  const {navigation, style} = props;
  const clickHandler = useCallback(
    () => navigation.push(Routes.SCREEN_SEARCH),
    [navigation],
  );
  return (
    <View style={style}>
      <View style={styles.search_wrapper}>
        <PDIcons
          name="search"
          size={20}
          color={'#8E8E93'}
          style={styles.search_icon}
        />
        <TouchableOpacity
          style={styles.search_input}
          containerStyle={styles.search_input_container}
          activeOpacity={0.8}
          onPress={clickHandler}>
          <Text style={styles.search_title}>Искать в {Config.APP_NAME}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  search_wrapper: {
    backgroundColor: 'rgba(118, 118, 128, 0.12)',
    height: 52,
    borderRadius: 15,
    padding: 15,
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 12.5,
    marginTop: 12.5,
  },
  search_input: {
    flex: 1,
    justifyContent: 'center',
  },
  search_icon: {
    marginRight: 10,
  },
  search_title: {
    color: PDColors.normalGrayText,
    fontSize: 16,
    fontFamily: 'SFProDisplay-Regular',
  },
  search_input_container: {justifyContent: 'center'},
});
