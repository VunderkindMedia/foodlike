import {View} from 'react-native';
import React from 'react';

interface IPDividerProps {
  height?: number;
  width?: number;
}
export const PDivider = (props: IPDividerProps) => {
  const {height = 0, width = 0} = props;
  return <View style={{width: width, height: height}} />;
};
