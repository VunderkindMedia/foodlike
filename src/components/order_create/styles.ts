import {StyleSheet} from 'react-native';
import PDColors from '../../constants/colors/colors';

export const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 24,
    paddingVertical: 32,
  },
  title: {
    color: PDColors.textBlack,
    fontSize: 24,
    fontFamily: 'SFProDisplay-Bold',
    marginBottom: 28,
  },
});
