import React, {useCallback, useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {FormikErrors} from 'formik';
import PDColors from '../../constants/colors/colors';
import {styles} from './styles';
import {
  ORDER_CREATE_USER_INFO_CHECKBOX,
  ORDER_CREATE_USER_INFO_TITLE,
  ORDER_CREATE_USER_NAME_TITLE,
  ORDER_CREATE_USER_PHONE_TITLE,
} from '../../constants/strings';
import {PDInput} from '../inputs/default_input/input';
import {PDCheckbox} from '../checkboxes/checkbox';
import {PDivider} from '../dividers/divider';
import {IPDCreateOrderForm} from '../../screens/cart/form/form-options';

interface IPDInOrderUserInfoProps {
  setValue: (field: string, value: any) => void;
  errors: FormikErrors<IPDCreateOrderForm>;
  values: IPDCreateOrderForm;
}

export const PDInOrderUserInfo = (props: IPDInOrderUserInfoProps) => {
  const {setValue, errors, values} = props;
  const [visible, setVisible] = useState(false);
  const onUserNameHandler = useCallback(
    (val: string) => {
      setValue('user_name', val);
    },
    [setValue],
  );
  const onUserPhoneHandler = useCallback(
    (val: string) => {
      setValue('user_phone', val);
    },
    [setValue],
  );
  const visibleHandler = useCallback(() => setVisible(!visible), [visible]);
  return (
    <View style={_styles.container}>
      <PDCheckbox
        onChange={visibleHandler}
        keyId={0}
        checked={visible}
        checkbox
        title={ORDER_CREATE_USER_INFO_CHECKBOX}
      />
      {visible && (
        <>
          <PDivider height={18} />
          <Text style={styles.title}>{ORDER_CREATE_USER_INFO_TITLE}</Text>
          <PDInput
            style={_styles.input}
            value={values.user_name}
            onChangeText={onUserNameHandler}
            label={ORDER_CREATE_USER_NAME_TITLE}
          />
          <PDInput
            value={values.user_phone}
            onChangeText={onUserPhoneHandler}
            label={ORDER_CREATE_USER_PHONE_TITLE}
          />
        </>
      )}
    </View>
  );
};

const _styles = StyleSheet.create({
  container: {
    marginTop: 34,
  },
  tile: {
    backgroundColor: PDColors.buttonLightGray,
    borderRadius: 8,
    padding: 16,
  },
  input: {
    marginBottom: 24,
  },
});
