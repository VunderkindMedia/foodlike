import React, {useCallback} from 'react';
import {View, Text, StyleSheet, LogBox} from 'react-native';
import {FormikErrors} from 'formik';
import {styles} from './styles';
import {ORDER_CREATE_DELIVERY_TIME_TITLE} from '../../constants/strings';
import {PDTileDefault} from '../tiles/default-tile';
import PDColors from '../../constants/colors/colors';
import {dateToStringFormatter} from '../../helpers/formatters';
import {TOrderCreateNavigatorProp} from '../../navigation/navigators/product/types';
import {Routes} from '../../navigation/constants/routes';
import {IPDCreateOrderForm} from '../../screens/cart/form/form-options';
import {globalStyles} from '../../constants/global-styles/styles';
// import {addHoursToDate, getAvailableDeliveryTime} from '../../helpers/date';

interface IPDInOrderDeliveryTimeProps {
  setValue: (field: string, value: any) => void;
  errors: FormikErrors<IPDCreateOrderForm>;
  values: IPDCreateOrderForm;
  navigation: TOrderCreateNavigatorProp;
}

export const PDInOrderDeliveryTime = (props: IPDInOrderDeliveryTimeProps) => {
  LogBox.ignoreLogs([
    'Non-serializable values were found in the navigation state',
  ]);

  const {setValue, errors, values, navigation} = props;
  const openHandler = useCallback(
    () =>
      navigation.push(Routes.SCREEN_DELIVERY_TIME, {
        value: values.delivery_time?.toString(),

        onConfirm: date => setValue('delivery_time', date),
      }),
    [navigation, setValue, values],
  );
  return (
    <View style={_styles.container}>
      <Text style={styles.title}>{ORDER_CREATE_DELIVERY_TIME_TITLE}</Text>
      <PDTileDefault
        onPress={openHandler}
        text={
          values.delivery_time
            ? dateToStringFormatter(values.delivery_time)
            : 'Время доставки'
        }
        style={[_styles.tile, !!errors.delivery_time && _styles.errorTile]}
      />
      {errors.delivery_time && (
        <Text style={globalStyles.fieldErrorText}>{errors.delivery_time}</Text>
      )}
    </View>
  );
};

const _styles = StyleSheet.create({
  container: {
    marginTop: 24,
  },
  tile: {
    backgroundColor: '#e8e8e8',
    borderRadius: 8,
    padding: 16,
  },
  errorTile: {
    borderColor: 'red',
    borderWidth: 1,
  },
});
