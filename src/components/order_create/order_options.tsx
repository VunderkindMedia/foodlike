import React, {useCallback} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {PDCheckbox} from '../checkboxes/checkbox';
import {PDCounterPickerWithTitle} from '../inputs/counter-pickers/title-counter-picker';
import {PDivider} from '../dividers/divider';
import PDColors from '../../constants/colors/colors';
import {
  ORDER_CREATE_NOT_RECALL,
  ORDER_CREATE_NOT_RECALL_MSG,
  ORDER_CREATE_TABLEWARE_COUNT,
  ORDER_CREATE_USE_BONUSES,
} from '../../constants/strings';
import {IPDCreateOrderForm} from '../../screens/cart/form/form-options';
import {Client} from '../../model/client';

interface IPDInOrderOptionsProps {
  setValue: (field: string, value: any) => void;
  values: IPDCreateOrderForm;
  client: Client | null;
}

export const PDInOrderOptions = (props: IPDInOrderOptionsProps) => {
  const {setValue, values, client} = props;
  const bonusesHandler = useCallback(
    () => setValue('use_bonuses', !values.use_bonuses),
    [setValue, values.use_bonuses],
  );
  const tablewareHandler = useCallback(
    val => setValue('tableware_count', val),
    [setValue],
  );
  const recallHandler = useCallback(
    () => setValue('recall', !values.recall),
    [setValue, values.recall],
  );

  return (
    <View style={styles.container}>
      <PDCounterPickerWithTitle
        units={'шт'}
        defaultValue={values.tableware_count}
        title={ORDER_CREATE_TABLEWARE_COUNT}
        onChange={tablewareHandler}
      />
      <PDivider height={13} />
      <PDCheckbox
        title={
          <Text style={styles.title}>
            {ORDER_CREATE_USE_BONUSES} (
            <Text style={styles.bonusesText}>
              {client?.bonus?.toFixed(2).replace('.', ',')} ₽
            </Text>
            )
          </Text>
        }
        onChange={bonusesHandler}
        keyId={0}
        checkbox
        checked={values.use_bonuses}
      />
      <PDivider height={13} />
      <PDCheckbox
        title={ORDER_CREATE_NOT_RECALL}
        onChange={recallHandler}
        keyId={0}
        checkbox
        checked={values.recall}
      />
      {values.recall && (
        <>
          <PDivider height={13} />
          <Text>{ORDER_CREATE_NOT_RECALL_MSG}</Text>
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    borderColor: PDColors.buttonLightGray,
    paddingVertical: 32,
  },
  bonusesText: {
    color: 'red',
  },
  title: {
    fontFamily: 'SFProDisplay-Regular',
  },
});
