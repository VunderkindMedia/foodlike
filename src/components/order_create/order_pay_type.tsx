import React, {useCallback, useMemo} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {FormikErrors} from 'formik';
import {PDCheckbox} from '../checkboxes/checkbox';
import {Payment} from '../../model/payment';
import {PDivider} from '../dividers/divider';
import {styles} from './styles';
import {ORDER_CREATE_PAY_TYPE_TITLE} from '../../constants/strings';
import {IPDCreateOrderForm} from '../../screens/cart/form/form-options';
import {globalStyles} from '../../constants/global-styles/styles';

interface IPDInOrderPayTypeProps {
  setValue: (field: string, value: any) => void;
  errors: FormikErrors<IPDCreateOrderForm>;
  values: IPDCreateOrderForm;
  payment_types: Payment[];
}

export const PDInOrderPayType = (props: IPDInOrderPayTypeProps) => {
  const {setValue, errors, values, payment_types} = props;
  const reversePaymentTypes = useMemo(
    () => payment_types.reverse(),
    [payment_types],
  );
  const changeHandler = useCallback(
    val => setValue('payment_type', val),
    [setValue],
  );
  return (
    <View style={_styles.container}>
      <Text style={styles.title}>{ORDER_CREATE_PAY_TYPE_TITLE}</Text>

      {reversePaymentTypes.map((type, index) => {
        const icon =
          type.payment_method_id === 1
            ? 'money'
            : type.payment_method_id === 2
            ? 'credit-card'
            : 'money';
        return (
          <View key={type.payment_method_id}>
            <PDCheckbox
              prefixIconName={icon}
              title={type.title}
              onChange={changeHandler}
              key={type.payment_method_id}
              keyId={type.payment_method_id}
              checked={values.payment_type === type.payment_method_id}
            />

            {payment_types.length > 1 && index !== payment_types.length - 1 && (
              <PDivider key={`${type.payment_method_id}divider`} height={30} />
            )}
          </View>
        );
      })}
      {errors.payment_type && (
        <Text style={[globalStyles.fieldErrorText, _styles.fieldErrorText]}>
          {errors.payment_type}
        </Text>
      )}
    </View>
  );
};
const _styles = StyleSheet.create({
  container: {
    marginVertical: 24,
  },
  fieldErrorText: {
    marginTop: 15,
  },
});
