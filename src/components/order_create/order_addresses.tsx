import React, {useCallback, useEffect, useMemo} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {FormikErrors} from 'formik';
import {
  ORDER_CREATE_ADDRESS_BUTTON_TITLE,
  ORDER_CREATE_ADDRESS_TITLE,
} from '../../constants/strings';
import {styles} from './styles';
import {PDTileDefault} from '../tiles/default-tile';
import PDColors from '../../constants/colors/colors';
import {
  TOrderCreateNavigatorProp,
  TOrderCreateRouteProp,
} from '../../navigation/navigators/product/types';
import {Routes} from '../../navigation/constants/routes';
import {Address} from '../../model/address';
import {IPDCreateOrderForm} from '../../screens/cart/form/form-options';
import {globalStyles} from '../../constants/global-styles/styles';
import {useAddresses, useCheckedAddress} from '../../redux/account/selectors';

interface IPDInOrderCheckAddressProps {
  errors: FormikErrors<IPDCreateOrderForm>;
  values: IPDCreateOrderForm;
  setValue: (field: string, value: any) => void;
  route: TOrderCreateRouteProp;
  navigation: TOrderCreateNavigatorProp;
  checked_address?: Address;
}
export const PDInOrderCheckAddress = (props: IPDInOrderCheckAddressProps) => {
  const {navigation, checked_address, errors, setValue} = props;
  const confirmHandler = useCallback(() => navigation.pop(), [navigation]);
  const goTo = useCallback(
    () =>
      navigation?.push(Routes.SCREEN_ADDRESSES, {onConfirm: confirmHandler}),
    [navigation, confirmHandler],
  );
  const checkedAddress = useCheckedAddress();
  const addresses = useAddresses();
  const address = useMemo(
    () => addresses.find(add => add.id === checkedAddress),
    [addresses, checkedAddress],
  );
  useEffect(() => {
    if (!address) {
      setValue('address', undefined);
    } else {
      setValue('address', JSON.stringify(address));
    }
  }, [address, setValue]);

  return (
    <View>
      <Text style={styles.title}>{ORDER_CREATE_ADDRESS_TITLE}</Text>
      <PDTileDefault
        onPress={goTo}
        text={checked_address?.name ?? ORDER_CREATE_ADDRESS_BUTTON_TITLE}
        style={[_styles.tile, !!errors.address && _styles.errorTile]}
        titleStyle={[
          _styles.tileTitle,
          !!checkedAddress && _styles.checkedValue,
        ]}
      />
      {!!errors.address && (
        <Text style={globalStyles.fieldErrorText}>{errors.address}</Text>
      )}
    </View>
  );
};
const _styles = StyleSheet.create({
  tile: {
    borderWidth: 1,
    borderRadius: 8,
    padding: 16,
  },
  tileTitle: {
    color: PDColors.textDark,
    fontFamily: 'SFProDisplay-Regular',
  },
  errorTile: {
    borderColor: 'red',
    borderWidth: 1,
  },
  checkedValue: {
    color: PDColors.textBlack,
  },
});
