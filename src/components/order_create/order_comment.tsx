import React, {useCallback} from 'react';
import {View, StyleSheet} from 'react-native';
import {ORDER_COMMENT_TITLE} from '../../constants/strings';
import {FormikErrors} from 'formik';
import PDColors from '../../constants/colors/colors';
import {PDInput} from '../inputs/default_input/input';
import {IPDCreateOrderForm} from '../../screens/cart/form/form-options';
interface IPDOrderCommentProps {
  setValue: (field: string, value: any) => void;
  errors: FormikErrors<IPDCreateOrderForm>;
  values: IPDCreateOrderForm;
}
export const PDInOrderComment = (props: IPDOrderCommentProps) => {
  const {setValue, errors, values} = props;
  const changeHandler = useCallback(
    val => setValue('comment', val),
    [setValue],
  );
  return (
    <View style={styles.container}>
      <PDInput
        label={ORDER_COMMENT_TITLE}
        onChangeText={changeHandler}
        style={styles.inputView}
        inputStyle={styles.input}
        numberOfLines={4}
        maxLength={250}
        selectionColor={PDColors.textDark}
        value={values.comment}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: 24,
  },
  title: {
    fontSize: 14,
    color: PDColors.textDark,
    marginBottom: 6,
  },
  inputView: {
    borderWidth: 1,
    borderColor: PDColors.buttonLightGray,
    borderRadius: 8,
    paddingHorizontal: 16,
    paddingVertical: 12,
    color: PDColors.textBlack,
    fontSize: 16,
  },
  input: {
    textAlignVertical: 'top',
  },
});
