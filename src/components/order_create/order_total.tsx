import React, {useMemo} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import PDColors from '../../constants/colors/colors';
import {Client} from '../../model/client';
import {discount} from '../../helpers/formatters';
import PDImages from '../../constants/images/images';

interface IPDInOrderTotalProps {
  discountSum: number;
  deliveryPrice: number;
  cartSum: number;
  client: Client | null;
  use_bonuses: boolean;
  inFinish: boolean;
}

export const PDInCreateOrderTotal = (props: IPDInOrderTotalProps) => {
  const {deliveryPrice, cartSum, client, use_bonuses, inFinish = false} = props;
  const bonuses = useMemo(
    () =>
      discount(
        client?.client_groups_discount ?? 0,
        use_bonuses ? cartSum - (client!.bonus ?? 0) : cartSum,
        1,
      ),
    [client, use_bonuses, cartSum],
  );
  const total = useMemo(
    () =>
      use_bonuses
        ? cartSum + deliveryPrice - (client!.bonus ?? 0)
        : cartSum + deliveryPrice,
    [cartSum, client, deliveryPrice, use_bonuses],
  );
  return (
    <View
      style={[
        styles.order_total__footer_wrapper,
        inFinish && styles.order_total__isFinish,
      ]}>
      {!inFinish && <Text style={styles.order_total__title}>Итого</Text>}
      {/*<View style={styles.order_total__discount_wrapper}>*/}
      {/*  <Text style={styles.order_total__discount_title}>Скидка</Text>*/}
      {/*  <Text style={styles.order_total__discount_value}>{discountSum} ₽</Text>*/}
      {/*</View>*/}
      {inFinish && (
        <View style={styles.order_total__row_wrapper}>
          <Text style={styles.order_total__discount_title}>Товары</Text>
          <Text style={styles.order_total__delivery_value}>{cartSum} ₽</Text>
        </View>
      )}
      {use_bonuses && (
        <View style={styles.order_total__row_wrapper}>
          <Text style={styles.order_total__discount_title}>Бонусы</Text>
          <Text style={styles.order_total__discount_value}>
            {client?.bonus?.toFixed(2).replace('.', ',')} ₽
          </Text>
        </View>
      )}
      <View style={styles.order_total__delivery_wrapper}>
        <Text style={styles.order_total__discount_title}>
          Стоимость доставки
        </Text>
        <Text style={styles.order_total__delivery_value}>
          {deliveryPrice} ₽
        </Text>
      </View>
      <View style={styles.order_total__row_wrapper}>
        <Text
          style={[
            styles.order_total__footer_amount_title,
            inFinish && styles.order_total__footer_amount_title_big,
          ]}>
          К оплате
        </Text>
        <Text
          style={[
            styles.order_total__footer_amount_title,
            inFinish && styles.order_total__footer_amount_title_big,
          ]}>
          {total.toFixed(2).replace('.', ',')} ₽
        </Text>
      </View>
      <View style={styles.order_total__bonuses_wrapper}>
        <Image
          style={styles.order_total__bonuses__icon}
          source={PDImages.money}
        />
        <Text
          style={
            styles.order_total__bonuses__text
          }>{`Будет начислено ${bonuses} бонусов`}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  order_total__title: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 24,
    color: PDColors.textBlack,
    marginBottom: 24,
  },
  order_total__footer_wrapper: {
    paddingVertical: 16,
  },
  order_total__row_wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  order_total__discount_title: {
    fontFamily: 'SFProDisplay-Regular',
    fontSize: 16,
    color: PDColors.textDark,
  },
  order_total__discount_value: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 16,
    color: PDColors.primary,
  },
  order_total__delivery_wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 24,
  },
  order_total__delivery_value: {
    fontFamily: 'SFProDisplay-Semibold',
    fontSize: 16,
    color: PDColors.textDark,
  },
  order_total__footer_amount_title: {
    fontFamily: 'SFProDisplay',
    fontWeight: '700',
    fontSize: 16,
    color: PDColors.textBlack,
  },
  order_total__bonuses_wrapper: {
    marginTop: 32,
    marginBottom: 32,
    padding: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderBottomColor: PDColors.buttonLightGray,
    borderTopColor: PDColors.buttonLightGray,
  },
  order_total__bonuses__icon: {
    marginRight: 8,
  },
  order_total__bonuses__text: {
    fontSize: 16,
    fontWeight: '400',
    color: PDColors.textBlack,
  },
  order_total__isFinish: {
    paddingHorizontal: 24,
  },
  order_total__footer_amount_title_big: {
    fontSize: 24,
  },
});
