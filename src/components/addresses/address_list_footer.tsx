import React from 'react';
import {StyleSheet, Text} from 'react-native';
import RNBounceable from '@freakycoder/react-native-bounceable';
import {PDIcons} from '../icons/custom-icons';
import PDColors from '../../constants/colors/colors';
import {ADDRESS_ADD_NEW} from '../../constants/strings';
interface IPDAddressListFooterProps {
  onPress: () => void;
}
export const PDAddressListFooter = (props: IPDAddressListFooterProps) => {
  const {onPress} = props;
  return (
    <RNBounceable onPress={onPress} style={styles.container}>
      <PDIcons name={'plus_circle'} size={20} color={PDColors.primary} />
      <Text style={styles.title}>{ADDRESS_ADD_NEW}</Text>
    </RNBounceable>
  );
};
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginTop: 26,
    marginLeft: 6,
  },
  title: {
    marginLeft: 18,
    fontSize: 16,
    color: PDColors.primary,
    fontFamily: 'SFProDisplay-Regular',
  },
});
