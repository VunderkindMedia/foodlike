import React, {useCallback} from 'react';
import {View, StyleSheet} from 'react-native';
import {PDInput} from '../inputs/default_input/input';
import {
  ADDRESS_APART_LABEL,
  ADDRESS_ENTRANCE_LABEL,
  ADDRESS_FLOOR_LABEL,
  ADDRESS_STREET_LABEL,
} from '../../constants/strings';

interface IPDAddressEditProps {
  onStreetChange: (val: string) => void;
  onApartChange: (val: string) => void;
  onEntranceChange: (val: string) => void;
  onFloorChange: (val: string) => void;
  street: string;
  apart: string;
  entrance: string;
  floor: string;
}

export const PDAddressEdit = (props: IPDAddressEditProps) => {
  const {
    onStreetChange,
    onApartChange,
    onEntranceChange,
    onFloorChange,
    street,
    apart,
    entrance,
    floor,
  } = props;
  const onStreetHandler = useCallback(
    (val: string) => onStreetChange(val),
    [onStreetChange],
  );
  const onApartHandler = useCallback(
    (val: string) => onApartChange(val),
    [onApartChange],
  );
  const onEntranceHandler = useCallback(
    (val: string) => onEntranceChange(val),
    [onEntranceChange],
  );
  const onFloorHandler = useCallback(
    (val: string) => onFloorChange(val),
    [onFloorChange],
  );
  return (
    <View style={styles.container}>
      <View style={styles.streetWrapper}>
        <PDInput
          onChangeText={onStreetHandler}
          label={ADDRESS_STREET_LABEL}
          value={street}
        />
      </View>
      <View style={styles.bottomWrapper}>
        <PDInput
          style={styles.input}
          value={apart}
          onChangeText={onApartHandler}
          label={ADDRESS_APART_LABEL}
          suffix={false}
        />
        <PDInput
          style={styles.input}
          value={entrance}
          onChangeText={onEntranceHandler}
          label={ADDRESS_ENTRANCE_LABEL}
          suffix={false}
        />
        <PDInput
          style={styles.input}
          value={floor}
          onChangeText={onFloorHandler}
          label={ADDRESS_FLOOR_LABEL}
          suffix={false}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  streetWrapper: {
    marginBottom: 24,
  },
  bottomWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  input: {
    width: 72,
  },
});
