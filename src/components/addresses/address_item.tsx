import React, {useCallback} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Address} from '../../model/address';
import {PDCheckbox} from '../checkboxes/checkbox';
import {useCheckedAddress} from '../../redux/account/selectors';
import {useCheckAddress, useRemoveAddress} from '../../hooks/account';
import RNBounceable from '@freakycoder/react-native-bounceable';
import {PDIcons} from '../icons/custom-icons';
import {PDivider} from '../dividers/divider';
import PDColors from '../../constants/colors/colors';
import {twoButtonAlert} from '../../constants/allerts';
interface IPDAddressItemProps {
  address: Address;
}
export const PDAddressItem = (props: IPDAddressItemProps) => {
  const {address} = props;
  const checkAddress = useCheckAddress();
  const removeAddress = useRemoveAddress();
  const checkHandler = useCallback(
    () => checkAddress(address),
    [address, checkAddress],
  );
  const removeHandler = useCallback(() => {
    twoButtonAlert(
      'Удалить?',
      'Вы действительно хотите удалить адрес?',
      'Удалить',
      () => removeAddress(address),
    );
  }, [address, removeAddress]);

  const checked = useCheckedAddress();

  return (
    <RNBounceable style={styles.container} onPress={checkHandler}>
      <View style={styles.wrapper}>
        <PDCheckbox
          onChange={checkHandler}
          keyId={address.id}
          checked={checked === address.id}
        />
        <PDivider width={16} />
        <Text style={styles.title}>{address.name}</Text>
      </View>

      <RNBounceable style={styles.close_button_wrapper} onPress={removeHandler}>
        <PDIcons size={8} name={'close'} color={PDColors.textWhite} />
      </RNBounceable>
    </RNBounceable>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 17,
    borderBottomWidth: 1,
    borderColor: PDColors.buttonLightGray,
  },
  wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 4,
  },
  title: {
    fontSize: 16,
    color: PDColors.textBlack,
    fontFamily: 'SFProDisplay-Regular',
  },
  close_button_wrapper: {
    width: 20,
    height: 20,
    borderRadius: 50,
    backgroundColor: PDColors.textDark,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
