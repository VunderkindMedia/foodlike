import React, {useCallback, useEffect, useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';

import {
  TOrderDeliveryTimeNavigatorProp,
  TOrderDeliveryTimeRouteProp,
} from '../../navigation/navigators/product/types';
import {DefaultPDButton} from '../buttons/default-button';
import PDColors from '../../constants/colors/colors';
import {
  correctDateMinutesByValue,
  getAvailableDeliveryTime,
} from '../../helpers/date';
import DatePicker from 'react-native-date-picker';
import moment from 'moment-timezone';
import {PDIcons} from '../icons/custom-icons';
moment.tz.setDefault('Asia/Sakhalin');

interface IPDDateTimePickerProps {
  date: Date;
  navigation: TOrderDeliveryTimeNavigatorProp;
  route: TOrderDeliveryTimeRouteProp;
}
export const PDDateTimePicker = (props: IPDDateTimePickerProps) => {
  const {navigation, route} = props;
  const {value, onConfirm} = route.params;
  const [currentDate] = useState(
    correctDateMinutesByValue(5, moment().add(1, 'h').toDate()),
  );
  const [date, setDate] = useState(
    value ? moment(value).toDate() : getAvailableDeliveryTime(currentDate),
  );

  useEffect(() => {
    if (onConfirm) {
      onConfirm(date.toJSON());
    }
  }, [date, onConfirm]);

  const changeHandler = useCallback(
    (val: Date) => {
      setDate(getAvailableDeliveryTime(val)!);
    },
    [setDate],
  );

  const fasterHandler = useCallback(() => {
    if (onConfirm) {
      onConfirm(getAvailableDeliveryTime(currentDate).toJSON());
    }
    navigation.pop();
  }, [currentDate, navigation, onConfirm]);
  const closeHandler = useCallback(() => navigation.pop(), [navigation]);

  return (
    <TouchableOpacity
      activeOpacity={1}
      style={styles.container}
      onPress={closeHandler}>
      <View style={styles.picker_wrapper}>
        <View style={styles.header_wrapper}>
          <Text style={styles.title}>Дата и время доставки</Text>
          <TouchableOpacity
            activeOpacity={0}
            onPress={closeHandler}
            style={styles.close_btn}>
            <PDIcons name={'close'} size={12} color={PDColors.textBlack} />
          </TouchableOpacity>
        </View>
        <DatePicker
          is24hourSource={'locale'}
          title={'Выбор даты'}
          androidVariant={'iosClone'}
          minimumDate={currentDate}
          date={date}
          onDateChange={changeHandler}
          minuteInterval={5}
        />

        <View style={styles.footer_wrapper}>
          <DefaultPDButton
            buttonStyle={styles.button}
            title={'Применить'}
            onPress={closeHandler}
          />
          <DefaultPDButton
            buttonStyle={styles.fastButton}
            title={'Как можно скорее'}
            onPress={fasterHandler}
            titleColor={PDColors.textBlack}
          />
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  free_space: {
    flex: 1,
    backgroundColor: 'red',
  },
  picker_wrapper: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    backgroundColor: PDColors.whiteBackground,
    paddingHorizontal: 24,
  },
  header_wrapper: {
    paddingTop: 21,
    paddingBottom: 21,
    borderBottomWidth: 1,
    width: '100%',
    alignItems: 'center',
    borderBottomColor: PDColors.buttonLightGray,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  title: {
    fontSize: 17,
    fontWeight: '600',
    color: PDColors.textBlack,
  },
  footer_wrapper: {
    paddingTop: 16,
    paddingBottom: 20,
    width: '100%',
  },
  button: {
    justifyContent: 'center',
    marginBottom: 5,
  },
  fastButton: {
    justifyContent: 'center',
    backgroundColor: PDColors.transparent,
  },
  close_btn: {
    position: 'absolute',
    right: 15,
    top: 25,
  },
});
