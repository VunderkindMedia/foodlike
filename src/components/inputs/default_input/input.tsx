import React, {useCallback, useEffect, useState} from 'react';
import {
  TextInput,
  TextInputChangeEventData,
  NativeSyntheticEvent,
  View,
  Animated,
  StyleSheet,
  StyleProp,
  ViewStyle,
  TextStyle,
} from 'react-native';
import PDColors from '../../../constants/colors/colors';
import {PDIcons} from '../../icons/custom-icons';
import RNBounceable from '@freakycoder/react-native-bounceable';
import {PhoneInput} from '../phone-input/phone-input';
interface IPDInputProps {
  onChangeText: (val: string) => void;
  label: string;
  value: string;
  style?: StyleProp<ViewStyle>;
  inputStyle?: StyleProp<TextStyle>;
  numberOfLines?: number;
  maxLength?: number;
  selectionColor?: string;
  suffix?: boolean;
  type?: string;
}
export const PDInput = (props: IPDInputProps) => {
  const [animated] = useState(new Animated.Value(0));
  const [isFocused, setFocus] = useState(false);
  const handleFocus = useCallback(() => setFocus(true), []);
  const handleBlur = useCallback(() => setFocus(false), []);
  const {
    onChangeText,
    label,
    value = '',
    style,
    numberOfLines,
    maxLength,
    inputStyle,
    selectionColor = PDColors.textDark,
    suffix = true,
    type = 'default',
  } = props;
  const dynamicLabelStyle = {
    left: animated.interpolate({
      inputRange: [0, 1],
      outputRange: [numberOfLines ? 20 : 0, numberOfLines ? 0 : 0],
    }),
    top: animated.interpolate({
      inputRange: [0, 1],
      outputRange: [20, numberOfLines ? -20 : -6],
    }),
    fontSize: animated.interpolate({
      inputRange: [0, 1],
      outputRange: [16, 14],
    }),
  };
  useEffect(() => {
    Animated.timing(animated, {
      toValue: isFocused || value ? 1 : 0,
      duration: 200,
      useNativeDriver: false,
    }).start();
  });

  const cleanHandler = useCallback(() => onChangeText(''), [onChangeText]);
  const changeHandler = useCallback(
    (val: NativeSyntheticEvent<TextInputChangeEventData>) =>
      onChangeText(val.nativeEvent.text),
    [onChangeText],
  );
  const phoneChangeHandler = useCallback(
    (text: string) => onChangeText(text),
    [onChangeText],
  );
  return (
    <View style={[styles.container, style]}>
      <Animated.Text style={[styles.labelStyle, dynamicLabelStyle]}>
        {label}
      </Animated.Text>
      <View style={styles.wrapper}>
        {type === 'phone' ? (
          <PhoneInput
            onChange={phoneChangeHandler}
            value={value}
            placeholderTextColor={selectionColor}
            styles={[styles.input, inputStyle, styles.phone_input]}
          />
        ) : (
          <TextInput
            selectionColor={selectionColor}
            maxLength={maxLength}
            textAlignVertical={'bottom'}
            numberOfLines={numberOfLines}
            value={value}
            onBlur={handleBlur}
            onFocus={handleFocus}
            onChange={changeHandler}
            style={[styles.input, inputStyle]}
          />
        )}

        {(!numberOfLines || numberOfLines === 1) && value.length > 0 && suffix && (
          <RNBounceable
            style={styles.close_button_wrapper}
            onPress={cleanHandler}>
            <PDIcons size={8} name={'close'} color={PDColors.textWhite} />
          </RNBounceable>
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderBottomWidth: 1,
    borderColor: PDColors.buttonLightGray,
  },
  labelStyle: {
    position: 'absolute',
    left: 0,
    color: PDColors.textDark,
    paddingHorizontal: 0,
  },
  input: {
    color: PDColors.textBlack,
    fontSize: 16,
    paddingBottom: 6,
    fontFamily: 'SFProDisplay-Regular',
    fontWeight: '400',
    flex: 1,
  },
  phone_input: {
    paddingBottom: 0,
  },
  close_button_wrapper: {
    width: 20,
    height: 20,
    borderRadius: 50,
    backgroundColor: PDColors.textDark,
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
