import {TextInputMask} from 'react-native-masked-text';
import React from 'react';
import {StyleProp, TextStyle} from 'react-native';
import {PHONE_MASK} from '../../../constants/strings';

interface IPhoneInputProps {
  styles?: StyleProp<TextStyle>;
  onChange: (value: string) => void;
  value: string;
  placeholderTextColor: string;
}
export const PhoneInput = (props: IPhoneInputProps) => {
  const {styles, onChange, value, placeholderTextColor} = props;
  return (
    <TextInputMask
      style={styles}
      type={'custom'}
      options={{
        mask: PHONE_MASK,
        validator: val => val.length === 18,
      }}
      keyboardType={'number-pad'}
      value={value}
      placeholder={PHONE_MASK}
      placeholderTextColor={placeholderTextColor}
      onChangeText={text => {
        onChange(text);
      }}
    />
  );
};
