import {TextInputMask} from 'react-native-masked-text';
import React from 'react';
import {StyleProp, TextStyle} from 'react-native';
import {CODE_MASK} from '../../../constants/strings';

interface ICodeInputProps {
  styles?: StyleProp<TextStyle>;
  onChange: (rawValue: string) => void;
  value: string;
  placeholderTextColor: string;
}
export const CodeInput = (props: ICodeInputProps) => {
  const {styles, onChange, value, placeholderTextColor} = props;
  return (
    <TextInputMask
      style={styles}
      type={'custom'}
      options={{
        mask: CODE_MASK,
        getRawValue: value => {
          return value.replace(/-/g, '');
        },
      }}
      includeRawValueInChangeText={true}
      keyboardType={'number-pad'}
      value={value}
      placeholder={CODE_MASK}
      placeholderTextColor={placeholderTextColor}
      onChangeText={(text, rawText) => {
        onChange(rawText!);
      }}
    />
  );
};
