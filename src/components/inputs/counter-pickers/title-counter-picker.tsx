import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {PDCounterPicker} from './counter-picker';
import PDColors from '../../../constants/colors/colors';
interface IPDCounterPickerWithTitleProps {
  title: string;
  onChange: (val: number) => void;
  defaultValue?: number;
  units?: string;
}
export const PDCounterPickerWithTitle = (
  props: IPDCounterPickerWithTitleProps,
) => {
  const {onChange, title, defaultValue, units} = props;
  return (
    <View style={styles.container}>
      <Text style={styles.text}>{title}</Text>
      <PDCounterPicker
        defaultValue={defaultValue}
        onChange={onChange}
        units={units}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  text: {
    fontSize: 16,
    color: PDColors.textBlack,
  },
});
