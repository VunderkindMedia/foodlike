import React, {useCallback, useEffect, useState} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import RNBounceable from '@freakycoder/react-native-bounceable';
import {PDIcons} from '../../icons/custom-icons';
import PDColors from '../../../constants/colors/colors';
interface IPDCounterPickerProps {
  onChange: (val: number) => void;
  defaultValue?: number;
  units?: string;
}
export const PDCounterPicker = (props: IPDCounterPickerProps) => {
  const {defaultValue, units} = props;
  const [value, setValue] = useState<number>(defaultValue ?? 0);
  const incrementHandler = useCallback(() => setValue(prev => prev + 1), []);
  const decrementHandler = useCallback(
    () => value !== 0 && setValue(prev => prev - 1),
    [value],
  );

  return (
    <View style={styles.container}>
      <RNBounceable onPress={decrementHandler} style={styles.button}>
        <PDIcons name={'minus'} color={PDColors.textBlack} size={15} />
      </RNBounceable>
      <Text style={styles.value}>
        {value}
        {units && ` ${units}`}
      </Text>
      <RNBounceable onPress={incrementHandler} style={styles.button}>
        <PDIcons name={'plus'} color={PDColors.textBlack} size={15} />
      </RNBounceable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  button: {
    width: 32,
    height: 32,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: PDColors.buttonLightGray,
    justifyContent: 'center',
    alignItems: 'center',
  },
  value: {
    marginHorizontal: 8,
    fontSize: 16,
    color: PDColors.textBlack,
    fontWeight: '500',
  },
});
