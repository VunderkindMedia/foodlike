export interface IBannerJSON {
  upload_date: string;
  start_date: string;
  end_date: string;
  adsURL: string;
  name: string;
  src: string;
}

export class Banner {
  static parse(bannerJSON: IBannerJSON): Banner {
    const banner = new Banner();
    Object.assign(banner, bannerJSON);
    return banner;
  }
  upload_date = '';
  start_date = '';
  end_date = '';
  adsURL = '';
  name = '';
  src = '';
}
