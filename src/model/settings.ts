import {Banner} from './banner';
import {Payment} from './payment';
import {Order} from './order';

export interface IBotSettings {
  bot_token: string;
  chat_id: string;
}
export interface IDeliverySettings {
  weightCoef: string;
  floorPrice: string;
}

export interface ISettingsJSON {
  bannerList: Banner[];
  app_main_title: string;
  mobile_app_enabled: string;
  mobile_app_disabled_text: string;
  botSettings: IBotSettings;
  chat_id: string;
  deliverySettings: IDeliverySettings;
}

export interface IBackClientsJSON {
  last_order: Order[];
  client_phone: string;
  token: string;
  os: string;
}

export class Settings {
  static parse(settingsJSON: ISettingsJSON): Settings {
    const settings = new Settings();
    Object.assign(settings, settingsJSON);
    settings.mobile_app_enabled = settingsJSON.mobile_app_enabled !== '0';
    return settings;
  }
  bannerList: Banner[] = [];
  app_main_title = '';
  mobile_app_enabled = true;
  botSettings?: IBotSettings;
  chat_id?: string = '';
  deliverySettings?: IDeliverySettings;
  payment_types: Payment[] = [];
  mobile_app_disabled_text = '';
}
