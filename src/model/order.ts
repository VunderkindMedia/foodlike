export interface OrderProduct {
  io_product_id: number;
  product_id: number;
  modificator_id: number;
  incoming_order_id: number;
  count: number;
  price: number;
}
export interface IOrderJson {
  products: OrderProduct[];
  incoming_order_id: number;
  type: number;
  spot_id: number;
  status: number;
  client_id: number;
  client_address_id: number;
  table_id: number | null;
  comment: string;
  created_at: string;
  updated_at: string;
  transaction_id: number;
  service_mode: number;
  delivery_price: string;
  fiscal_spreading: number;
  fiscal_method: string;
  promotion: number | null;
  delivery_time: string;
  payment_method_id: number | null;
  first_name: string | null;
  last_name: string | null;
  phone: string;
  email: string | null;
  sex: string | null;
  birthday: string | null;
  address: string | null;
  text_status: string;
  id: number;
}

export class Order {
  products: OrderProduct[] = [];
  incoming_order_id = -1;
  type = -1;
  spot_id = -1;
  status = -1;
  client_id = -1;
  client_address_id = -1;
  table_id = -1;
  comment = '';
  created_at = '';
  updated_at = '';
  transaction_id = -1;
  service_mode = -1;
  delivery_price = '';
  fiscal_spreading = -1;
  fiscal_method = '';
  promotion = -1;
  delivery_time = '';
  payment_method_id = -1;
  first_name = '';
  last_name = '';
  phone = '';
  email = '';
  sex = '';
  birthday = '';
  address = '';
  text_status = '';
  id = -1;
  total_sum = 0;

  static parse(orderJson: IOrderJson): Order {
    const order = new Order();
    Object.assign(order, orderJson);
    order.total_sum = order.products.reduce(
      (w, product) => w + product.price * product.count,
      0,
    );
    return order;
  }
}
