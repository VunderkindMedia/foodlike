import {getClone} from '../helpers/json';

export interface IProductJson {
  product_id: number;
  name: string;
  description: string | null;
  seoTitle: string;
  seoDescription: string;
  pos_id: number;
  isWeight: boolean;
  popular: boolean;
  categoryPosId: number;
  categoryName: string;
  photo: string;
  price: number;
  discountPrice?: number;
  cartCount: number;
}
export class Product {
  product_id = 0;
  name = '';
  description = '';
  seoTitle = '';
  seoDescription = '';
  pos_id = 0;
  isWeight = false;
  popular = false;
  categoryPosId = 0;
  categoryName = '';
  photo = '';
  price = 0;
  discountPrice?: number;
  cartCount = 0;

  static parse(productJson: IProductJson): Product {
    const product = new Product();
    Object.assign(product, productJson);
    return product;
  }

  get clone(): Product {
    return Product.parse(getClone(this));
  }
}

export class OrderProduct {
  product_id = 0;
  count = 0;
}
