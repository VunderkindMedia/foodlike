export interface IPromotionJson {
  promotion_id: string;
  name: string;
  params: IPromotionParamsJson;
  date_start: string;
  date_end: string;
}

export interface IPromotionParamsJson {
  conditions: IConditionJson[];
  discount_value: string;
}

export interface IConditionJson {
  type: string;
  id: string;
  pcs: number;
  g: number;
  sum: number;
}

export class Promotion {
  promotion_id = '';
  name = '';
  params = new PromotionParams();
  date_start = '';
  date_end = '';

  static parse(promotionJson: IPromotionJson): Promotion {
    const promotion = new Promotion();
    Object.assign(promotion, promotionJson);
    promotion.params = PromotionParams.parse(promotionJson.params);
    return promotion;
  }
}

export class PromotionParams {
  conditions = [] as Condition[];
  discount_value = '';

  static parse(promotionParamsJson: IPromotionParamsJson): PromotionParams {
    const promotionParams = new PromotionParams();
    Object.assign(promotionParams, promotionParamsJson);
    promotionParams.conditions = promotionParamsJson.conditions.map(paramJson =>
      Condition.parse(paramJson),
    );
    return promotionParams;
  }
}

export class Condition {
  type = '';
  id = '';
  pcs = 0;
  g = 0;
  sum = 0;

  static parse(conditionJson: IConditionJson): Condition {
    const condition = new Condition();
    Object.assign(condition, conditionJson);
    return condition;
  }
}
