import {OrderProduct, Product} from './product';
import {Address} from './address';
import moment from 'moment';

export class OrderRequest {
  client_address: OrderAddress = new OrderAddress();
  first_name = '';
  products: OrderProduct[] = [];
  spot_id = 1;
  phone = '';
  service_mode = 3;
  delivery_time = '2021-12-12 23:55';
  comment = '';
  _userComment?: string = '';
  _paymentType?: string = 'Оплата картой курьеру';
  _tablewareCount?: number = 1;
  _recall?: boolean = false;
  _useBonuses?: boolean = false;
  _userName?: string = '';
  _userPhone?: string = '';

  static get Builder(): Builder {
    return new Builder();
  }
}

export class OrderAddress {
  address1 = '';
  address2 = '';
}

class Builder {
  private _orderRequest = new OrderRequest();

  withClientAddress(address?: Address): Builder {
    const tempAddress = new OrderAddress();
    tempAddress.address1 = address?.street ?? '';
    tempAddress.address2 = `п. ${address?.entrance ?? '-'} э. ${
      address?.floor ?? '-'
    } кв. ${address?.apartment ?? '-'}`;
    this._orderRequest.client_address = tempAddress;
    return this;
  }

  withName(name: string): Builder {
    this._orderRequest.first_name = name;
    return this;
  }

  withProducts(products: Product[]): Builder {
    this._orderRequest.products = products.map(product => {
      const tempProduct = new OrderProduct();
      tempProduct.product_id = product.pos_id;
      tempProduct.count = product.cartCount;
      return tempProduct;
    });
    return this;
  }

  withTablewareCount(count: number): Builder {
    this._orderRequest._tablewareCount = count;
    return this;
  }

  withRecall(recall: boolean): Builder {
    this._orderRequest._recall = recall;
    return this;
  }

  withBonuses(useBonus: boolean): Builder {
    this._orderRequest._useBonuses = useBonus;
    return this;
  }

  withPhone(phone: string): Builder {
    this._orderRequest.phone = phone;
    return this;
  }

  withPaymentType(type: string): Builder {
    this._orderRequest._paymentType = type;
    return this;
  }

  withDeliveryTime(time?: Date): Builder {
    this._orderRequest.delivery_time = moment(time)
      .tz('Asia/Sakhalin')
      .format('YYYY-MM-DD HH:mm:ss');
    return this;
  }

  withUserComment(comment: string): Builder {
    this._orderRequest._userComment = comment;
    return this;
  }

  withUserName(name: string): Builder {
    this._orderRequest._userName = name;
    return this;
  }

  withUserPhone(phone: string): Builder {
    this._orderRequest._userPhone = phone;
    return this;
  }

  build(): OrderRequest {
    let comment = ' - Android (2.0).';

    /* Способ оплаты */
    comment = `${comment} СО: ${this._orderRequest._paymentType}`;
    /* - Способ оплаты -  */

    /* Дата и время доставки */
    comment = `${comment}, ВД: ${this._orderRequest.delivery_time}`;
    /* - Дата и время доставки - */

    /* Кол-во приборов */
    comment = `${comment}, КП: ${this._orderRequest._tablewareCount}`;
    /* - Кол-во приборов - */

    /* Перезвонить или нет */
    comment = `${comment}, З: ${
      this._orderRequest._recall ? 'не перезванивать мне' : 'перезванивать'
    }`;
    /* - Перезвонить или нет - */

    /* Использовать бонусы */
    comment = `${comment}, Б: ${
      this._orderRequest._useBonuses ? 'списать' : 'не списывать'
    }`;
    /* - Использовать бонусы - */

    /* Пользовательский коммент */
    if (this._orderRequest._userComment) {
      comment = `${comment}, К: ${this._orderRequest._userComment}`;
    }
    /* - Пользовательский коммент - */

    /* Для другого пользователя */
    if (this._orderRequest._userComment) {
      comment = `${comment}, ЗДЧ: ${this._orderRequest._userName}, ${this._orderRequest._userPhone}`;
    }
    /* - Для другого пользователя - */

    this._orderRequest.comment = comment;
    return this._orderRequest;
  }
}
