import {Product} from './product';

export interface ITransactionJson {
  transaction_id: string;
  date_start: string;
  date_start_new: string;
  date_close: string;
  status: string;
  guests_count: string;
  discount: string;
  bonus: string;
  pay_type: string;
  payed_bonus: string;
  payed_card: string;
  payed_cash: string;
  payed_sum: string;
  payed_cert: string;
  payed_third_party: string;
  payed_card_type: string;
  payed_ewallet: string;
  round_sum: string;
  tip_sum: string;
  tips_card: string;
  tips_cash: string;
  sum: string;
  tax_sum: string;
  payment_method_id: string;
  spot_id: string;
  table_id: string;
  name: string;
  user_id: string;
  client_id: string;
  card_number: string;
  transaction_comment: string;
  reason: string;
  print_fiscal: string;
  total_profit: string;
  total_profit_netto: string;
  table_name: string | null;
  client_firstname: string;
  client_lastname: string;
  date_close_date: string;
  service_mode: string;
  processing_status: string;
  client_phone: string;
  delivery: TransactionDeliveryJson;
  products: TransactionProductJson[];
}
export interface TransactionDeliveryJson {
  payment_method_id: number;
  delivery_zone_id: number;
  bill_amount: number;
  delivery_price: number;
  country: string;
  city: string;
  address1: string;
  address2: string;
  comment: string;
  lat: string | null;
  lng: string | null;
  zip_code: string;
  delivery_time: string;
  courier_id: string | null;
}

export interface TransactionProductJson {
  product_id: string;
  modification_id: string;
  num: string;
  bonus_accrual: string;
  product_price: string;
  payed_sum: string;
  product_cost: string;
  product_cost_netto: string;
  product_profit: string;
  product_profit_netto: string;
}
export class Transaction {
  transaction_id = '';
  date_start = '';
  date_start_new = '';
  date_close = '';
  status = '';
  guests_count = '';
  discount = '';
  bonus = '';
  pay_type = '';
  payed_bonus = '';
  payed_card = '';
  payed_cash = '';
  payed_sum = '';
  payed_cert = '';
  payed_third_party = '';
  payed_card_type = '';
  payed_ewallet = '';
  round_sum = '';
  tip_sum = '';
  tips_card = '';
  tips_cash = '';
  sum = '';
  tax_sum = '';
  payment_method_id = '';
  spot_id = '';
  table_id = '';
  name = '';
  user_id = '';
  client_id = '';
  card_number = '';
  transaction_comment = '';
  reason = '';
  print_fiscal = '';
  total_profit = '';
  total_profit_netto = '';
  table_name = '';
  client_firstname = '';
  client_lastname = '';
  date_close_date = '';
  service_mode = '';
  processing_status = '';
  client_phone = '';
  delivery = new TransactionDelivery();
  products: Product[] = [];

  static parse(
    transactionJson: ITransactionJson,
    product_list: Product[],
  ): Transaction {
    const products: Product[] = [];
    const transaction = new Transaction();
    Object.assign(transaction, transactionJson);
    transactionJson.products.forEach(product => {
      const funded_product = product_list.find(
        p => p.pos_id === +product.product_id,
      );
      if (funded_product) {
        funded_product.cartCount = +product.num;
        products.push(funded_product);
      }
    });
    transaction.products = products;
    return transaction;
  }
}

export class TransactionDelivery {
  payment_method_id = -1;
  delivery_zone_id = -1;
  bill_amount = -1;
  delivery_price = -1;
  country = '';
  city = '';
  address1 = '';
  address2 = '';
  comment = '';
  lat = '';
  lng = '';
  zip_code = '';
  delivery_time = '';
  courier_id = '';
}
export class TransactionProduct {
  product_id = '';
  modification_id = '';
  num = '';
  bonus_accrual = '';
  product_price = '';
  payed_sum = '';
  product_cost = '';
  product_cost_netto = '';
  product_profit = '';
  product_profit_netto = '';
}
