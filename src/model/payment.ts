export interface IPaymentJson {
  payment_method_id: number;
  title: string;
  icon: string;
  color: string;
  money_type: MoneyTypes;
  payment_type: PaymentTypes;
  is_active: number;
}
export class Payment {
  payment_method_id = 0;
  title = '';
  icon = '';
  color = '';
  money_type = MoneyTypes.Empty;
  payment_type = PaymentTypes.Empty;
  is_active = 0;

  static parse(paymentJson: IPaymentJson): Payment {
    const payment = new Payment();
    Object.assign(payment, paymentJson);
    if (payment.payment_method_id === 1) {
      payment.title = 'Оплата наличными';
    } else if (payment.payment_method_id === 2) {
      payment.title = 'Оплата картой курьеру';
    }
    return payment;
  }
}

export enum PaymentTypes {
  Empty = 0,
  Cash = 1,
  Virtual = 2,
  Certificate = 4,
  Deposit = 5,
  Custom = 7,
}

export enum MoneyTypes {
  Empty = 0,
  Cash = 1,
  Card = 2,
  Another = 3,
}
