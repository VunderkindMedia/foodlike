import {getClone} from '../helpers/json';

export interface ICategoryJson {
  category_id: number;
  category_name: string;
  category_photo: string;
  category_photo_origin: string;
  parent_category: number;
  category_color: string;
  category_hidden: string;
  sort_order: string;
  fiscal: string;
  nodiscount: string;
  tax_id: string;
  left: string;
  right: string;
  level: string;
  category_tag: string;
}
export class Category {
  category_id = 0;
  category_name = '';
  category_photo = '';
  category_photo_origin = '';
  parent_category: number | null = null;
  category_color = 'pink';
  category_hidden = '0';
  sort_order = '0';
  fiscal = '0';
  nodiscount = '0';
  tax_id = '0';
  left = '0';
  right = '0';
  level = '0';
  category_tag = '';

  static parse(categoryJson: ICategoryJson): Category {
    const category = new Category();
    Object.assign(category, categoryJson);
    category.category_id = +categoryJson.category_id;
    category.parent_category = +categoryJson.parent_category;
    return category;
  }
  get clone(): Category {
    return Category.parse(getClone(this));
  }
}
