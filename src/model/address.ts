import {getClone} from '../helpers/json';
import {GUID} from '../helpers/randomizers';

export interface IAddressJson {
  name: string;
  street: string;
  apartment: string;
  entrance: string;
  floor: string;
}
export class Address {
  name = '';
  street = '';
  apartment = '';
  entrance = '';
  floor = '';
  id = '';

  static parse(addressJSON: IAddressJson): Address {
    const address = new Address();
    Object.assign(address, addressJSON);
    return address;
  }
  get clone(): Address {
    return Address.parse(getClone(this));
  }
  static get Builder(): Builder {
    return new Builder();
  }
}

class Builder {
  private _address = new Address();

  withStreet(street: string): Builder {
    this._address.street = street;
    return this;
  }

  withApartment(apartment: string): Builder {
    this._address.apartment = apartment;
    return this;
  }

  withEntrance(entrance: string): Builder {
    this._address.entrance = entrance;
    return this;
  }

  withFloor(floor: string): Builder {
    this._address.floor = floor;
    return this;
  }

  build(): Address {
    this._address.id = GUID();
    this._address.name = `${this._address.street}`;
    if (this._address.floor) {
      this._address.name = this._address.name + `, п. ${this._address.floor}`;
    }
    if (this._address.entrance) {
      this._address.name =
        this._address.name + `, э. ${this._address.entrance}`;
    }

    this._address.name =
      this._address.name + `, кв. ${this._address.apartment}`;

    return this._address;
  }
}
