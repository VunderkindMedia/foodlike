import {getClone} from '../helpers/json';
import {IPDProfileEditForm} from '../screens/profile/form/form-options';

export interface IClientJson {
  client_id: string;
  firstname: string;
  lastname: string;
  patronymic: string;
  discount_per: string;
  bonus: number;
  total_payed_sum: string;
  date_activale: string;
  phone: string;
  phone_number: string;
  email: string;
  birthday: string;
  card_number: string;
  client_sex: string;
  country: string;
  city: string;
  comment: string;
  client_groups_id: string;
  client_groups_name: string;
  client_groups_discount: number;
  loyalty_type: string;
  birthday_bonus: string;
}
export class Client {
  client_id = '-1';
  firstname = '-';
  lastname = '-';
  client_name = '-';
  discount_per = '';
  bonus? = 0;
  total_payed_sum = '';
  date_activale = '';
  phone = '';
  phone_number = '';
  email = '';
  birthday = '';
  client_sex = '';
  country = '';
  city = '';
  comment = '';
  client_groups_id = '';
  client_groups_id_client = 1;
  client_groups_name = '';
  client_groups_discount: number | null = null;
  loyalty_type = '1';
  birthday_bonus = '';

  static parse(clientJSON: IClientJson): Client {
    const client = new Client();
    Object.assign(client, clientJSON);
    client.client_groups_discount = +clientJSON.client_groups_discount;
    client.bonus = clientJSON.bonus ? +clientJSON.bonus / 100 : 0;
    return client;
  }
  static fromForm(values: IPDProfileEditForm, currentClient: Client): Client {
    const client = currentClient.clone;
    client.client_name = `${values.lastname} ${values.firstname}`;
    client.client_sex = values.client_sex;
    client.birthday = values.birthday;
    client.email = values.email;
    client.phone = values.phone;
    client.bonus = undefined;
    return client;
  }
  get clone(): Client {
    return Client.parse(getClone(this));
  }
  static get Builder(): Builder {
    return new Builder();
  }
}

class Builder {
  private _client = new Client();

  withClientName(firstName: string, lastName: string): Builder {
    this._client.client_name = `${lastName} ${firstName}`;
    this._client.firstname = firstName;
    this._client.lastname = lastName;
    return this;
  }

  withPhone(phone: string): Builder {
    this._client.phone = phone;
    return this;
  }

  withEmail(email: string): Builder {
    this._client.email = email;
    return this;
  }

  build(): Client {
    return this._client;
  }
}
